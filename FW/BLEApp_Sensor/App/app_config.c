#include "app_config.h"
#include "app_state.h"
#include "config.h"
#include "fds.h"
#include "ble_gap.h"
#include "nrf_sdm.h"
#include "nrf_drv_uart.h"
#include "ctype.h"
#include "nrf_power.h"
#include "helper.h"

#include <stdlib.h>

#define APP_CONFIG_LOG_ENABLED //test
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
#ifndef APP_CONFIG_LOG_ENABLED
    #define APP_CONFIG_LOG_LEVEL 0
#endif

#define NRF_LOG_MODULE_NAME CFG
#include "nrf_log.h"

NRF_LOG_MODULE_REGISTER();

extern const app_config_t def_app_config;
	
app_config_t* m_app_config = (app_config_t*)&def_app_config;

static void app_config_erase(void)
{
	nrf_power_gpregret_set(GPREGRET_ERASE_FLASH1);
	nrf_power_gpregret2_set(GPREGRET_ERASE_FLASH2);
	
	NVIC_SystemReset();
}

uint32_t app_config_init(void)
{
	uint32_t err_code;
	
	do
	{		
		err_code = app_config_flash_init(&def_app_config, &m_app_config);
		NRF_LOG_INFO("int config : %d", err_code);
		BREAK_IF_ERROR(err_code);
	
		fds_gc();
		
		if (!cal_is_valid(&m_app_config->cal))
		{
			trace("Date time error");
			break;
		}
		
		// 2019.12.27 GC가 돌지 않으면  52810에서 NO_SPACE error
		fds_gc();
		
		return NRF_SUCCESS;
	}
	while (0);

	do
	{
		// 2019.12.27 GC가 돌지 않으면  52810에서 NO_SPACE error
		fds_gc();
		
		err_code = app_config_reset();
		BREAK_IF_ERROR(err_code);
		
		err_code = app_config_flash_renew(m_app_config);
		BREAK_IF_ERROR(err_code);
		
		err_code = app_config_save();
		BREAK_IF_ERROR(err_code);
		
		fds_gc();
	} while (0);
	
	if (err_code != NRF_SUCCESS)
	{
		trace("Unrecoverable error");
		app_config_erase();
	}
		
	return err_code;
}

uint32_t app_config_save(void)
{
	uint32_t err_code;
		
	err_code = app_config_flash_config_store(m_app_config);
	RETURN_IF_ERROR(err_code);
	
	//NRF_LOG_INFO("save config : %d", err_code);
	
	fds_gc();
	
	return err_code;
}

uint32_t app_config_reset(void)
{
	NRF_LOG_INFO("reset config");
	
	memcpy(m_app_config, &def_app_config, sizeof(app_config_t));
	
	// set bluetooth mac address to eth
	ble_gap_addr_t addr;
	sd_ble_gap_addr_get(&addr);
	
	return NRF_SUCCESS;
}

