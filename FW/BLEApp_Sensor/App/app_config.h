#ifndef _app_config_h
#define _app_config_h

#include "app_config_flash.h"
#include "nrf_sdm.h"

uint32_t app_config_init(void);
uint32_t app_config_reset(void);
uint32_t app_config_save(void);

extern app_config_t* m_app_config;

#endif // _app_config_h
