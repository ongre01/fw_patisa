#include "app_config.h"
#include "config.h"
#include "nrf_uart.h"

const app_config_t def_app_config = {
	.cal = {
		.year = 2023, .month=1, .day=1, .hour=0, .min=0, .sec=0,
	},
};
