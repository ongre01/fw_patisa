#include "app_config_flash.h"
#include "config.h"
#include "nrf_fstorage.h"
#include "fds.h"
#include "app_scheduler.h"
#include "m_event_queue.h"
#include "crc32.h"

#define APP_CONFIG_FLASH_LOG_ENABLED
#ifndef APP_CONFIG_FLASH_LOG_ENABLED
	#define NRF_LOG_LEVEL 0
#endif

#define NRF_LOG_MODULE_NAME APP_CFG_FLASH
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

#define APP_CONFIG_FLASH_CONFIG_VALID 0x42UL
#define APP_CONFIG_FILE_ID 0x3000
#define APP_CONFIG_REC_KEY 0x3001

#define APP_CONFIG_CERT_ID  0x3100
#define APP_CONFIG_CERT_KEY 0x3101

/**@brief Data structure of configuration data stored to flash.
 */
typedef struct
{
    uint32_t     valid;
	uint32_t	 crc;
    app_config_t config;
} app_config_flash_data_t;

/**@brief Configuration data with size.
 */
typedef union
{
    app_config_flash_data_t data;
    uint32_t                padding[CEIL_DIV(sizeof(app_config_flash_data_t), 4)];
} app_config_flash_config_t;

static fds_record_desc_t       		m_record_desc;
static app_config_flash_config_t 	m_config;
static bool                    		m_fds_initialized = false;
static bool                    		m_fds_write_success = false;

/**@brief Function for handling flash data storage events.
 */
void app_config_fds_evt_handler( fds_evt_t const * const p_fds_evt )
{
    switch (p_fds_evt->id)
    {
        case FDS_EVT_INIT:
            if (p_fds_evt->result == FDS_SUCCESS)
            {
                m_fds_initialized = true;
            }
            else
            {
                // Initialization failed.
                NRF_LOG_ERROR("FDS init failed!");
                APP_ERROR_CHECK_BOOL(false);
            }
            break;
        case FDS_EVT_WRITE:
			if (p_fds_evt->write.file_id == APP_CONFIG_FILE_ID)
				pushEvent2_force(EVT_flash_result, p_fds_evt->id, p_fds_evt->result);
            if (p_fds_evt->result == FDS_SUCCESS)
            {
                if (p_fds_evt->write.file_id == APP_CONFIG_FILE_ID)
                {
                    NRF_LOG_INFO("FDS write success! %d FileId: 0x%x RecKey:0x%x", p_fds_evt->write.is_record_updated,
                                                                                                   p_fds_evt->write.file_id,
                                                                                                   p_fds_evt->write.record_key);
                    m_fds_write_success = true;
                }
            }
            else
            {
                // Initialization failed.
                NRF_LOG_ERROR("FDS write failed!");
                APP_ERROR_CHECK_BOOL(false);
            }
            break;
		case FDS_EVT_UPDATE:
			if (p_fds_evt->write.file_id == APP_CONFIG_FILE_ID)
				pushEvent2_force(EVT_flash_result, p_fds_evt->id, p_fds_evt->result);
			break;
        default:
            NRF_LOG_DEBUG("FDS handler : %d - %d", p_fds_evt->id, p_fds_evt->result);
            APP_ERROR_CHECK(p_fds_evt->result);
            break;
    }
}

uint32_t app_config_flash_config_store(app_config_t * p_config)
{
    uint32_t            err_code;
    fds_record_t        record;

    NULL_PARAM_CHECK(p_config);

	if (p_config != &m_config.data.config)
		memcpy(&m_config.data.config, p_config, sizeof(app_config_t));
    m_config.data.valid = APP_CONFIG_FLASH_CONFIG_VALID;
	m_config.data.crc   = crc32_compute((uint8_t*)&m_config.data.config, sizeof(app_config_t), NULL);

    // Set up record.
    record.file_id              = APP_CONFIG_FILE_ID;
    record.key                  = APP_CONFIG_REC_KEY;
    record.data.p_data          = &m_config;
    record.data.length_words    = sizeof(app_config_flash_config_t)/4;

    err_code = fds_record_update(&m_record_desc, &record);
    RETURN_IF_ERROR(err_code);

    return NRF_SUCCESS;
}


uint32_t app_config_flash_config_load(app_config_t ** p_config)
{
    uint32_t            err_code;
	uint32_t            crc;

    fds_flash_record_t  flash_record;
    fds_find_token_t    ftok;

    memset(&ftok, 0x00, sizeof(fds_find_token_t));

    NRF_LOG_INFO("Loading configuration");

    err_code = fds_record_find(APP_CONFIG_FILE_ID, APP_CONFIG_REC_KEY, &m_record_desc, &ftok);
    RETURN_IF_ERROR(err_code);

    err_code = fds_record_open(&m_record_desc, &flash_record);
    RETURN_IF_ERROR(err_code);

    fds_header_t const * const p_header = (fds_header_t*)m_record_desc.p_record;
	if (p_header->length_words != sizeof(app_config_flash_config_t) / 4)
	{
		trace("Invalid config data words");
		fds_record_close(&m_record_desc);
		return NRF_ERROR_INVALID_LENGTH;
	}
	
    memcpy(&m_config, flash_record.p_data, sizeof(app_config_flash_config_t));

    err_code = fds_record_close(&m_record_desc);
    RETURN_IF_ERROR(err_code);

	if (m_config.data.valid != APP_CONFIG_FLASH_CONFIG_VALID)
	{
		trace("FLASH Valid code error");
		return NRF_ERROR_INVALID_DATA;
	}
	
	crc = crc32_compute((uint8_t*)&m_config.data.config, sizeof(app_config_t), NULL);
	if (crc != m_config.data.crc)
	{
		trace("CRC error");
		return NRF_ERROR_INVALID_DATA;
	}
	
    *p_config = &m_config.data.config;

    return NRF_SUCCESS;
}

uint32_t app_config_flash_renew(app_config_t* p_config)
{
	uint32_t err_code;
	
	// remove previous config if found
	fds_find_token_t ftok;
	memset(&ftok, 0x00, sizeof(fds_find_token_t));
	err_code = fds_record_find(APP_CONFIG_FILE_ID, APP_CONFIG_REC_KEY, &m_record_desc, &ftok);
	if (err_code == NRF_SUCCESS)
	{
		trace("Remove dirty record");
		fds_record_delete(&m_record_desc);
	}
					
	fds_record_t        record;

	NRF_LOG_INFO("Write default config");

	if (p_config != &m_config.data.config)
		memcpy(&m_config.data.config, p_config, sizeof(app_config_t));
	m_config.data.valid = APP_CONFIG_FLASH_CONFIG_VALID;
	m_config.data.crc   = crc32_compute((uint8_t*)&m_config.data.config, sizeof(app_config_t), NULL);

	// Set up record.
	record.file_id              = APP_CONFIG_FILE_ID;
	record.key                  = APP_CONFIG_REC_KEY;
	record.data.p_data          = &m_config;
	record.data.length_words    = sizeof(app_config_flash_config_t)/4;
	
	err_code = fds_record_write(&m_record_desc, &record);
	return err_code;
}

uint32_t app_config_flash_init(const app_config_t * p_default_config,
                               app_config_t      ** p_config)
{
    uint32_t                err_code;

    NRF_LOG_INFO("Initialization");
    NULL_PARAM_CHECK(p_default_config);

    err_code = fds_register(app_config_fds_evt_handler);
    RETURN_IF_ERROR(err_code);

    err_code = fds_init();
    RETURN_IF_ERROR(err_code);

    while (m_fds_initialized == false)
    {
        app_sched_execute();
		vTaskDelay(1);
    }

    err_code = app_config_flash_config_load(p_config);
    if (err_code != NRF_SUCCESS)
    {
		// remove previous config if found
		fds_find_token_t ftok;
		memset(&ftok, 0x00, sizeof(fds_find_token_t));
		err_code = fds_record_find(APP_CONFIG_FILE_ID, APP_CONFIG_REC_KEY, &m_record_desc, &ftok);
		if (err_code == NRF_SUCCESS)
		{
			trace("Remove dirty record");
			fds_record_delete(&m_record_desc);
		}
					
        fds_record_t        record;

        NRF_LOG_INFO("Writing default config");

        memcpy(&m_config.data.config, p_default_config, sizeof(app_config_t));
        m_config.data.valid = APP_CONFIG_FLASH_CONFIG_VALID;
		m_config.data.crc   = crc32_compute((uint8_t*)&m_config.data.config, sizeof(app_config_t), NULL);

        // Set up record.
        record.file_id              = APP_CONFIG_FILE_ID;
        record.key                  = APP_CONFIG_REC_KEY;
        record.data.p_data          = &m_config;
        record.data.length_words    = sizeof(app_config_flash_config_t)/4;
		
        m_fds_write_success = false;
        err_code = fds_record_write(&m_record_desc, &record);
        RETURN_IF_ERROR(err_code);

        *p_config = &m_config.data.config;

        while(m_fds_write_success != true)
        {
            app_sched_execute();
			vTaskDelay(1);
       }
    }
    else
    {
        RETURN_IF_ERROR(err_code);
    }

    return NRF_SUCCESS;
}

void app_config_flash_fds_erase(void)
{
	fds_erase();
}


