#ifndef _app_config_flash_h
#define _app_config_flash_h

#include "sdk_common.h"
#include "nrf_section.h"
#include "macros_common.h"
#include "rtc_timer.h"
#include "nrf_serial.h"
#include "ble_gap.h"

#pragma pack(1)

#define APP_CONFIG_MODEL_SIZE() (sizeof(app_config_model_t) - (MAX_INVERTER - m_app_config->model.inv_reg_count)*sizeof(inverter_reg_t))

typedef struct
{	
	calendar_t		cal;
} app_config_t;

#pragma pack()

uint32_t app_config_flash_config_store(app_config_t * p_config);
uint32_t app_config_flash_config_load(app_config_t ** p_config);
uint32_t app_config_flash_renew(app_config_t* p_config);
uint32_t app_config_flash_init(const app_config_t * p_default_config,
                               app_config_t      ** p_config);
void app_config_flash_fds_erase(void);

#endif // _app_config_flash_h
