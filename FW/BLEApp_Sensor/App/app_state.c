#include "app_state.h"
#include "app_config.h"
#include "ble_nus.h"
#include "nrf_power.h"
#include "config.h"
#include "nrf_drv_uart.h"

#define APP_STATE_LOG_ENABLED //test
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
#ifndef APP_STATE_LOG_ENABLED
	#define NRF_LOG_LEVEL 0
#endif

#define NRF_LOG_MODULE_NAME APP_STATE
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();


//////////////////////////////////////////////////////////////////////////////////
// Helper macros for section variables.
#define SYSTEM_STATE_SECTION_ITEM_GET(i)      NRF_SECTION_ITEM_GET(system_state_data, system_state_config_t, (i))
#define SYSTEM_STATE_SECTION_ITEM_COUNT       NRF_SECTION_ITEM_COUNT(system_state_data, system_state_config_t)
#define SYSTEM_STATE_SECTION_START_ADDR       NRF_SECTION_START_ADDR(system_state_data)
#define SYSTEM_STATE_SECTION_END_ADDR         NRF_SECTION_END_ADDR(system_state_data)

// Create section 'power__data'.
NRF_SECTION_DEF(system_state_data, system_state_config_t);

//////////////////////////////////////////////////////////////////////////////////
// application state variable
app_state_t m_app_state = {
	.app_flag     = 0,
    .ble_nus_max_data_len = BLE_NUS_MAX_DATA_LEN,
};

void app_state_init(void)
{
	m_app_state.gpregret[0] = NRF_POWER->GPREGRET;
	m_app_state.gpregret[1] = NRF_POWER->GPREGRET2;
	NRF_POWER->GPREGRET  = 0;
	NRF_POWER->GPREGRET2 = 0;
	
	m_app_state.reset_reason = NRF_POWER->RESETREAS;
	nrf_power_resetreas_clear(m_app_state.reset_reason);
	
	if (m_app_state.reset_reason & POWER_RESETREAS_RESETPIN_Msk)
	{
		trace("*** Reset PIN");
	}
	if (m_app_state.reset_reason & POWER_RESETREAS_DOG_Msk)
	{
		trace("Reset WatchDog");
	}
	if (m_app_state.reset_reason & POWER_RESETREAS_SREQ_Msk)
	{
		trace("*** Softreset");
	}
	if (m_app_state.reset_reason & POWER_RESETREAS_LOCKUP_Msk)
	{
		trace("*** Reset by Lockup");
	}
	if (m_app_state.reset_reason & POWER_RESETREAS_OFF_Msk)
	{
		trace("*** Off");
	}
#ifdef POWER_RESETREAS_LPCOMP_Msk	// nRF52810 doesn't support
	if (m_app_state.reset_reason & POWER_RESETREAS_LPCOMP_Msk)
	{
		trace("*** LPCOMP mode");
	}
#endif
	if (m_app_state.reset_reason & POWER_RESETREAS_DIF_Msk)
	{
		trace("*** Wakeup debug interface mode");
	}
#ifdef POWER_RESETREAS_NFC_Msk		// nRF52810 doesn't support
	if (m_app_state.reset_reason & POWER_RESETREAS_NFC_Msk)
	{
		trace("*** NFC Wakeup");
	}
#endif	
}

static void notify_system_state(void)
{
    uint32_t const config_count = SYSTEM_STATE_SECTION_ITEM_COUNT;
	
	for (uint32_t i=0; i<config_count; i++)
	{
		system_state_config_t const * const p_config = SYSTEM_STATE_SECTION_ITEM_GET(i);
		if (p_config->change_handler)
			p_config->change_handler(m_app_state.system_state);
	}
}

system_state_t app_state_get_system_state(void)
{
	return m_app_state.system_state;
}

// set system state to recipients
void app_state_set_system_state(system_state_t new_state)
{	
#if NRF_MODULE_ENABLED(NRF_LOG)
	if (m_app_state.system_state != new_state)
	{
		switch (new_state)
		{
			case system_state_power_off : 	///< No input, 				no adv (bat is under 3.4v)
				NRF_LOG_INFO("System State = Power off\n"); 	break;
			case system_state_idle 		:	///< Home button wake-up, 	no adv
				NRF_LOG_INFO("System State = Idle\n"); 		break;
			case system_state_sleep 	:	///< Home / Select, 		adv
				NRF_LOG_INFO("System State = Sleep\n"); 		break;
			case system_state_active 	:	///< All input,				adv
				NRF_LOG_INFO("System State = Active\n"); 		break;
			default :
				NRF_LOG_INFO("System State = *** Unknown\n"); 	break;
		}
	}
#endif	
	
	m_app_state.system_state = new_state;	
	notify_system_state();
}

uint32_t convert_2_nrf_baud_rate(uint32_t baud)
{
	uint32_t value = 0;
	switch (baud)
	{
		case    1200 : value = NRF_UART_BAUDRATE_1200;  break;
		case    2400 : value = NRF_UART_BAUDRATE_2400 ; break;
		case    4800 : value = NRF_UART_BAUDRATE_4800 ; break;
		case    9600 : value = NRF_UART_BAUDRATE_9600 ; break;
		case   14400 : value = NRF_UART_BAUDRATE_14400; break;
		case   19200 : value = NRF_UART_BAUDRATE_19200; break;
		case   28800 : value = NRF_UART_BAUDRATE_28800; break;
		case   31250 : value = NRF_UART_BAUDRATE_31250; break;
		case   38400 : value = NRF_UART_BAUDRATE_38400; break;
		case   56000 : value = NRF_UART_BAUDRATE_56000; break;
		case   57600 : value = NRF_UART_BAUDRATE_57600; break;
		case   76800 : value = NRF_UART_BAUDRATE_76800; break;
		case  115200 : value = NRF_UART_BAUDRATE_115200 ; break;
		case  230400 : value = NRF_UART_BAUDRATE_230400 ; break;
		case  250000 : value = NRF_UART_BAUDRATE_250000 ; break;
		case  460800 : value = NRF_UART_BAUDRATE_460800 ; break;
		case  921600 : value = NRF_UART_BAUDRATE_921600 ; break;
		case 1000000 : value = NRF_UART_BAUDRATE_1000000; break;
	}
	
	return value;
}

uint32_t convert_2_natural_baud_rate(uint32_t baud)
{
	uint32_t value = 0;
	switch (baud)
	{
		case NRF_UART_BAUDRATE_1200    : value =    1200;  break;
		case NRF_UART_BAUDRATE_2400    : value =    2400; break;
		case NRF_UART_BAUDRATE_4800    : value =    4800; break;
		case NRF_UART_BAUDRATE_9600    : value =    9600; break;
		case NRF_UART_BAUDRATE_14400   : value =   14400; break;
		case NRF_UART_BAUDRATE_19200   : value =   19200; break;
		case NRF_UART_BAUDRATE_28800   : value =   28800; break;
		case NRF_UART_BAUDRATE_31250   : value =   31250; break;
		case NRF_UART_BAUDRATE_38400   : value =   38400; break;
		case NRF_UART_BAUDRATE_56000   : value =   56000; break;
		case NRF_UART_BAUDRATE_57600   : value =   57600; break;
		case NRF_UART_BAUDRATE_76800   : value =   76800; break;
		case NRF_UART_BAUDRATE_115200  : value =  115200; break;
		case NRF_UART_BAUDRATE_230400  : value =  230400; break;
		case NRF_UART_BAUDRATE_250000  : value =  250000; break;
		case NRF_UART_BAUDRATE_460800  : value =  460800; break;
		case NRF_UART_BAUDRATE_921600  : value =  921600; break;
		case NRF_UART_BAUDRATE_1000000 : value = 1000000; break;
	}
	
	return value;
}

