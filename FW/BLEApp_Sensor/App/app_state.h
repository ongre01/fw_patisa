#ifndef _app_state_h
#define _app_state_h

#include "m_ble.h"
#include "nrf_section.h"
#include "ble_advertising.h"
#include "rtc_timer.h"
#include "bsp.h"
#include "ble_gap.h"
#include "app_beacon_info.h"
#include "app_packet.h"
#include "config.h"

// register system state section
#define SYSTEM_STATE_REGISTER_CFG(cfg_var) NRF_SECTION_ITEM_REGISTER(system_state_data, cfg_var)

// define system state
typedef enum
{
	system_state_power_off,		///< No input, 				no adv (bat is under 3.4v)
	system_state_idle,			///< Home button wake-up, 	adv
	system_state_sleep,			///< Home / Select, 		adv
	system_state_active,		///< All input,				adv
} system_state_t;

///////////////////////////////////////////////////////////////////////////////
typedef void (*system_state_handler_t)(system_state_t system_state);
typedef void (*system_state_evt_handler_t)(const T_Event* const e);
typedef struct
{
	system_state_handler_t 		change_handler;
	system_state_evt_handler_t	evt_handler;
} system_state_config_t;

///////////////////////////////////////////////////////////////////////////////
typedef enum
{
	af_none    			= 0x0000,		// none flag
	//---------- Configuration
	af_config_changed  	= 0x0001,
	af_config_system 	= 0x0002,
	af_config_uart 		= 0x0004,
	af_config_uart_485	= 0x0008,
	af_config_bluetooth = 0x0010,
	af_config_device 	= 0x0020,
	af_config_dongle 	= 0x0040,
	af_config_central   = 0x0080,
	af_config_all       = (af_config_system|af_config_bluetooth|af_config_device|af_config_dongle),
	//---------- State
	af_adv				= 0x0100,			// advertise or not
	af_adv_stop			= 0x0200,			// stop adverting flag
	af_connected		= 0x0400, 			// app is connected or not
	af_reset_req		= 0x0800,			// reset request
	af_echo				= 0x1000,
	//---------- USB Dongle
	af_nus_c_connect	= 0x2000,			// nus client connection
	af_acm_open	     	= 0x4000,
	af_force_reset      = 0x8000,			// force system reset
} app_flag_t;

#define APP_FLAG_IS_SET(flag) 	((m_app_state.app_flag & (flag))?true:false)
#define APP_FLAG_IS_CLR(flag) 	(!APP_FLAG_IS_SET(flag))
#define APP_FLAG_SET(flag) 		(m_app_state.app_flag |= (flag))
#define APP_FLAG_CLR(flag) 		(m_app_state.app_flag &= (~(flag)))

#define GPREGRET_ERASE_FLASH1	0x45
#define GPREGRET_ERASE_FLASH2	0x46

///////////////////////////////////////////////////////////////////////////////
ANON_UNIONS_ENABLE;

typedef struct
{
	uint32_t			gpregret[2];
	uint32_t			app_flag;					///< Application flag
	uint32_t			reset_reason;				///< Reset reason
	system_state_t		system_state;				///< System power state
	ble_adv_evt_t		adv_state;					///< Advertising state
    	
	timestamp_t			timestamp;					// current timestmap
	ble_gap_addr_t      my_addr;
	ble_gap_addr_t		timeslot_addr;
	
	uint16_t			ble_nus_max_data_len;
	timestamp_t			config_save_time;
	
	//app_beacon_info_t   bi;
} app_state_t;
ANON_UNIONS_DISABLE;

void app_state_init(void);
system_state_t app_state_get_system_state(void);
void app_state_set_system_state(system_state_t new_state);
uint32_t app_state_get_setting(void);

uint32_t convert_2_nrf_baud_rate(uint32_t baud);
uint32_t convert_2_natural_baud_rate(uint32_t baud);

extern app_state_t 	    m_app_state;

#endif // _app_state_h
