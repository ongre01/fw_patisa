#include "config.h"
#include "bsp.h"
#include "app_scheduler.h"
#include "m_event_queue.h"
#include "macros_common.h"
#include "drv_max17330.h"
#include "nrf_pwr_mgmt.h"

#define NRF_LOG_MODULE_NAME APP_THREAD

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();

#include "m_ble.h"
#include "nrf_sdh_freertos.h"
#include "app_state.h"
#include "app_config.h"
#include "rtc_timer.h"

#include "m_usb_com.h"
#include "app_scanner.h"
#include "m_ad5940.h"

static xTaskHandle      hAppThread;
static void app_thread(void* arg);

APP_TIMER_DEF(m_config_timer);

static void on_config_save_timer_handler(void* p_context);

// create application specific threads
uint32_t app_thread_create(void)
{
    uint32_t err_code;

    do
    {
        // event thread
        err_code = m_event_queue_create();
        BREAK_IF_ERROR(err_code);


        nrf_sdh_freertos_init(NULL, NULL);

		// Create App thread
		if (pdPASS != xTaskCreate(app_thread, "App", STACK_SIZE_APP_THREAD, NULL, 1, &hAppThread))
			return NRF_ERROR_NO_MEM;
	
		// create config save timer
		app_timer_create(&m_config_timer, APP_TIMER_MODE_SINGLE_SHOT, on_config_save_timer_handler);
		
        return NRF_SUCCESS;
    } while (0);

    return NRF_ERROR_NO_MEM;
}
 
__weak void app_thread_idle(void)
{
	// WDT feed by main application
}

static void app_thread(void* arg)
{
	for (;;)
	{
		app_sched_execute();
		
		app_thread_idle();
		
		vTaskDelay(1);
	}
}

//////////////////////////////////////////////////////////////////////
// calendar handler
static void calendar_change_handler(calendar_t cal)
{
	// save current timestamp
	m_app_state.timestamp = cal_get_timestamp(&cal);
}

// this function is called when the calendar min has been changed
static void calendar_min_change_handler(calendar_t cal)
{
	trace("%04d-%02d-%02d %02d:%02d:%02d", 
		cal.year, cal.month, cal.day, cal.hour, cal.min, cal.sec);

	// save data & time
	m_app_config->cal = cal;
	
	//pushEvent0_unforce(EVT_config_changed);
}

////////////////////////////////////////////////////////////////////////////

static void on_app_ready(void)
{
	// initialize config
	app_config_init();
	NRF_LOG_FLUSH();
	
	//m_ble_adv_name_update();
	
	// initialize calendar
	const calendar_init_t cal_init = 
	{
		.cur 					= m_app_config->cal,
		.cal_change_handler		= calendar_change_handler,
		.cal_min_change_handler	= calendar_min_change_handler,
		
	};

	cal_start(&cal_init);

	bsp_indication_set(BSP_INDICATE_READY);
	
	// init ad5941 adc module
	m_ad5940_init();
}

static void on_config_save_timer_handler(void* p_context)
{
	NRF_LOG_INFO("---- Save config ----");
	app_config_save();
}

static void on_config_changed(void)
{
	app_timer_stop(m_config_timer);
	app_timer_start(m_config_timer, APP_TIMER_TICKS(CONFIG_SAVE_DELAY), NULL);
}

static void on_config_reset(void)
{
	NRF_LOG_INFO("---- RESET config ----");
	app_config_reset();
	app_config_save();
	vTaskDelay(pdMS_TO_TICKS(1000));
	NVIC_SystemReset();
}

static void on_reset(void)
{
	NRF_LOG_INFO("---- Reboot ----");
	vTaskDelay(pdMS_TO_TICKS(1000));
	NVIC_SystemReset();
}

static void off_power(void)
{
	NRF_LOG_INFO("--- Power Off ---");
	//max17330_ship_enable();
	max17330_fet_off();
	//nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_SYSOFF);
}

static void on_sleep(void)
{
	NRF_LOG_INFO("------- Power off ---------");
}

static void evt_handler(T_Event const* evt, void* p_context)
{
    switch (evt->event)
    {
    case EVT_app_ready      : on_app_ready();   	break;
	case EVT_go_sleep       : on_sleep(); 			break;
	case EVT_reset_config   : on_config_reset();    break;
	case EVT_reset          : on_reset();           break;
	case EVT_save_config    :
	case EVT_config_changed : on_config_changed(); 	break;
	case EVT_long_click     : off_power();  break;
    }
}

EVENT_QUEUE_OBSERVER(evt, M_APP_EVENT_PRIORITIES) =
{
    .evt_handler = evt_handler,
    .p_context = NULL,
};
