#include "sdk_common.h"
#include "config.h"
#include "nrf_pwr_mgmt.h"
#include "fds.h"
#include "nrf_drv_clock.h"
#include "nrf_soc.h"
#include "nrf_sdh.h"
#include "nrf_sdh_freertos.h"
#include "nrf_sdm.h"
#include "nrf_power.h"
#include "nrf_drv_ppi.h"
#include "nrf_drv_wdt.h"
#include "nrf_serial.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

#include "stdlib.h"

#include "app_timer.h"
#include "nrf_serial.h"
#include "app_util_platform.h"
#include "app_scheduler.h"
#include "app_gpiote.h"
#if defined (UART_PRESENT)
#include "nrf_uart.h"
#endif
#if defined (UARTE_PRESENT)
#include "nrf_uarte.h"
#endif

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrf_delay.h"

#include "m_ble.h"
#include "m_nus.h"
#include "m_dfu.h"
#include "m_usb_com.h"

#include "app_state.h"
#include "app_config.h"
#include "rtc_timer.h"
#include "helper.h"
#include "SEGGER_RTT.h"
#include "macros_common.h"
#include "crc32.h"
#include "app_threads.h"
#include "drv_max17330.h"

static bool app_shutdown_handler(nrf_pwr_mgmt_evt_t event);
static bool	ble_service_data_callback(ble_advdata_service_data_t* p_data);
static void ble_stack_was_initialized(void);
static void on_timeout_handler(void* p_context);

NRF_PWR_MGMT_HANDLER_REGISTER(app_shutdown_handler, 0);

static m_ble_service_handle_t  	m_ble_service_handles[BLE_SERVICES_MAX];
#ifndef DEBUG
static nrf_drv_wdt_channel_id 	m_channel_id;
#endif

#if NRF_LOG_ENABLED && NRF_LOG_DEFERRED
static TaskHandle_t m_logger_thread;                                
#endif

#define FPU_EXCEPTION_MASK 0x0000009F

APP_TIMER_DEF(m_timer);

void FPU_IRQHandler(void)
{
    uint32_t *fpscr = (uint32_t *)(FPU->FPCAR+0x40);
    (void)__get_FPSCR();

    *fpscr = *fpscr & ~(FPU_EXCEPTION_MASK);
}

#if !defined(DEBUG)
void app_error_fault_handler(uint32_t id, uint32_t pc, uint32_t info)
{
	NRF_LOG_FLUSH();
	NRF_LOG_ERROR("*** Fault : %d, 0x%05x, %d", id, pc, info);
	NRF_LOG_PROCESS();
    NVIC_SystemReset();
}
#endif

void HardFault_Handler(uint32_t * p_stack_address)
{
	NRF_LOG_FLUSH();
	NRF_LOG_ERROR("*** Hard Fault : 0x%05x", p_stack_address);
	NRF_LOG_PROCESS();
    NVIC_SystemReset();
}

void vApplicationMallocFailedHook(void)
{
	NRF_LOG_INFO("Malloc failed");
	NRF_LOG_PROCESS(); 
	
    NVIC_SystemReset();
}

void vApplicationStackOverflowHook(void)
{
	NRF_LOG_INFO("Stack overflow");
	NRF_LOG_PROCESS();
	
    NVIC_SystemReset();
}

static void on_prepare_sysoff(void)
{
	uint32_t err_code;
	
	UNUSED_VARIABLE(err_code);
	
	nrf_drv_ppi_uninit();
}

#if NRF_LOG_ENABLED && NRF_LOG_DEFERRED
// Thread for handling the logger.
// This thread is responsible for processing log entries if logs are deferred.
//          Thread flushes all log entries and suspends. It is resumed by idle task hook.
//
// param[in]   arg   Pointer used for passing some arbitrary information (context) from the
//                    osThreadCreate() call to the thread.
static void logger_thread(void * arg)
{
    UNUSED_PARAMETER(arg);

    while (1)
    {
        NRF_LOG_FLUSH();
        vTaskSuspend(NULL); // Suspend myself
    }
}

#endif //NRF_LOG_ENABLED && NRF_LOG_DEFERRED


// Handler for shutdown preparation.
// During shutdown procedures, this function will be called at a 1 second interval
// untill the function returns true. When the function returns true, it means that the
// app is ready to reset to DFU mode.
static bool app_shutdown_handler(nrf_pwr_mgmt_evt_t event)
{
	NRF_LOG_FLUSH();
	
    switch (event)
    {
			case NRF_PWR_MGMT_EVT_PREPARE_DFU:
				NRF_LOG_INFO("Power management wants to reset to DFU mode.");
				break;
			case NRF_PWR_MGMT_EVT_PREPARE_WAKEUP : 
			  NRF_LOG_INFO("Power management prepares to wake_up");
			  bsp_sleep_mode_prepare();
				break;
			case NRF_PWR_MGMT_EVT_PREPARE_SYSOFF:
				on_prepare_sysoff();
				break;
			case NRF_PWR_MGMT_EVT_PREPARE_RESET:
				break;
			default:
            // YOUR_JOB: Implement any of the other events available from the power management module:
            //      -NRF_PWR_MGMT_EVT_PREPARE_WAKEUP
            return true;
    }

	NRF_LOG_FLUSH();
	
    return true;
}


void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

static void scheduler_init(void)
{
    APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);

    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}


static void buttons_leds_init(void)
{
    uint32_t err_code;

	err_code = bsp_init();
    APP_ERROR_CHECK(err_code);
}


static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}

static void clock_init(void)
{
    ret_code_t err_code = nrf_drv_clock_init();
    APP_ERROR_CHECK(err_code);
}

static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}

#ifndef DEBUG
void wdt_event_handler(void)
{
    NRF_LOG_INFO("WDT event");
}

static void wdt_init(void)
{
	uint32_t err_code;
	
	nrf_drv_wdt_config_t config = NRF_DRV_WDT_DEAFULT_CONFIG;
	config.behaviour 	= NRF_WDT_BEHAVIOUR_RUN_SLEEP_HALT;	// run in sleep / halt
	config.reload_value = DEF_WDT_RELOAD_MS;
    err_code = nrf_drv_wdt_init(&config, wdt_event_handler);
    APP_ERROR_CHECK(err_code);
	
    err_code = nrf_drv_wdt_channel_alloc(&m_channel_id);
    APP_ERROR_CHECK(err_code);
	
    nrf_drv_wdt_enable();	
}
#endif


static void gpio_init(void)
{
	// enable VDD 3.3
	nrf_gpio_cfg_output(VDD_3V3_EN);
	nrf_gpio_pin_set(VDD_3V3_EN);
}

static void on_max17330_alert_handler(drv_max17330_evt_t evt)
{
	m_usb_com_write_line_fmt("\nStatus : 0x%04x chg_type : 0x%04x", evt.status, evt.charging_type);
	
	if (evt.status & MAX1720X_STATUS_PROTECTION_ALRT) {
		m_usb_com_write_line("Alert: Protection!");

		if (evt.protection_alert & MAX17330_PROTSTATUS_CHGWDT)
			m_usb_com_write_line("\tProtection Alert: Charge Watch Dog Timer!");
		if (evt.protection_alert & MAX17330_PROTSTATUS_TOOHOTC)
			m_usb_com_write_line("\tProtection Alert: Overtemperature for Charging!");
		if (evt.protection_alert & MAX17330_PROTSTATUS_FULL)
			m_usb_com_write_line("\tProtection Alert: Full Detection!");
		if (evt.protection_alert & MAX17330_PROTSTATUS_TOOCOLDC)
			m_usb_com_write_line("\tProtection Alert: Undertemperature!");
		if (evt.protection_alert & MAX17330_PROTSTATUS_OVP)
			m_usb_com_write_line("\tProtection Alert: Overvoltage!");
		if (evt.protection_alert & MAX17330_PROTSTATUS_OCCP)
			m_usb_com_write_line("\tProtection Alert: Overcharge Current!");
		if (evt.protection_alert & MAX17330_PROTSTATUS_QOVFLW)
			m_usb_com_write_line("\tProtection Alert: Q Overflow!");
		if (evt.protection_alert & MAX17330_PROTSTATUS_PREQF)
			m_usb_com_write_line("\tProtection Alert: Prequal timeout!");
		if (evt.protection_alert & MAX17330_PROTSTATUS_BLOCKCHG)
			m_usb_com_write_line("\tProtection Alert: Block change!");
		if (evt.protection_alert & MAX17330_PROTSTATUS_PERMFAIL)
			m_usb_com_write_line("\tProtection Alert: Permanent Failure!");
		if (evt.protection_alert & MAX17330_PROTSTATUS_DIEHOT)
			m_usb_com_write_line("\tProtection Alert: Overtemperature for die temperature!");
		if (evt.protection_alert & MAX17330_PROTSTATUS_TOOHOTD)
			m_usb_com_write_line("\tProtection Alert: Overtemperature for Discharging!");
		if (evt.protection_alert & MAX17330_PROTSTATUS_UVP)
			m_usb_com_write_line("\tProtection Alert: Undervoltage Protection!");
		if (evt.protection_alert & MAX17330_PROTSTATUS_ODCP)
			m_usb_com_write_line("\tProtection Alert: Overdischarge current!");
		if (evt.protection_alert & MAX17330_PROTSTATUS_TOOCOLDD)
			m_usb_com_write_line("\tProtection Alert: Undertemperature for Discharging!");
		if (evt.protection_alert & MAX17330_PROTSTATUS_SHDN)
			m_usb_com_write_line("\tProtection Alert: Shutdown Event !");
	}

	if (evt.status & MAX1720X_STATUS_SOC_MAX_ALRT)
		m_usb_com_write_line("Alert: SOC MAX!");
	if (evt.status & MAX1720X_STATUS_SOC_MIN_ALRT)
		m_usb_com_write_line("Alert: SOC MIN!");
	if (evt.status & MAX1720X_STATUS_TEMP_MAX_ALRT)
		m_usb_com_write_line("Alert: TEMP MAX!");
	if (evt.status & MAX1720X_STATUS_TEMP_MIN_ALRT)
		m_usb_com_write_line("Alert: TEMP MIN!");
	if (evt.status & MAX1720X_STATUS_VOLT_MAX_ALRT)
		m_usb_com_write_line("Alert: VOLT MAX!");
	if (evt.status & MAX1720X_STATUS_VOLT_MIN_ALRT)
		m_usb_com_write_line("Alert: VOLT MIN!");
	if (evt.status & MAX1720X_STATUS_CURR_MAX_ALRT)
		m_usb_com_write_line("Alert: CURR MAX!");
	if (evt.status & MAX1720X_STATUS_CURR_MIN_ALRT)
		m_usb_com_write_line("Alert: CURR MIN!");

	if (evt.status & MAX1720X_STATUS_CHARGING_ALRT) {
		m_usb_com_write_line("Alert: CHARGING!");
		if (evt.charging_type & MAX17330_CHGSTAT_CP)
			m_usb_com_write_line("\tCharging Alert: Heat limit!");
		if (evt.charging_type & MAX17330_CHGSTAT_CT)
			m_usb_com_write_line("\tCharging Alert: FET Temperature limit!");
		if (evt.charging_type & MAX17330_CHGSTAT_DROPOUT)
			m_usb_com_write_line("\tCharging Alert: Dropout!");
	}
	
	if (evt.charging_type & MAX17330_CHGSTAT_CV)
		m_usb_com_write_line("\tCHR Mode: Const. Volt!");
	if (evt.charging_type & MAX17330_CHGSTAT_CC)
		m_usb_com_write_line("\tCHR Mode: Const. Curr!");			

}

static uint32_t max17330_dev_init(void)
{
	static const nrf_drv_twi_t  m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_MAX17330);
	uint32_t err_code;

	// initialize INA219 measurement IC
	static const nrf_drv_twi_config_t twi_config = {
		.scl = MAX17330_SCL,
		.sda = MAX17330_SDA,
		.frequency = NRF_DRV_TWI_FREQ_100K,
		.interrupt_priority = APP_IRQ_PRIORITY_LOW,
	};

	static const drv_max17330_init_t param = 
	{
		.p_twi_instance = &m_twi,
		.p_twi_config   = &twi_config,
		.int_pin        = MAX17330_ALERT,
		.addr_model_gauge      = MAX17330_I2C_ADDR_MODEL_GAUGE,
		.addr_nonvolatile_data = MAX17330_I2C_ADDR_NONVOLATILE_DATA,
		.config = {
				.rsense   = 100		,		
				.temp_min = -20    ,		
				.temp_max = 60 		,		
				.volt_min = 3300	,		
				.volt_max = 4000	,		
				.soc_min  = 1		,		
				.soc_max  = 80		,		
				.curr_min = -100	,		
				.curr_max = 70	,		
		},
		.int_handler = on_max17330_alert_handler,
	};
	
	err_code = drv_max17330_init(&param);
	REPORT_IF_ERROR(err_code);
	
	NRF_LOG_INFO("MAX17330 init done");

	return err_code;
}

static void board_init(void)
{
	
    log_init();
	clock_init();
    power_management_init();

#if NRF_LOG_ENABLED && NRF_LOG_DEFERRED
    // Start execution.
    if (pdPASS != xTaskCreate(logger_thread, "LOGGER", 256, NULL, 1, &m_logger_thread))
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }
#endif	
	 	
    NRF_LOG_INFO("------ BEDSORE ------");

	// enable watchdog
#ifndef DEBUG	
	wdt_init();
#endif	

	gpio_init();
	max17330_dev_init();
	
	app_state_init();

	if (m_app_state.gpregret[0] == GPREGRET_ERASE_FLASH1 &&
		m_app_state.gpregret[1] == GPREGRET_ERASE_FLASH2)
	{
		app_config_flash_fds_erase();
	}
	
	
    scheduler_init();                                                                                                                                           
	NRF_LOG_PROCESS();
		
    buttons_leds_init();
	
	m_usb_com_init();
	NRF_LOG_PROCESS();
	
    app_thread_create();
	
	app_timer_create(&m_timer, APP_TIMER_MODE_REPEATED, on_timeout_handler);
	app_timer_start(m_timer, APP_TIMER_TICKS(1000), NULL);
}

static void on_timeout_handler(void* p_context)
{
	uint32_t err_code;
	drv_max17330_evt_t evt;
	
	// read max17330 status
	err_code = drv_max17330_get_events(&evt);
	if (err_code == NRF_SUCCESS)
	{
		on_max17330_alert_handler(evt);
	}
}

// this function is called when the ble stack was initialized
static void ble_stack_was_initialized(void)
{
	uint32_t err_code;
		
	NRF_LOG_FLUSH();
	
	err_code = sd_ble_gap_addr_get(&m_app_state.my_addr);
	APP_ERROR_CHECK(err_code);
}

static uint32_t app_init(void)
{
	uint32_t err_code;
	
	// NUS service
	m_nus_init_t nus_param;
	m_nus_init(&m_ble_service_handles[BLE_SERVICE_NUS], &nus_param);

	// dfu service
	m_dfu_init(&m_ble_service_handles[BLE_SERVICE_DFU]);
	
	////////////////////////////////////////////////////////////
	// BLE init
	static const m_ble_init_t ble_params = {
		.erase_bond        = false,
		.p_service_handles = m_ble_service_handles,
		.service_num       = BLE_SERVICES_MAX,
		.p_stack_init_callback = ble_stack_was_initialized,
		.p_service_data_callback = ble_service_data_callback,
	};
		
	err_code = m_ble_init(&ble_params);
	RETURN_IF_ERROR(err_code);
	NRF_LOG_PROCESS();
		
	return NRF_SUCCESS;
}

// service data callback
static bool	ble_service_data_callback(ble_advdata_service_data_t* p_data)
{
	return true;
}

static void initialize(void)
{
	board_init();	
	NRF_LOG_PROCESS();
	
	app_init(); 
	NRF_LOG_PROCESS();
	
	pushEvent0_force(EVT_app_ready);
}

void app_thread_idle(void)
{
#ifndef DEBUG
	nrf_drv_wdt_channel_feed(m_channel_id);
#endif	
}

// A function which is hooked to idle task.
// Idle hook must be enabled in FreeRTOS configuration (configUSE_IDLE_HOOK).
void vApplicationIdleHook( void )
{
#if NRF_LOG_ENABLED && NRF_LOG_DEFERRED
     vTaskResume(m_logger_thread);
#endif
}

int main(void)
{
 	initialize();
	
    // Start FreeRTOS scheduler.
    vTaskStartScheduler();

    for (;;)
    {
        APP_ERROR_HANDLER(NRF_ERROR_FORBIDDEN);
        //idle_state_handle();
    }

}
