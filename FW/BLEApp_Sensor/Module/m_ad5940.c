#include "ad5940.h"
#include "macros_common.h"
#include "config.h"
#include "m_usb_com.h"

#define  NRF_LOG_MODULE_NAME M_AD5940
#include "nrf_log.h"

NRF_LOG_MODULE_REGISTER();

#define ADCPGA_GAIN_SEL   ADCPGA_1P5

static struct
{
	xTaskHandle 		hThread;
} m_cfg;

uint32_t AD5940_Init(void);
void AD5940_Main(void);

static void adc_thread(void* arg)
{
	AD5940_Main();
}

uint32_t m_ad5940_init(void)
{
	uint32_t err_code;
	
	err_code = AD5940_MCUResourceInit(NULL);
	RETURN_IF_ERROR(err_code);
	
	err_code = AD5940_Init();
	RETURN_IF_ERROR(err_code);
	
	if (pdPASS != xTaskCreate(adc_thread, "ADC", STACK_SIZE_ADC, NULL, 1, &m_cfg.hThread))
		return NRF_ERROR_NO_MEM;
	
	NRF_LOG_INFO("AD5940 init done");
	
	return NRF_SUCCESS;
}

