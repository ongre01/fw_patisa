#include "ad5940.h"
#include "macros_common.h"
#include "config.h"
#include "m_usb_com.h"
#include "BodyImpedance.h"

#define  NRF_LOG_MODULE_NAME M_BIO
#include "nrf_log.h"

NRF_LOG_MODULE_REGISTER();


#define APPBUFF_SIZE 512
uint32_t AppBuff[APPBUFF_SIZE];


int32_t BIAShowResult(uint32_t *pData, uint32_t DataCount)
{
	float freq;
	static char     buf[200];

	fImpPol_Type *pImp = (fImpPol_Type*)pData;
	AppBIACtrl(BIACTRL_GETFREQ, &freq);

	
	sprintf(buf, "Freq:%.2f ", freq);
	NRF_LOG_INFO("%s", nrf_log_push(buf));
	m_usb_com_write_line(buf);
	
	/*Process data*/
	for(int i=0;i<DataCount;i++)
	{
		sprintf(buf, "Impedance : %f Ohm , Phase: %f \n",pImp[i].Magnitude,pImp[i].Phase*180/MATH_PI);
		NRF_LOG_INFO("%s", nrf_log_push(buf));
		m_usb_com_write_line(buf);
	}
	
	return 0;
}


static int32_t AD5940PlatformCfg(void)
{
  CLKCfg_Type clk_cfg;
  FIFOCfg_Type fifo_cfg;
  AGPIOCfg_Type gpio_cfg;

  
  AD5940_HWReset();
  
  AD5940_Initialize();
  
  clk_cfg.ADCClkDiv = ADCCLKDIV_1;
  clk_cfg.ADCCLkSrc = ADCCLKSRC_HFOSC;
  clk_cfg.SysClkDiv = SYSCLKDIV_1;
  clk_cfg.SysClkSrc = SYSCLKSRC_HFOSC;
  clk_cfg.HfOSC32MHzMode = bFALSE;
  clk_cfg.HFOSCEn = bTRUE;
  clk_cfg.HFXTALEn = bFALSE;
  clk_cfg.LFOSCEn = bTRUE;
  AD5940_CLKCfg(&clk_cfg);
  
  fifo_cfg.FIFOEn = bFALSE;
  fifo_cfg.FIFOMode = FIFOMODE_FIFO;
  fifo_cfg.FIFOSize = FIFOSIZE_4KB;
  fifo_cfg.FIFOSrc = FIFOSRC_DFT;
  fifo_cfg.FIFOThresh = 4;
  AD5940_FIFOCfg(&fifo_cfg);
  fifo_cfg.FIFOEn = bTRUE;  
  AD5940_FIFOCfg(&fifo_cfg);
  
  
  
  AD5940_INTCCfg(AFEINTC_1, AFEINTSRC_ALLINT, bTRUE); 
  AD5940_INTCCfg(AFEINTC_0, AFEINTSRC_DATAFIFOTHRESH, bTRUE);
  AD5940_INTCClrFlag(AFEINTSRC_ALLINT);
  
  gpio_cfg.FuncSet = GP6_SYNC|GP5_SYNC|GP4_SYNC|GP2_TRIG|GP1_SYNC|GP0_INT;
  gpio_cfg.InputEnSet = AGPIO_Pin2;
  gpio_cfg.OutputEnSet = AGPIO_Pin0|AGPIO_Pin1|AGPIO_Pin4|AGPIO_Pin5|AGPIO_Pin6;
  gpio_cfg.OutVal = 0;
  gpio_cfg.PullEnSet = 0;

  AD5940_AGPIOCfg(&gpio_cfg);
  AD5940_SleepKeyCtrlS(SLPKEY_UNLOCK);  
  return 0;
}


void AD5940BIAStructInit(void)
{
  AppBIACfg_Type *pBIACfg;
  
  AppBIAGetCfg(&pBIACfg);
  
  pBIACfg->SeqStartAddr = 0;
  pBIACfg->MaxSeqLen = 512; 
  
  pBIACfg->RcalVal = 10000.0;
  pBIACfg->DftNum = DFTNUM_8192;
  pBIACfg->NumOfData = -1;      
  pBIACfg->BiaODR = 20;         
  pBIACfg->FifoThresh = 4;      
  pBIACfg->ADCSinc3Osr = ADCSINC3OSR_2;
}

void AD5940_Main(void)
{
	static uint32_t IntCount;
	static uint32_t count;
	uint32_t temp;

	AD5940PlatformCfg();

	AD5940BIAStructInit(); 

	AppBIAInit(AppBuff, APPBUFF_SIZE);
	AppBIACtrl(BIACTRL_START, 0);     

	while(1)
	{
		
		
		vTaskDelay(1000);
		if(AD5940_GetMCUIntFlag())
		{
			IntCount++;
			AD5940_ClrMCUIntFlag();
			temp = APPBUFF_SIZE;
			AppBIAISR(AppBuff, &temp); 
			BIAShowResult(AppBuff, temp); 

			if(IntCount == 240)
			{
				IntCount = 0;
				//AppBIACtrl(BIACTRL_SHUTDOWN, 0);
			}
		}
		count++;
		if(count > 1000000)
		{
			count = 0;
			//AppBIAInit(0, 0);    
			//AppBIACtrl(BIACTRL_START, 0); 
		}
				
		vTaskDelay(1);
	}
}

uint32_t AD5940_Init(void)
{
	AD5940PlatformCfg();
	
	AD5940BIAStructInit(); 

	AppBIAInit(AppBuff, APPBUFF_SIZE); 
	AppBIACtrl(BIACTRL_START, 0);      
	
	return NRF_SUCCESS;
}

