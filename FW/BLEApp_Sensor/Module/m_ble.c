#include "m_ble.h"
#include "config.h"
#include "app_state.h"
#include "app_config.h"
#include "app_timer.h"

#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"

#include "ble_nus.h"
#include "SEGGER_rtt.h"

#define APP_FEATURE_NOT_SUPPORTED       BLE_GATT_STATUS_ATTERR_APP_BEGIN + 2        /**< Reply when unsupported features are requested. */

#define M_BLE_LOG_ENABLED //test
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
#ifndef M_BLE_LOG_ENABLED
	#define NRF_LOG_LEVEL 0
#endif

#define NRF_LOG_MODULE_NAME m_ble

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
NRF_LOG_MODULE_REGISTER();

NRF_BLE_GATT_DEF(m_gatt);                                                           /**< GATT module instance. */
NRF_BLE_QWR_DEF(m_qwr);                                                             /**< Context for the Queued Write module.*/
BLE_ADVERTISING_DEF(m_advertising);                                                 /**< Advertising module instance. */

static const ble_uuid_t 			m_adv_uuids[] =
{
    {BLE_UUID_DEVICE_INFORMATION_SERVICE, BLE_UUID_TYPE_BLE}
    //{BLE_UUID_BATTERY_SERVICE, BLE_UUID_TYPE_BLE}
    //{BLE_UUID_NUS_SERVICE, NUS_SERVICE_UUID_TYPE}
};

static bool       m_initialized = false;
static uint16_t   m_conn_handle = BLE_CONN_HANDLE_INVALID;                 /**< Handle of the current connection. */

static m_ble_service_handle_t   * m_service_handles = 0;
static uint32_t                   m_service_num = 0;
static m_ble_service_data_cb_t	  m_service_data_handler;
	
////////////////////////////////////////////////////////////////////////////////////////////////////
// BLE stack

static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    uint32_t err_code;
	
	UNUSED_VARIABLE(err_code);

	switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("Connected");
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            err_code = nrf_ble_qwr_conn_handle_assign(&m_qwr, m_conn_handle);
            APP_ERROR_CHECK(err_code);
		
			bsp_indication_set(BSP_INDICATE_CONNECTED);
			APP_FLAG_CLR(af_adv);				// clear adv flag
			APP_FLAG_SET(af_connected);
			pushEvent0_force(EVT_connected);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected");
            // LED indication will be changed when advertising starts.
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
		
			bsp_indication_set(BSP_INDICATE_IDLE);
			APP_FLAG_CLR(af_connected);
			pushEvent0_force(EVT_disconnected);
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported
			err_code = sd_ble_gap_sec_params_reply(m_conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
			APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            // No system attributes have been stored.
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;
        case BLE_GAP_EVT_DATA_LENGTH_UPDATE_REQUEST:
        {
            ble_gap_data_length_params_t dl_params;

            // Clearing the struct will effectively set members to @ref BLE_GAP_DATA_LENGTH_AUTO.
            memset(&dl_params, 0, sizeof(ble_gap_data_length_params_t));
            err_code = sd_ble_gap_data_length_update(p_ble_evt->evt.gap_evt.conn_handle, &dl_params, NULL);
            APP_ERROR_CHECK(err_code);
        } break;
        case BLE_EVT_USER_MEM_REQUEST:
            err_code = sd_ble_user_mem_reply(p_ble_evt->evt.gattc_evt.conn_handle, NULL);
            APP_ERROR_CHECK(err_code);
            break;
        case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST:
        {
            ble_gatts_evt_rw_authorize_request_t  req;
            ble_gatts_rw_authorize_reply_params_t auth_reply;

            req = p_ble_evt->evt.gatts_evt.params.authorize_request;

            if (req.type != BLE_GATTS_AUTHORIZE_TYPE_INVALID)
            {
                if ((req.request.write.op == BLE_GATTS_OP_PREP_WRITE_REQ)     ||
                    (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_NOW) ||
                    (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_CANCEL))
                {
                    if (req.type == BLE_GATTS_AUTHORIZE_TYPE_WRITE)
                    {
                        auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
                    }
                    else
                    {
                        auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
                    }
                    auth_reply.params.write.gatt_status = APP_FEATURE_NOT_SUPPORTED;
                    err_code = sd_ble_gatts_rw_authorize_reply(p_ble_evt->evt.gatts_evt.conn_handle,
                                                               &auth_reply);
                    APP_ERROR_CHECK(err_code);
                }
            }
        } break; // BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST
        case BLE_GAP_EVT_PASSKEY_DISPLAY:
        {
            char passkey[BLE_GAP_PASSKEY_LEN + 1];
            memcpy(passkey, p_ble_evt->evt.gap_evt.params.passkey_display.passkey, BLE_GAP_PASSKEY_LEN);
            passkey[BLE_GAP_PASSKEY_LEN] = 0;
			
            NRF_LOG_INFO("Passkey: %s", nrf_log_push(passkey));
        } break;

        default:
            // No implementation needed.
            break;
    }
}

static uint32_t ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    RETURN_IF_ERROR(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    RETURN_IF_ERROR(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    RETURN_IF_ERROR(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);

	return NRF_SUCCESS;
}


static uint32_t gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;
	char					adv_name[40];


    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

	sprintf(adv_name, "%s [%02X-%02X]", 
		DEF_ADV_NAME, 
		m_app_state.my_addr.addr[1],
		m_app_state.my_addr.addr[0]);
	
	// set device name
    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (uint8_t*)adv_name,
                                          strlen(adv_name));
	
    RETURN_IF_ERROR(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

	// ble_gap.h 에서 min / max 범위를 확인
	// bluetooth spec requirement
	// supervision-timeout(ms) > (1+conn_latency)*conn_interval_max(ms)*2
	// 4000 > (1+0) * 4000 * 2
    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = DEF_SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    RETURN_IF_ERROR(err_code);
	
	return NRF_SUCCESS;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Gatt init

static void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    if ((m_conn_handle == p_evt->conn_handle) && (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED))
    {
        m_app_state.ble_nus_max_data_len = p_evt->params.att_mtu_effective - OPCODE_LENGTH - HANDLE_LENGTH;
        NRF_LOG_INFO("Data len is set to 0x%X(%d)", m_app_state.ble_nus_max_data_len, m_app_state.ble_nus_max_data_len);
    }
    NRF_LOG_DEBUG("ATT MTU exchange completed. central 0x%x peripheral 0x%x",
                  p_gatt->att_mtu_desired_central,
                  p_gatt->att_mtu_desired_periph);
}

static uint32_t gatt_init(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    RETURN_IF_ERROR(err_code);

    err_code = nrf_ble_gatt_att_mtu_periph_set(&m_gatt, NRF_SDH_BLE_GATT_MAX_MTU_SIZE);
    RETURN_IF_ERROR(err_code);
	
	return err_code;
}

// Function for handling Queued Write Module errors.
static void nrf_qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Services

// Function for initializing the ble services.
static uint32_t services_init(m_ble_service_handle_t * p_service_handles, uint32_t num_services)
{
    uint32_t 			err_code;
    nrf_ble_qwr_init_t 	qwr_init = {0};

    // Initialize Queued Write Module.
    qwr_init.error_handler = nrf_qwr_error_handler;

    err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init);
    APP_ERROR_CHECK(err_code);
	
	// initialize services
    for (uint32_t i = 0; i < num_services; i++)
    {
        if (p_service_handles[i].init_cb != NULL)
        {
            err_code = p_service_handles[i].init_cb();
			
            if (err_code != NRF_SUCCESS)
				NRF_LOG_ERROR("BLE SVC %d init err_code is ... %d", i, err_code);
        }
    }

    return NRF_SUCCESS;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Advertising
static void on_adv_evt(ble_adv_evt_t evt)
{
	// notify adv change status
	pushEvent1_force(EVT_adv_changed, evt);
}


static bool get_service_data(ble_advdata_service_data_t* service_data)
{
	if (!m_service_data_handler)
		return false;

	return m_service_data_handler(service_data);
}

static void get_advertising_data(ble_advertising_init_t* p_init)
{
    memset(p_init, 0, sizeof(ble_advertising_init_t));
	

    p_init->advdata.include_appearance = false;
    p_init->advdata.flags              = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    p_init->advdata.name_type          = BLE_ADVDATA_FULL_NAME;
    p_init->advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    p_init->advdata.uuids_complete.p_uuids  = (ble_uuid_t*)m_adv_uuids;
	
	// specify scan response data if scan response data is present
	static ble_advdata_service_data_t service_data;
	if (get_service_data(&service_data))
	{
		p_init->srdata.p_service_data_array = &service_data;
		p_init->srdata.service_data_count   = 1;
	}
	
    p_init->config.ble_adv_fast_enabled  = true;
    p_init->config.ble_adv_fast_interval = APP_ADV_INTERVAL;
	p_init->config.ble_adv_fast_timeout  = BLE_GAP_ADV_TIMEOUT_LIMITED_MAX;
	
    p_init->evt_handler = on_adv_evt;
}


static uint32_t advertising_init(void)
{
    uint32_t                 err_code;
    ble_advertising_init_t   init;

	get_advertising_data(&init);
	
    err_code = ble_advertising_init(&m_advertising, &init);
    RETURN_IF_ERROR(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
	
	return NRF_SUCCESS;
}


uint32_t m_ble_adv_update(bool permanent)
{
    uint32_t               	err_code;
    ble_advertising_init_t	init;
		
	// 1. get advertising data
	get_advertising_data(&init);
	
	// get adv data from advertising structre's adv_data
	ble_gap_adv_data_t* p_adv_data = &m_advertising.adv_data;
	memset(p_adv_data, 0x00, sizeof(ble_gap_adv_data_t));

	p_adv_data->adv_data.p_data      = m_advertising.enc_advdata;
	p_adv_data->scan_rsp_data.p_data = m_advertising.enc_scan_rsp_data;
	
	// 2. calc advertisement buffer size
    if (m_advertising.adv_modes_config.ble_adv_extended_enabled == true)
    {
		p_adv_data->adv_data.len      = sizeof(m_advertising.enc_advdata);
		p_adv_data->scan_rsp_data.len = sizeof(m_advertising.enc_scan_rsp_data);
    }
    else
    {
		p_adv_data->adv_data.len      = BLE_GAP_ADV_SET_DATA_SIZE_MAX;
		p_adv_data->scan_rsp_data.len = BLE_GAP_ADV_SET_DATA_SIZE_MAX;
    }
	
	// 3. encode adv data
    err_code = ble_advdata_encode(&init.advdata, p_adv_data->adv_data.p_data,      &p_adv_data->adv_data.len);
    RETURN_IF_ERROR(err_code);

    err_code = ble_advdata_encode(&init.srdata,  p_adv_data->scan_rsp_data.p_data, &p_adv_data->scan_rsp_data.len);
    RETURN_IF_ERROR(err_code);

	// change advertising data while advertising
	if (APP_FLAG_IS_SET(af_adv))
	{
		// 동일 buffer를 수정할 경우, sd_ble_gap_adv_set_configure를 호출하면
		// NRF_ERROR_INVALID_STATE error가 발생한다. 
		// buffer를 수정하면 바로 변영되므로, sd_ble_gap_adv_set_configure를 사용안함
		//err_code = sd_ble_gap_adv_set_configure(&m_advertising.adv_handle,
		//										 p_adv_data,
		//										 NULL); // This parameter must be set to NULL while advertising
		//RETURN_IF_ERROR(err_code);
	}

	
	return NRF_SUCCESS;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Conn params init

// Function for handling an event from the Connection Parameters Module.
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
	else
	{
		trace("Conn params update");
	}
}

static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

static uint32_t conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    RETURN_IF_ERROR(err_code);
	
	return NRF_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
uint32_t m_ble_adv_start(void)
{
	trace("start adv");
	
    uint32_t err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
    RETURN_IF_ERROR(err_code);

	return NRF_SUCCESS;
}

uint32_t m_ble_adv_stop(void)
{
	trace("stop adv");
	
    uint32_t err_code = sd_ble_gap_adv_stop(m_advertising.adv_handle);
    RETURN_IF_ERROR(err_code);
	
	return NRF_SUCCESS;
}


uint16_t m_ble_get_conn_handle(void)
{
	return m_conn_handle;
}

uint32_t m_ble_disconnect(void)
{
	uint32_t err_code = NRF_ERROR_INVALID_STATE;
	
	if (m_conn_handle != BLE_CONN_HANDLE_INVALID)
	{
		err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
	}
	
	return err_code;
}

bool m_ble_is_connected(void)
{
	return m_conn_handle != BLE_CONN_HANDLE_INVALID;
}


uint32_t m_ble_set_tx_power(int8_t tx_power)
{
	uint32_t err_code;
	
	err_code = sd_ble_gap_tx_power_set(BLE_GAP_TX_POWER_ROLE_ADV, 
		m_advertising.adv_handle, 
		tx_power);
	RETURN_IF_ERROR(err_code);
	
	return NRF_SUCCESS;
}

uint32_t m_ble_init(const m_ble_init_t * p_params)
{
    uint32_t err_code;

	if (m_initialized)
		return NRF_SUCCESS;
	
	// save ble services handle
    m_service_handles 		= p_params->p_service_handles;
    m_service_num     		= p_params->service_num;
	m_service_data_handler 	= p_params->p_service_data_callback;
	
	// initialize ble stack
    err_code = ble_stack_init();
	RETURN_IF_ERROR(err_code);
	
    // Enable FPU again due to SD issue
    #if (__FPU_USED == 1)
        SCB->CPACR |= (3UL << 20) | (3UL << 22);
        __DSB();
        __ISB();
    #endif
	
	// call ble stack init callback after stack was initialized
	if (p_params->p_stack_init_callback)
		p_params->p_stack_init_callback();
	NRF_LOG_FLUSH();
	
    err_code = gap_params_init();
	RETURN_IF_ERROR(err_code);
	
	err_code = gatt_init();
	RETURN_IF_ERROR(err_code);
	
    services_init(m_service_handles, m_service_num);
	NRF_LOG_FLUSH();

    err_code = advertising_init();
	RETURN_IF_ERROR(err_code);
	
    err_code = conn_params_init();
	RETURN_IF_ERROR(err_code);
	
	NRF_LOG_FLUSH();
	
	m_ble_adv_start();
	
    return NRF_SUCCESS;
}

static void on_adv_changed(T_Event const * e)
{
	ble_adv_evt_t evt = (ble_adv_evt_t)e->param1;
	
    switch (evt)
    {
        case BLE_ADV_EVT_FAST:
		case BLE_ADV_EVT_DIRECTED_HIGH_DUTY: 
		case BLE_ADV_EVT_DIRECTED:           
		case BLE_ADV_EVT_SLOW:               
		case BLE_ADV_EVT_FAST_WHITELIST:     
		case BLE_ADV_EVT_SLOW_WHITELIST:     
		case BLE_ADV_EVT_WHITELIST_REQUEST:  
			bsp_indication_set(BSP_INDICATE_ADVERTISING);
			APP_FLAG_SET(af_adv);
			break;
		case BLE_ADV_EVT_PEER_ADDR_REQUEST :
			return;
        case BLE_ADV_EVT_IDLE:
        default:
			bsp_indication_set(BSP_INDICATE_IDLE);
			APP_FLAG_CLR(af_adv);
            break;
    }	
	trace("ADV state : %d", APP_FLAG_IS_SET(af_adv));
	
	// restart advertising when advertising-timeouts in case where adv_duration is infinite
	if (APP_FLAG_IS_CLR(af_adv))
	{
		m_ble_adv_start();	
	}
}

static void on_set_adv(T_Event const * evt)
{
	if (APP_FLAG_IS_SET(af_adv) != evt->param1)
	{
		trace("ADV : %d", evt->param1);

		if (evt->param1) 
		{
			APP_FLAG_CLR(af_adv_stop);				// set re-advertising
			m_ble_adv_start();
		}
		else
		{
			m_ble_adv_stop();
			
			// 2019.12.27 52810에서 adv idle이 오지 않음
			bsp_indication_set(BSP_INDICATE_IDLE);
			
			APP_FLAG_CLR(af_adv);					// clear advertising flag
			APP_FLAG_SET(af_adv_stop);				// stop re-advertising
		}
	}
}

static void on_connected(void)
{	
}

static void on_disconnected(void)
{
	m_ble_adv_start();
}

static void evt_handler(T_Event const * evt, void* p_context)
{
	switch (evt->event)
	{
		case EVT_connected 			: on_connected(); 			break;
		case EVT_disconnected 		: on_disconnected(); 		break;
		case EVT_adv_changed    	: on_adv_changed(evt);		break;
		case EVT_set_adv			: on_set_adv(evt);			break;
	}
}

EVENT_QUEUE_OBSERVER(evt, M_BLE_EVENT_PRIORITIES) =
{
    .evt_handler = evt_handler,
    .p_context = NULL,
};
