#ifndef _m_ble_h
#define _m_ble_h

#include "sdk_common.h"
#include "nrf.h"
#include "ble.h"
#include "ble_advdata.h"

// BLE event types.
typedef enum
{
    ble_evt_connected,
    ble_evt_disconnected,
    ble_evt_timeout
}m_ble_evt_type_t;

// BLE event structure.
typedef struct
{
    m_ble_evt_type_t evt_type;
    void             * p_data;
    uint32_t           size;
}m_ble_evt_t;

// BLE service callback definitions
typedef uint32_t 	(*m_ble_service_init_cb_t)(void);
typedef void 		(*m_ble_init_callback_t)(void);
typedef bool		(*m_ble_service_data_cb_t)(ble_advdata_service_data_t* p_data);

// BLE service handle structure.
typedef struct
{
    m_ble_service_init_cb_t    		init_cb;
} m_ble_service_handle_t;

// Initialization parameters.
typedef struct
{
    m_ble_service_handle_t * 	p_service_handles;
	m_ble_init_callback_t		p_stack_init_callback;
	m_ble_service_data_cb_t		p_service_data_callback;
    uint32_t                 	service_num;
	bool						erase_bond;
} m_ble_init_t;

uint32_t m_ble_init(const m_ble_init_t * p_params);
uint16_t m_ble_get_conn_handle(void);
bool	 m_ble_is_connected(void);
uint32_t m_ble_adv_start(void);
uint32_t m_ble_adv_stop(void);
uint32_t m_ble_disconnect(void);
uint32_t m_ble_adv_update(bool permanent);
uint32_t m_ble_set_tx_power(int8_t tx_power);

#endif
