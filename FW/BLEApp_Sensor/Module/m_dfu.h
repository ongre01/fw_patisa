#ifndef _m_dfu_h
#define _m_dfu_h

#include "m_ble.h"

uint32_t 	m_dfu_init(m_ble_service_handle_t * p_handle);

#endif // _m_dfu_h
