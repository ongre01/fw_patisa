#include "m_event_queue.h"
#include "config.h"

#define NRF_LOG_MODULE_NAME	evt_queue
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

static xTaskHandle      hMainThread;
static QueueHandle_t    hEventQueue;

#define define_event(name) case name : return #name

// Create section 'event_queue_observers'.
NRF_SECTION_SET_DEF(event_queue_observers, event_queue_osbserver_t, EVENT_QUEUE_COUNT);

uint32_t push_event(event_type_t event, uint32_t param1, uint32_t param2, uint32_t param3)
{
    if (hEventQueue == NULL)
        return NRF_ERROR_INVALID_STATE;

    T_Event ed = {
        .event  = event,
        .param1 = param1,
        .param2 = param2,
        .param3 = param3,
    };
    return pdPASS != xQueueSend(hEventQueue, (void*)&ed, sizeof(ed)) ? NRF_ERROR_NO_MEM : NRF_SUCCESS;
}

const char* get_event_name(event_type_t event)
{
    switch(event)
    {
    define_event(EVT_menu_changed	);
    define_event(EVT_change_menu	);
    define_event(EVT_connected		);
    define_event(EVT_disconnected	);
    define_event(EVT_adv_changed	);
    define_event(EVT_set_tx_power	);
    define_event(EVT_pressed		);
    define_event(EVT_released		);
    define_event(EVT_click			);
    define_event(EVT_double_click	);
    define_event(EVT_long_click		);
    define_event(EVT_erase_config	);
    define_event(EVT_config_changed	);
    define_event(EVT_flash_result	);
    define_event(EVT_reset			);
    define_event(EVT_go_sleep		);
    define_event(EVT_set_adv		);
    define_event(EVT_app_ready		);
    define_event(EVT_acm_open		);
    define_event(EVT_acm_closed		);
    define_event(EVT_acm_tx_done	);
    define_event(EVT_acm_rx_done	);
    define_event(EVT_delay_event	);
    define_event(EVT_reset_config	);
    }

    return "Unknown";
}

static void event_thread(void * arg)
{
    static T_Event e;
    static nrf_section_iter_t   iter;

    for (;;)
    {
        if (pdTRUE != xQueueReceive(hEventQueue, &e, portMAX_DELAY))
            continue;

#if 0		
        NRF_LOG_INFO("Event received : %s(%d) (%d, %d, %d)", 
            nrf_log_push((char*)get_event_name(e.event)),
            e.event, e.param1, e.param2, e.param3);
#endif			
        // Notify observers 
        for (nrf_section_iter_init(&iter, &event_queue_observers);
             nrf_section_iter_get(&iter) != NULL;
             nrf_section_iter_next(&iter))
        { 
            event_queue_osbserver_t* p_observer;
            event_queue_handler_t    handler;

            p_observer = (event_queue_osbserver_t *) nrf_section_iter_get(&iter);
            handler    = p_observer->evt_handler;

            handler(&e, p_observer->p_context);
        }
    }
}

uint32_t m_event_queue_create(void)
{
    do
    {
        // Create event queue
        hEventQueue = xQueueCreate(MAX_EVENT_COUNT, sizeof(T_Event));
        if (hEventQueue == NULL)
            break;

        // Create main thread
        if (pdPASS != xTaskCreate(event_thread, "Event queue", 2048, NULL, 1, &hMainThread))
            break;
        return NRF_SUCCESS;
    } while (0);

    return NRF_ERROR_NO_MEM;
}
