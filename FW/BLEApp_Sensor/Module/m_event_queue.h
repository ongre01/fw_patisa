#ifndef _event_queue_h
#define _event_queue_h

#include "sdk_common.h"
#include "nrf_section_iter.h"
#include "event_def.h"

#define EVENT_QUEUE_COUNT 10

typedef struct
{
    event_type_t event;
    uint32_t     param1;
    uint32_t     param2;
    uint32_t     param3;
} T_Event;

typedef void (*event_queue_handler_t)(T_Event const* e, void* p_context);
typedef struct
{
	event_queue_handler_t evt_handler;
	void* 				  p_context;
} event_queue_osbserver_t;

#define EVENT_QUEUE_OBSERVER(_observer, _prio)                                                          \
STATIC_ASSERT(_prio < EVENT_QUEUE_COUNT, "Priority level unavailable.");                 \
/*lint -esym(528,*_observer) -esym(529,*_observer) : Symbol not referenced. */                            \
NRF_SECTION_SET_ITEM_REGISTER(event_queue_observers, _prio, static event_queue_osbserver_t const _observer)


uint32_t m_event_queue_create(void);
uint32_t push_event(event_type_t event, uint32_t param1, uint32_t param2, uint32_t param3);

inline static uint32_t push_event0(event_type_t event)                                      { return push_event(event, 0, 0, 0); }
inline static uint32_t push_event1(event_type_t event, uint32_t param1)                     { return push_event(event, param1, 0, 0); }
inline static uint32_t push_event2(event_type_t event, uint32_t param1, uint32_t param2)    { return push_event(event, param1, param2, 0); }

// compatibility
#define pushEvent0(event, force) 					{ push_event(event, 0, 0, 0); }
#define pushEvent0_force(event) 					{ push_event(event, 0, 0, 0); }
#define pushEvent0_unforce(event) 					{ push_event(event, 0, 0, 0); }

#define pushEvent1(event, param1, force) 			{ push_event(event, param1, 0, 0); }
#define pushEvent1_force(event, param1) 			{ push_event(event, param1, 0, 0); }
#define pushEvent1_unforce(event, param1) 			{ push_event(event, param1, 0, 0); }

#define pushEvent2(event, param1, param2, force) 	{ push_event(event, param1, param2, 0); }
#define pushEvent2_force(event, param1, param2) 	{ push_event(event, param1, param2, 0); }
#define pushEvent2_unforce(event, param1, param2)	{ push_event(event, param1, param2, 0); }

#define pushEvent3(event, param1, param2, param3, force) 	{ push_event(event, param1, param2, param3); }
#define pushEvent3_force(event, param1, param2, param3) 	{ push_event(event, param1, param2, param3); }
#define pushEvent3_unforce(event, param1, param2, param3)	{ push_event(event, param1, param2, param3); }

#endif // _event_queue_h
