#include "ad5940.h"
#include "macros_common.h"
#include "config.h"
#include "m_usb_com.h"
#include "Impedance.h"
#include "app_packet.h"
#include "m_nus.h"

#define  NRF_LOG_MODULE_NAME M_IMPEDANCE
#include "nrf_log.h"

NRF_LOG_MODULE_REGISTER();

#define APPBUFF_SIZE 512
uint32_t AppBuff[APPBUFF_SIZE];

uint32_t temperature1, pressure1, impedance1;
float temperaturep, pressurep, impedancep;

uint16_t imp_voltage1;
uint32_t imp_freq1;

extern BLE_CMD_SETTING_Typedef mMeasure_Setting;
extern volatile bool is_Measure_Setting_Changed;
extern volatile bool is_Measure_Start;
extern volatile bool is_Measure_Stop;
extern volatile bool is_Measure_Pause;
extern volatile bool is_Measure_Restart;
extern volatile AppIMPCfg_Type AppIMPCfg;
int32_t ImpedanceShowResult(uint32_t *pData, uint32_t DataCount)
{
  float freq;
	float volt;
	int idx;
	static char     buf[400];
	
	
  fImpPol_Type *pImp = (fImpPol_Type*)pData;
  AppIMPCtrl(IMPCTRL_GETFREQ, &freq);
	AppIMPCtrl(IMPCTRL_GETVOLT, &volt);

  //sprintf(buf, "Freq:%.2f ", freq);
  //NRF_LOG_INFO("%s", nrf_log_push(buf));
	//m_usb_com_write_line(buf);
	
  for(int i=0;i<DataCount;i++)
  { 
#if 0		
		sprintf(buf, "Volt: %.0f, Freq:%.0f, RzMag: %f Ohm , RzPhase: %f \n",volt,freq,pImp[i].Magnitude,pImp[i].Phase*180/MATH_PI);
		NRF_LOG_INFO("%s", nrf_log_push(buf));
		m_usb_com_write_line(buf);
#else		
		sprintf(buf,"Volt: %.1f (id: %d), Freq:%.1f (id: %d), RzMag: %.2f, RzPhase: %.2f, RpMag: %.2f, RpPhase: %.2f, RtMag: %.2f, RtPhase: %.2f \n",
		           volt,AppIMPCfg.SweepCfg.SweepVoltIndex,freq,AppIMPCfg.SweepCfg.SweepIndex,
		           pImp[3*i+0].Magnitude,pImp[3*i+0].Phase*180/MATH_PI,
							 pImp[3*i+1].Magnitude,pImp[3*i+1].Phase*180/MATH_PI,
		           pImp[3*i+2].Magnitude,pImp[3*i+2].Phase*180/MATH_PI);
		m_usb_com_write_line(buf);
#endif		
  }
	
	idx=DataCount-1;
	impedance1 = (uint32_t)(pImp[3*idx+0].Magnitude);
	impedancep = (float)(pImp[3*idx+0].Phase*180/MATH_PI);
	pressure1 = (uint32_t)(pImp[3*idx+1].Magnitude);
	pressurep = (float)(pImp[3*idx+1].Phase*180/MATH_PI);
	temperature1 = (uint32_t)(pImp[3*idx+2].Magnitude);
	temperaturep = (float)(pImp[3*idx+2].Phase*180/MATH_PI);
	imp_voltage1 = (uint16_t)volt;
	imp_freq1 = (uint32_t)freq;
	
  return 0;
}

static int32_t AD5940PlatformCfg(void)
{
  CLKCfg_Type clk_cfg;
  FIFOCfg_Type fifo_cfg;
  AGPIOCfg_Type gpio_cfg;

  /* Use hardware reset */
  AD5940_HWReset();
  AD5940_Initialize();
  /* Platform configuration */
  /* Step1. Configure clock */
  clk_cfg.ADCClkDiv = ADCCLKDIV_1;
  clk_cfg.ADCCLkSrc = ADCCLKSRC_HFOSC;
  clk_cfg.SysClkDiv = SYSCLKDIV_1;
  clk_cfg.SysClkSrc = SYSCLKSRC_HFOSC;
  clk_cfg.HfOSC32MHzMode = bFALSE;
  clk_cfg.HFOSCEn = bTRUE;
  clk_cfg.HFXTALEn = bFALSE;
  clk_cfg.LFOSCEn = bTRUE;
  AD5940_CLKCfg(&clk_cfg);
  /* Step2. Configure FIFO and Sequencer*/
  fifo_cfg.FIFOEn = bFALSE;
  fifo_cfg.FIFOMode = FIFOMODE_FIFO;
  fifo_cfg.FIFOSize = FIFOSIZE_4KB;                       /* 4kB for FIFO, The reset 2kB for sequencer */
  fifo_cfg.FIFOSrc = FIFOSRC_DFT;
  fifo_cfg.FIFOThresh = AppIMPCfg.FifoThresh;        /* DFT result. One pair for RCAL, another for Rz. One DFT result have real part and imaginary part */
  AD5940_FIFOCfg(&fifo_cfg);
  fifo_cfg.FIFOEn = bTRUE;
  AD5940_FIFOCfg(&fifo_cfg);
  
  /* Step3. Interrupt controller */
  AD5940_INTCCfg(AFEINTC_1, AFEINTSRC_ALLINT, bTRUE);   /* Enable all interrupt in INTC1, so we can check INTC flags */
  AD5940_INTCClrFlag(AFEINTSRC_ALLINT);
  AD5940_INTCCfg(AFEINTC_0, AFEINTSRC_DATAFIFOTHRESH, bTRUE); 
  AD5940_INTCClrFlag(AFEINTSRC_ALLINT);
  /* Step4: Reconfigure GPIO */
  gpio_cfg.FuncSet = GP0_INT|GP1_SLEEP|GP2_SYNC;
  gpio_cfg.InputEnSet = 0;
  gpio_cfg.OutputEnSet = AGPIO_Pin0|AGPIO_Pin1|AGPIO_Pin2;
  gpio_cfg.OutVal = 0;
  gpio_cfg.PullEnSet = 0;
  AD5940_AGPIOCfg(&gpio_cfg);
  AD5940_SleepKeyCtrlS(SLPKEY_UNLOCK);  /* Allow AFE to enter sleep mode. */
  return 0;
}

void AD5940ImpedanceStructInit(void)
{
  AppIMPCfg_Type *pImpedanceCfg;
  
  AppIMPGetCfg(&pImpedanceCfg);
  /* Step1: configure initialization sequence Info */
  pImpedanceCfg->SeqStartAddr = 0;
  pImpedanceCfg->MaxSeqLen = 512; /* @todo add checker in function */

  pImpedanceCfg->RcalVal = 20000.0;
  pImpedanceCfg->SinFreq = 50000.0;
  pImpedanceCfg->FifoThresh = AppIMPCfg.FifoThresh;
	
	/* Set switch matrix to onboard(EVAL-AD5940ELECZ) dummy sensor. */
	/* Note the RCAL0 resistor is 10kOhm. */
	pImpedanceCfg->DswitchSel = SWD_OPEN;
	pImpedanceCfg->PswitchSel = SWP_OPEN;
	pImpedanceCfg->NswitchSel = SWN_OPEN;
	pImpedanceCfg->TswitchSel = SWT_OPEN;
	/* The dummy sensor is as low as 5kOhm. We need to make sure RTIA is small enough that HSTIA won't be saturated. */
	pImpedanceCfg->HstiaRtiaSel = HSTIARTIA_1K;	
	
	/* Configure the sweep function. */
	pImpedanceCfg->SweepCfg.SweepEn = bTRUE;
	pImpedanceCfg->SweepCfg.SweepStart = 1000.0f;	/* Start from 1kHz */
	pImpedanceCfg->SweepCfg.SweepStop = 200e3f;		/* Stop at 100kHz */
	pImpedanceCfg->SweepCfg.SweepPoints = 21;		/* Points is 101 */
	pImpedanceCfg->SweepCfg.SweepLog = bTRUE;

	/* Configure the voltage sweep function. */
	pImpedanceCfg->SweepCfg.SweepVoltEn = bFALSE;
	pImpedanceCfg->SweepCfg.SweepVoltStart = 100.0f;	/* Start from 10 mV */
	pImpedanceCfg->SweepCfg.SweepVoltStop = 800.0f;		/* Stop at Vpp 800 mV */
	pImpedanceCfg->SweepCfg.SweepVoltPoints = 5;		/* Points is 101 */
	pImpedanceCfg->SweepCfg.SweepVoltLog = bTRUE;
	
	
	/* Configure Power Mode. Use HP mode if frequency is higher than 80kHz. */
	pImpedanceCfg->PwrMod = AFEPWR_HP;
	/* Configure filters if necessary */
	pImpedanceCfg->ADCSinc3Osr = ADCSINC3OSR_2;		/* Sample rate is 800kSPS/2 = 400kSPS */
  pImpedanceCfg->DftNum = DFTNUM_16384;
  pImpedanceCfg->DftSrc = DFTSRC_SINC3;
}

void AD5940_Main(void)
{
  uint32_t temp;  
	
#if 0	
  AD5940PlatformCfg();
  AD5940ImpedanceStructInit();
  
  AppIMPInit(AppBuff, APPBUFF_SIZE);    /* Initialize IMP application. Provide a buffer, which is used to store sequencer commands */
  AppIMPCtrl(IMPCTRL_START, 0);          /* Control IMP measurement to start. Second parameter has no meaning with this command. */
 #endif
  while(1)
  {
		vTaskDelay(1000);
		
		if(AD5940_GetMCUIntFlag())
    {
      AD5940_ClrMCUIntFlag();
      temp = APPBUFF_SIZE;
      AppIMPISR(AppBuff, &temp);
      ImpedanceShowResult(AppBuff, temp);
    }
		
		if(is_Measure_Pause==1)
		{
				if(is_Measure_Restart == 0)
				{
					is_Measure_Pause = 0;
					is_Measure_Restart = 1;
					NRF_LOG_INFO("Pause\r\n");					
					AppIMPCtrl(IMPCTRL_STOPSYNC, 0);
				}
				else
				{
					is_Measure_Pause = 0;
					is_Measure_Restart = 0;
					NRF_LOG_INFO("Restart\r\n");
					AppIMPCtrl(IMPCTRL_RESTART, 0);					
				}
		}
		
		if(is_Measure_Start==1)
		{
			is_Measure_Start=0;
			//AppIMPInit(AppBuff, APPBUFF_SIZE);
			//AppIMPCtrl(IMPCTRL_START, 0);
		}
		
		if(is_Measure_Stop==1)
		{
			is_Measure_Stop = 0;
			AppIMPCtrl(IMPCTRL_STOPNOW, 0);
		}
	
		if(is_Measure_Setting_Changed==1)
		{
			is_Measure_Setting_Changed = 0;
			is_Measure_Pause = 0;
			is_Measure_Restart = 0;
			AppIMPCtrl(IMPCTRL_STOPNOW, 0);
			AD5940PlatformCfg();
			AD5940ImpedanceStructInit(); 
			AppIMPCtrl(IMPCTRL_SETCONDITION, (void*)&mMeasure_Setting);
			AppIMPInit(AppBuff, APPBUFF_SIZE);
			AppIMPCtrl(IMPCTRL_START, 0);
		}
		
		
		vTaskDelay(1);
  }
}


uint32_t AD5940_Init(void)
{
	AD5940PlatformCfg();
	//AD5940ImpedanceStructInit(); 

	//AppIMPInit(AppBuff, APPBUFF_SIZE);
	//AppIMPCtrl(IMPCTRL_START, 0);      
	
	return NRF_SUCCESS;
}

