#include "config.h"
#include "ble_nus.h"

#include "m_ble.h"
#include "m_nus.h"
#include "app_state.h"
#include "macros_common.h"
#include "helper.h"
#include "app_scheduler.h"
#include "app_config.h"
#include "app_timer.h"
#include "nrf_delay.h"
#include "helper.h"
#include "app_packet.h"
#include "crc16.h"
#include "m_usb_com.h"

#define M_NUS_LOG_ENABLED

#ifndef M_NUS_LOG_ENABLED
	#define APP_CONFIG_LOG_LEVEL 0
#endif

#define NRF_LOG_MODULE_NAME M_NUS
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

// Service instance
BLE_NUS_DEF(m_nus, NRF_SDH_BLE_TOTAL_LINK_COUNT);
APP_TIMER_DEF(m_timer);

typedef enum
{
	ps_stx,
	ps_header,
	ps_data,
	ps_etx,
} packet_state_t;

ANON_UNIONS_ENABLE;
typedef struct
{
	bool 		ready;
	union
	{
		uint8_t 		buf[MAX_PACKET];
		app_packet_t 	packet;
	};
	uint16_t 		pos;
	packet_state_t 	state;
} m_config_t;

m_config_t m_cfg;

ANON_UNIONS_DISABLE;

static bool check_crc(void);
static void on_packet_complete(void);
static void send_ack(uint8_t ack);
static void on_start(void);
static void on_stop(void);
static void on_pause(void);
static void write_packet(void* p_data, uint16_t len);

static void clear_packet(void)
{
	m_cfg.pos = 0;
	m_cfg.state = ps_stx;
}

static void parse_packet(uint8_t* p_data, uint16_t len)
{
	uint8_t ch;
	for (int i=0; i<len; i++)
	{
		ch = *p_data++;
		switch (m_cfg.state)
		{
		case ps_stx : 
			if (ch == STX)
			{
				m_cfg.state = ps_header;
				m_cfg.pos   = 0;
			}
			break;
		case ps_header :
			m_cfg.buf[m_cfg.pos++] = ch;
			if (m_cfg.pos == sizeof(app_packet_header_t))
			{
				
				m_cfg.state = ps_data;
			}
			break;
		case ps_data :
			m_cfg.buf[m_cfg.pos++] = ch;
			if (m_cfg.pos == sizeof(app_packet_header_t) + m_cfg.packet.header.len + CRC_LEN - 1)
			{
				m_cfg.state = ps_etx;
			}
			break;
		case ps_etx :
			if (ch == ETX)
			{
				if (check_crc())
				{
					on_packet_complete();
				}
				clear_packet();
			}
			break;
		}
	}
}

BLE_CMD_SETTING_Typedef mMeasure_Setting={0,};
volatile bool is_Measure_Setting_Changed = 0;
volatile bool is_Measure_Start = 0;
volatile bool is_Measure_Stop = 0;
volatile bool is_Measure_Pause = 0;
volatile bool is_Measure_Restart = 0;
static void app_cmd_parser(uint8_t* p_data, uint16_t len)
{
	
	BLE_REC_Data_Typedef *pRecData;
	pRecData = (BLE_REC_Data_Typedef *) p_data;
	uint8_t data_buf[100];
	uint8_t buf[200]={0,};
	
	if(pRecData->Start == BLE_START_BYTE && pRecData->Address == TO_BLEDEVICE)
	{
		
		switch(pRecData->CMD)
		{
			case  BLE_CMD_SETTING:
				memcpy((void*)&mMeasure_Setting, (void*)&pRecData->Data[0],sizeof(BLE_CMD_SETTING_Typedef));
				is_Measure_Setting_Changed = 1;
			  //sprintf(buf,"Setting\r\n");
			  //m_usb_com_write_line(buf);
				send_ack(ACK);
				break;
			case	BLE_CMD_START:
			  if(is_Measure_Restart==1)
				{
					is_Measure_Pause = 1;
				}
				else
				{
					is_Measure_Start = 1;
				}
				//sprintf(buf,"Measure_Start,%d\r\n",is_Measure_Start);
			  //m_usb_com_write_line(buf);
				send_ack(ACK);
			  on_start();
				break;
			case	BLE_CMD_STOP:
				is_Measure_Stop = 1;
				//sprintf(buf,"Measure_Stop,%d\r\n",is_Measure_Start);
				//m_usb_com_write_line(buf);
				send_ack(ACK);	
				on_stop();
				break;
			case	BLE_CMD_PAUSE:
				is_Measure_Pause = 1;
				//sprintf(buf,"Measure_Pause,%d\r\n",is_Measure_Pause);
			  //m_usb_com_write_line(buf);
				send_ack(ACK);
				on_pause();
				break;
			default:
				break;
		}
	}
}

static bool check_crc(void)
{
	uint16_t calc_crc = crc16_compute(m_cfg.buf, m_cfg.pos - CRC_LEN, NULL);
	uint16_t crc      = (m_cfg.buf[m_cfg.pos-1] << 8) | m_cfg.buf[m_cfg.pos-2];
	if (calc_crc != crc)
	{
		m_usb_com_write_line_fmt("CRC err : 0x%04x(c) != 0x%04x(r)", 
			calc_crc, crc);
		return false;
	}
	
	return true;
}

static void on_packet_complete(void)
{
	NRF_LOG_INFO("cmd : 0x%02x", m_cfg.packet.header.cmd);

	// send pack
	send_ack(ACK);
	
	// consume packet
	switch (m_cfg.packet.header.cmd)
	{
	case CMD_SETTING : break;
	case CMD_START	 : on_start(); 	break;
	case CMD_STOP	 : on_stop(); 	break;
	case CMD_PAUSE	 : on_pause(); 	break;
	}
}

static void write_packet(void* p_data, uint16_t len)
{
	uint8_t buf[MAX_PACKET];
	uint16_t pos = 0, crc;

	buf[pos++] = STX;
	
	memcpy(&buf[pos], p_data, len);
	pos += len;
	
	crc = crc16_compute(p_data, len, NULL);
	memcpy(&buf[pos], &crc, CRC_LEN);
	pos += CRC_LEN;
	
	buf[pos++] = ETX;
	
	m_usb_com_write_hexdump(buf, pos, "SND :");
	
	m_nus_write(buf, pos);
}

static void send_ack(uint8_t ack)
{
	app_packet_header_t header;

	header.addr = ADDR_APP;
	header.cmd  = ack;
	header.len  = 0;
	
	write_packet(&header, sizeof(header));
}

static void start_timer(void)
{
	app_timer_start(m_timer, APP_TIMER_TICKS(1000), NULL);
}

static void stop_timer(void)
{
	app_timer_stop(m_timer);
}

static void on_start(void)
{
	start_timer();
}

static void on_stop(void)
{
	stop_timer();
}

static void on_pause(void)
{
	stop_timer();
}

extern uint32_t temperature1, pressure1, impedance1;
extern float temperaturep, pressurep, impedancep;

extern uint16_t imp_voltage1;
extern uint32_t  imp_freq1;

static void on_timer_handler(void* p_context)
{
	// 임시데이터
	uint8_t buf[200]={0,};
	
	static app_packet_data_t t; 
	
	t.temperature = temperature1;
	t.tphase = temperaturep;
	t.pressure = pressure1;
	t.pphase = pressurep;
	t.impedance = impedance1;
	t.iphase = impedancep;
	t.imp_voltage = imp_voltage1;
	t.imp_freq[0] = (uint8_t) (imp_freq1 & 0xFF);
	t.imp_freq[1] = (uint8_t) (imp_freq1>>8 & 0xFF);
	t.imp_freq[2] = (uint8_t) (imp_freq1>>16 & 0xFF);
	
	
	sprintf(buf, "tA:%d, tP:%f, volt:%d, freq:%d\r\n",t.temperature,t.tphase,t.imp_voltage,*(uint32_t*)t.imp_freq & 0xFFFFFF);
	m_usb_com_write_line(buf);
	
	app_packet_t packet;
	memset(&packet, 0x00, sizeof(packet));
	
	packet.header.addr = ADDR_APP;
	packet.header.cmd  = CMD_DATA;
	packet.header.len  = sizeof(app_packet_data_t) + 1;
	
	memcpy(&packet.data, &t, sizeof(app_packet_data_t));
	
	write_packet(&packet, sizeof(app_packet_header_t) + sizeof(app_packet_data_t));
}

static void nus_data_handler(ble_nus_evt_t * p_evt)
{
    switch ((uint32_t)p_evt->type)
	{
		case BLE_NUS_EVT_COMM_STARTED :
		case BLE_NUS_EVT_COMM_STOPPED : 
			pushEvent1_force(EVT_nus_comm_stat, p_evt->type);
			break;
		case BLE_NUS_EVT_RX_DATA :
		{
			//NRF_LOG_INFO("Received data from BLE NUS. Writing data on UART.");
			//NRF_LOG_HEXDUMP_INFO(p_evt->params.rx_data.p_data, p_evt->params.rx_data.length);

			uint16_t len    = p_evt->params.rx_data.length;
			uint8_t* p_data = (uint8_t*)p_evt->params.rx_data.p_data;
	
			m_usb_com_write_hexdump(p_data, len, "RCV :");
			
			//parse_packet(p_data, len);
			app_cmd_parser(p_data, len);
		}
			break;
	}
}

uint32_t m_nus_write(const uint8_t* p_data, uint16_t len)
{
	uint32_t err_code;
	
	do
	{
		if (!m_cfg.ready)
			return NRF_ERROR_INVALID_STATE;
		
		err_code = ble_nus_data_send(&m_nus, (uint8_t*)p_data, 
								     &len, m_ble_get_conn_handle());
		if ( (err_code != NRF_ERROR_INVALID_STATE) && (err_code != NRF_ERROR_BUSY) &&
			 (err_code != NRF_ERROR_NOT_FOUND) )
		{
			//NRF_LOG_INFO("NUS write error : %d\n", err_code);
			return err_code;
		}
		//NRF_LOG_INFO("NUS write error : %d\n", err_code);
	} while (err_code == NRF_ERROR_BUSY);	
	
	return NRF_SUCCESS;
}

static uint32_t nus_service_init(void)
{
	uint32_t err_code;
	
    const ble_nus_init_t     nus_init = {
		.data_handler = nus_data_handler
	};
	
    // Initialize NUS.
    err_code = ble_nus_init(&m_nus, &nus_init);
    APP_ERROR_CHECK(err_code);
	
	return NRF_SUCCESS;
}

uint32_t m_nus_init(m_ble_service_handle_t * p_handle, m_nus_init_t * p_params)
{
    NULL_PARAM_CHECK(p_handle);
	
	// initialize service params
    p_handle->init_cb = nus_service_init;
	
	app_timer_create(&m_timer, APP_TIMER_MODE_REPEATED, on_timer_handler);
	
	//start_timer();
	
	return NRF_SUCCESS;
}

////////////////////////////////////////////////////////////////////////////////
// Event handler
static void on_nus_comm_state(T_Event const * evt)
{
	m_cfg.ready = (evt->param1 == BLE_NUS_EVT_COMM_STARTED);
	NRF_LOG_INFO("nus ready = %d", m_cfg.ready);
}

static void on_connected(void)
{
	m_cfg.ready = false;
	
	// 임시로 시작
	//start_timer();
}

static void on_disconnected(void)
{
	m_cfg.ready = false;
	
	stop_timer();
}

static void evt_handler(T_Event const * evt, void* p_context)
{
	switch (evt->event)
	{
		case EVT_nus_comm_stat		: on_nus_comm_state(evt);	break;
		case EVT_connected 			: on_connected(); 			break;
		case EVT_disconnected 		: on_disconnected(); 		break;
	}
}

EVENT_QUEUE_OBSERVER(evt, M_NUS_EVENT_PRIORITIES) =
{
    .evt_handler = evt_handler,
    .p_context = NULL,
};
