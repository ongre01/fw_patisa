#ifndef _m_nus_h
#define _m_nus_h

#include "m_ble.h"

typedef struct
{
	void* param;
} m_nus_init_t;

typedef	__packed struct{
	uint8_t Start;
	uint8_t Address;
	uint8_t Length;
	uint8_t CMD;
	uint8_t Data[100];
}BLE_REC_Data_Typedef;

typedef	__packed struct{
	uint16_t Volt_Start_mV;
	uint16_t Volt_Step;
	uint16_t Volt_Stop;
	uint16_t Volt_Period;
	uint16_t Volt_Duration;
	uint16_t Freq_Start_Hz;
	uint16_t Freq_Step;
	uint32_t Freq_Stop : 24;
	uint16_t Freq_Period;
	uint16_t Freq_Duration;
}BLE_CMD_SETTING_Typedef;

typedef enum
{
	BLE_CMD_SETTING=0x01,
	BLE_CMD_START,
	BLE_CMD_STOP,
	BLE_CMD_PAUSE,
	BLE_CMD_DATA=0x11
} ble_cmd_t;

typedef enum
{
	TO_BLEDEVICE=0x01,
	TO_APP,
} ble_address_t;

typedef enum
{
	BLE_START_BYTE=0x02,
	BLE_STOP_BYTE,
} ble_start_and_stop_t;

uint32_t 	m_nus_init(m_ble_service_handle_t * p_handle, m_nus_init_t * p_params);
uint32_t 	m_nus_write(const uint8_t* p_buf, uint16_t len);

void 		m_nus_send_tcp(void* buf, uint16_t len);
void 		m_nus_send_udp(void* buf, uint16_t len);
#endif // _m_nus_h
