#include <stdarg.h>
#include "config.h"
#include "app_timer.h"
#include "app_state.h"
#include "m_usb_cdc.h"
#include "macros_common.h"
#include "app_scheduler.h"

#include "m_usb_com.h"
#include "app_fifo.h"
#include "helper.h"
#include "utility.h"

#define NRF_LOG_MODULE_NAME	M_USB_COM
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

#define USB_TX_BUFFER_SIZE		2048
#define USB_BLOCK_SIZE			64

#define CMD_BUFFER_SIZE			256
#define MAX_HEX_DUMP_SIZE 		16

// USB TX buffer
static struct
{
	app_fifo_t tx_fifo;
	bool       initialized;
	bool       tx_in_progress;
} m_cfg;


APP_TIMER_DEF(m_timer);
static SemaphoreHandle_t	m_mutex;

// USB task handle
static TaskHandle_t m_hUSBTaskHandle;

static void tx_buffer_flush(void)
{
	uint8_t buf[USB_BLOCK_SIZE];
	uint32_t size;
	uint32_t err_code;
	
	do
	{
		size = sizeof(buf);
		if (app_fifo_read(&m_cfg.tx_fifo, buf, &size) == NRF_ERROR_NOT_FOUND || size == 0)
			return;
		
		if (!m_usb_cdc_is_ready())
		{
			app_fifo_flush(&m_cfg.tx_fifo);
			return;
		}		
		
		//NRF_LOG_INFO("W : %d", tx_buf_pos);
		err_code = m_usb_cdc_write(buf, size);
		//NRF_LOG_HEXDUMP_INFO(buf, size);
		if (err_code != NRF_SUCCESS)
		{
			//NRF_LOG_INFO("CDC write error : %d", err_code);
			break;
		}		
		m_cfg.tx_in_progress = true;
	} while (0);
}

// this function is called when the usb received rx data
static void usb_data_received_handler(const void* p_data, uint32_t len)
{
}

static void usb_acm_status_handler(app_usbd_cdc_acm_user_event_t event)
{
    switch (event)
    {
        case APP_USBD_CDC_ACM_USER_EVT_PORT_OPEN:
			//NRF_LOG_INFO("*** Open");
			pushEvent0_force(EVT_acm_open);
            break;
        case APP_USBD_CDC_ACM_USER_EVT_PORT_CLOSE:
			//NRF_LOG_INFO("*** Close");
			pushEvent0_force(EVT_acm_closed);
            break;
		case APP_USBD_CDC_ACM_USER_EVT_TX_DONE :
			//NRF_LOG_INFO("*** Tx done");
			pushEvent0_force(EVT_acm_tx_done);
			break;
		case APP_USBD_CDC_ACM_USER_EVT_RX_DONE:
			//NRF_LOG_INFO("*** Rx done");
			//pushEvent0_force(EVT_acm_rx_done);
			break;
	}	
}

uint32_t m_usb_com_write_ch(uint8_t ch)
{
	return m_usb_com_write(&ch, 1);
}


uint32_t m_usb_com_write(const void* p_data, uint16_t len)
{
	uint32_t size, err_code;
	
	if (!m_cfg.initialized || !APP_FLAG_IS_SET(af_acm_open))
		return NRF_ERROR_INVALID_STATE;
	
	if (!m_usb_cdc_is_ready())
		return NRF_ERROR_INVALID_STATE;
	
	xSemaphoreTake(m_mutex, portMAX_DELAY );
	
	while (len != 0)
	{
		size = len;
		
		err_code = app_fifo_write(&m_cfg.tx_fifo, (uint8_t*)p_data, &size);
		if (err_code == NRF_ERROR_NO_MEM || size != len)
		{
			app_fifo_flush(&m_cfg.tx_fifo);
		}
		else
		{
			len    -= size;
			p_data  = (uint8_t*)p_data + size;
		}
	}

	xSemaphoreGive(m_mutex);

	return NRF_SUCCESS;
} 

uint32_t m_usb_com_write_line_fmt(const char* fmt, ...)
{
	static char buf[512];
	
	if (!m_cfg.initialized || !APP_FLAG_IS_SET(af_acm_open))
		return NRF_ERROR_INVALID_STATE;
	
	va_list list;
	va_start(list, fmt);
	vsnprintf(buf, sizeof(buf)-1, fmt, list);
	va_end(list);
	
	m_usb_com_write_line(buf);
	
	return NRF_SUCCESS;
}


uint32_t m_usb_com_write_line(const char* str)
{
	//NRF_LOG_INFO("%s", nrf_log_push((char*)str));

	m_usb_com_write(str, strlen(str));
	m_usb_com_write("\r\n", 2);
		
	return NRF_SUCCESS;
}

uint32_t m_usb_com_write_hexdump(uint8_t* p_dat, uint16_t len, const char* prefix)
{
	char buf[128];
	
	sprintf(buf, "%s", prefix);
	to_hex_string(buf + strlen(buf), sizeof(buf) - strlen(buf) - 1, p_dat, len, '-');
	
	return m_usb_com_write_line(buf);
}

static void usb_thread(void* arg)
{
	while (true)
	{
		m_usb_cdc_run();
		
		if (app_fifo_get_length(&m_cfg.tx_fifo) != 0 &&	
			m_cfg.tx_in_progress == false)
		{
			tx_buffer_flush();
		}

		vTaskDelay(1);
	}
}

static void on_timer_handler(void* p_context)
{
	push_event0(EVT_usb_ready);
}

uint32_t m_usb_com_init(void)
{
	static uint8_t    tx_buf[USB_TX_BUFFER_SIZE];
	uint32_t err_code;
		
	app_fifo_init(&m_cfg.tx_fifo, tx_buf, sizeof(tx_buf));
	
	const static m_usb_cdc_init_t usb_cfg = {
		.acm_rcv_callback = usb_data_received_handler,
		.acm_status_callback = usb_acm_status_handler,
	};
	err_code = m_usb_cdc_init(&usb_cfg);
	RETURN_IF_ERROR(err_code);

	// Create USB thread
	if (pdPASS != xTaskCreate(usb_thread, "USB", 1024, NULL, 1, &m_hUSBTaskHandle))
		return NRF_ERROR_NO_MEM;
	
	m_mutex = xSemaphoreCreateMutex();

	app_timer_create(&m_timer, APP_TIMER_MODE_SINGLE_SHOT, on_timer_handler);
	
	m_cfg.initialized = true;
	 
	return NRF_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
static void clear_tx_buffer(void)
{
	// clear tx buffer
	app_fifo_flush(&m_cfg.tx_fifo);
	m_cfg.tx_in_progress = false;
}

static void on_acm_open(void)
{
	APP_FLAG_SET(af_acm_open);
	
	app_timer_start(m_timer, APP_TIMER_TICKS(1000), NULL);
	clear_tx_buffer();
}

static void on_acm_closed(void)
{
	APP_FLAG_CLR(af_acm_open);
	clear_tx_buffer();
}

static void on_acm_tx_done(void)
{
	m_cfg.tx_in_progress = false;
}

static void on_acm_rx_done(void)
{
}

static void on_app_ready(void)
{
#if 1	
	uint32_t err_code;
	
	err_code = m_usb_cdc_power_events_enable();
	REPORT_IF_ERROR(err_code);
#endif	
}

////////////////////////////////////////////////////////////////////////////////////
// Event handler
static void evt_handler(T_Event const * evt, void* p_context)
{
	switch (evt->event)
	{
		case EVT_app_ready			: on_app_ready();	break;
		case EVT_acm_open			: on_acm_open(); 	break;
		case EVT_acm_closed			: on_acm_closed(); 	break;
		case EVT_acm_tx_done		: on_acm_tx_done(); 	break;
		case EVT_acm_rx_done		: on_acm_rx_done(); 	break;
	}
}


EVENT_QUEUE_OBSERVER(evt, M_USB_EVENT_PRIORITIES) =
{
    .evt_handler = evt_handler,
    .p_context = NULL,
};
