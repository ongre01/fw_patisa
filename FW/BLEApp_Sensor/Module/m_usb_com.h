#ifndef _m_usb_com_h
#define _m_usb_com_h

#include "sdk_common.h"
#include "ble_gap.h"

uint32_t m_usb_com_init(void);
void     m_usb_com_run(void);

uint32_t m_usb_com_write(const void* p_data, uint16_t len);
uint32_t m_usb_com_write_ch(uint8_t ch);

uint32_t m_usb_com_write_line(const char* str);
uint32_t m_usb_com_write_line_fmt(const char* fmt, ...);
uint32_t m_usb_com_write_hexdump(uint8_t* p_dat, uint16_t len, const char* prefix);

#endif // _m_usb_com_h
