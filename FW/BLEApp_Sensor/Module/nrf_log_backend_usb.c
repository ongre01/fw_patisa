#include "sdk_common.h"

#if NRF_MODULE_ENABLED(NRF_LOG) && NRF_MODULE_ENABLED(NRF_LOG_BACKEND_USB)
#include "nrf_log_backend_uart.h"
#include "nrf_log_backend_serial.h"
#include "nrf_log_internal.h"
#include "app_error.h"
#include "nrf_delay.h"
#include "m_usb_com.h"
#include "m_uart.h"

//#include "m_nus_packet.h"
//#include "app_packet_ble.h"

#include "m_nus.h"

#define LOG_NUS		0
#define LOG_USB		1
#define LOG_UART	0

static uint8_t m_string_buff[NRF_LOG_BACKEND_USB_TEMP_BUFFER_SIZE];

void nrf_log_backend_uart_init(void)
{
}

static void serial_tx(void const * p_context, char const * p_buffer, size_t len)
{
#if LOG_UART
	m_uart_write((uint8_t*)p_buffer, len);
#endif
	
#if LOG_NUS
	m_nus_write((uint8_t*)p_buffer, len);
#endif
	
#if LOG_USB	
	m_usb_com_write((uint8_t*)p_buffer, len);
#endif	
}

static void nrf_log_backend_usb_put(nrf_log_backend_t const * p_backend,
                                     nrf_log_entry_t * p_msg)
{
    nrf_log_backend_serial_put(p_backend, p_msg, m_string_buff,
                               NRF_LOG_BACKEND_USB_TEMP_BUFFER_SIZE, serial_tx);
}

static void nrf_log_backend_usb_flush(nrf_log_backend_t const * p_backend)
{

}

static void nrf_log_backend_usb_panic_set(nrf_log_backend_t const * p_backend)
{
}

const nrf_log_backend_api_t nrf_log_backend_uart_api = {
        .put       = nrf_log_backend_usb_put,
        .flush     = nrf_log_backend_usb_flush,
        .panic_set = nrf_log_backend_usb_panic_set,
};
#endif //NRF_MODULE_ENABLED(NRF_LOG) && NRF_MODULE_ENABLED(NRF_LOG_BACKEND_USB)
