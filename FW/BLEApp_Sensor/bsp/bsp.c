#include "bsp.h"
#include <stddef.h>
#include <stdio.h>
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_gpio.h"
#include "nrf_error.h"
#include "bsp_config.h"
#include "boards.h"
#include "config.h"
#include "macros_common.h"
#include "app_config.h"
#include "m_ble.h"
#include "m_pwm_buzzer.h"
#include "note.h"

#define BSP_LOG_ENABLED //test
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
#ifndef BSP_LOG_ENABLED
    #define NRF_LOG_LEVEL 0
#endif

#define NRF_LOG_MODULE_NAME BSP
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

#include "app_timer.h"
#include "app_button.h"
#include "app_scheduler.h"
#include "app_state.h"

#include "rtc_timer.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"

#include <float.h>

static const m_pwm_buzzer_note_t m_system_sound[][5] = {
	{
		{.note=NOTE_C6, .duration=100}, 
		{.note=0, 		.duration=10},
	},
	{
		{.note=NOTE_G6, .duration=100}, 
		{.note=NOTE_E6, .duration=100}, 
		{.note=NOTE_C6, .duration=100}, 
		{.note=0, 		.duration=300},
	},
};

#define SOUND_BEEP 		0
#define SOUND_ALERT 	1

static void play_buzzer(uint32_t index, uint16_t repeat)
{
	m_pwm_buzzer_play_start(m_system_sound[index], repeat);
}

#if BUTTONS_NUMBER > 0

#define LONG_CLICK_CHK_PERIOD_MS (100) 
#define LONG_CLICK_TICKS		APP_TIMER_TICKS(LONG_CLICK_CHK_PERIOD_MS)
#define MIN_BUTTON_DURATION 	50

#define DBL_CLK_TOLERANCE		(300)
#define DBL_CLK_CHK_PERIOD_MS 	(10) 
#define DBL_CLK_TICKS			APP_TIMER_TICKS(DBL_CLK_CHK_PERIOD_MS)

typedef enum
{
	bs_init,
	bs_pressed,
	bs_double_click,
} button_state_t;

typedef struct
{
	timer_t 		tick;
	uint8_t 		state;
	uint8_t			button_state;
} button_t;

///////////////////////////////////////////////////////////////////////////////
typedef struct
{
    uint8_t              pin_no;           /**< Pin to be used as a button. */
    uint8_t              active_state;     /**< APP_BUTTON_ACTIVE_HIGH or APP_BUTTON_ACTIVE_LOW. */
    nrf_gpio_pin_pull_t  pull_cfg;         /**< Pull-up or -down configuration. */
    app_button_handler_t button_handler;   /**< Handler to be called when button is pushed. */
} bsp_button_cfg_t;
#endif

static bsp_indication_t m_stable_state        = BSP_INDICATE_IDLE;
static bool             m_leds_clear          = false;
APP_TIMER_DEF(m_leds_timer_id);

APP_TIMER_DEF(m_eth_con_timer_id);
APP_TIMER_DEF(m_eth_timer_id);
APP_TIMER_DEF(m_det_timer_id);
APP_TIMER_DEF(m_alert_timer_id);

#if BUTTONS_NUMBER > 0

static bool m_long_timer_started;
APP_TIMER_DEF(m_long_timer_id);

static bool m_dblclk_timer_started;
APP_TIMER_DEF(m_dblclk_timer_id);
static void bsp_button_event_handler(uint8_t pin_no, uint8_t button_action);

static const bsp_button_cfg_t app_buttons[BUTTONS_NUMBER] =
{
#ifdef BSP_BUTTON_0
    {BSP_BUTTON_0,       APP_BUTTON_ACTIVE_LOW, BUTTON_PULL, bsp_button_event_handler},
#endif	
#ifdef BSP_BUTTON_1
    {BSP_BUTTON_1,       APP_BUTTON_ACTIVE_LOW, BUTTON_PULL, bsp_button_event_handler},
#endif	
#ifdef BSP_BUTTON_2
    {BSP_BUTTON_2,       APP_BUTTON_ACTIVE_LOW, BUTTON_PULL, bsp_button_event_handler},
#endif
#ifdef PWR_ON
    {PWR_ON,       APP_BUTTON_ACTIVE_HIGH, BUTTON_PULL, bsp_button_event_handler},
#endif	
};

static button_t m_buttons[BUTTONS_NUMBER];

bool bsp_button_is_pressed(uint32_t pin)
{
    return bsp_board_button_state_get(bsp_board_pin_to_button_idx(pin));
}

static void notify_event(uint32_t evt, uint32_t pin)
{
	push_event1((event_type_t)evt, pin);
}

// Function for handling button events.
static void bsp_button_event_handler(uint8_t pin_no, uint8_t button_action)
{
    uint32_t button_idx = 0;
    button_idx = bsp_board_pin_to_button_idx(pin_no);
    if (button_idx >= BUTTONS_NUMBER)
		return;
	button_t* p_button = &m_buttons[button_idx];

	// notify button event
	notify_event(((button_action==APP_BUTTON_PUSH)?EVT_pressed:EVT_released), pin_no);
	
	switch (p_button->state)
	{
		case bs_init :		// initial state
			if (button_action == APP_BUTTON_PUSH)
			{				
				NRF_LOG_INFO("Button %d push\n", pin_no);
				p_button->tick = app_timer_cnt_get();
				p_button->state = bs_pressed;					// click detect
				// start long push timer when previously stopped 
				if (!m_long_timer_started)
				{
					NRF_LOG_INFO("Start long touch timer\n", pin_no);
					app_timer_start(m_long_timer_id, LONG_CLICK_TICKS, NULL);
					m_long_timer_started = true;
				}
			}
			else if (button_action == APP_BUTTON_RELEASE)
			{
				NRF_LOG_INFO("Button %d released\n", pin_no);
				p_button->state = bs_init;						// init state
			}
			break;
		case bs_pressed:	// pressed state
			if (button_action == APP_BUTTON_RELEASE)
			{
				if (app_timer_is_timeout(p_button->tick, MIN_BUTTON_DURATION))
				{
		 			p_button->state = bs_double_click;
					
					// save last button click time
					p_button->tick = app_timer_cnt_get();
					
					if (!m_dblclk_timer_started)
					{
						NRF_LOG_INFO("Start dblclk timer\n", pin_no);
						app_timer_start(m_dblclk_timer_id, DBL_CLK_TICKS, NULL);
						m_dblclk_timer_started = true;
					}
				}
				else
				{
					p_button->state = bs_init;						// init state
				}
			}
			break;
		case bs_double_click : // check double click
			if (button_action == APP_BUTTON_RELEASE)
			{
				if (app_timer_is_timeout(p_button->tick, MIN_BUTTON_DURATION))
				{
					// generate click event
					NRF_LOG_INFO("Button %d double click\n", pin_no);
					
					notify_event(EVT_double_click, pin_no);
					
					// save last button click time
					p_button->tick = app_timer_cnt_get();
				}
				p_button->state = bs_init;						// init state
			}
			else if (button_action == APP_BUTTON_PUSH)
			{
				NRF_LOG_INFO("Dblclk %d pressed\n", pin_no);

				// save last button click time
				p_button->tick = app_timer_cnt_get();
				
				if (!m_long_timer_started)
				{
					NRF_LOG_INFO("Start long touch timer\n", pin_no);
					app_timer_start(m_long_timer_id, LONG_CLICK_TICKS, NULL);
					m_long_timer_started = true;
				}
			}
			
			break;
	}
}

static void long_timer_handler(void* p_context)
{
	uint32_t detect_count = 0;
	
	for (uint32_t i=0; i<BUTTONS_NUMBER; i++)
	{
		button_t* p_button = &m_buttons[i];
		if (p_button->state == bs_pressed ||
			p_button->state == bs_double_click)
		{
			if (app_timer_is_timeout(p_button->tick, BSP_LONG_PUSH_TIMEOUT_MS))
			{
				uint32_t pin_no = bsp_board_button_idx_to_pin(i);
				
				NRF_LOG_INFO("Button %d Long push\n", pin_no);
				p_button->state = bs_init;						// init state
				
				// generate long touch event
				notify_event(EVT_long_click, pin_no);
			}
			else
			{
				detect_count++;
			}
		}		
	}
	
	if (detect_count == 0)
	{
		app_timer_stop(m_long_timer_id);
		m_long_timer_started = false;
	}
}

static void dblclk_timer_handler(void* p_context)
{
	uint32_t remain_count = 0;
	
	for (uint32_t i=0; i<BUTTONS_NUMBER; i++)
	{
		button_t* p_button = &m_buttons[i];
		if (p_button->state == bs_double_click)
		{
			if (app_timer_is_timeout(p_button->tick, DBL_CLK_TOLERANCE))
			{
				uint32_t pin_no = bsp_board_button_idx_to_pin(i);
				NRF_LOG_INFO("Clicked : %d\n", pin_no);
				p_button->state = bs_init;
				
				// generate click event
				notify_event(EVT_click, pin_no);
			}
			else
			{
				remain_count++;
			}
		}		
	}
	
	if (remain_count == 0)
	{
		app_timer_stop(m_dblclk_timer_id);
		m_dblclk_timer_started = false;
	}
}

#endif

static void detect_timer_handler(void * p_context)
{
	bsp_board_led_off(BSP_LED_INDICATE_DETECT);
}

static void eth_timer_handler(void * p_context)
{
}

static void eth_con_timer_handler(void * p_context)
{
	bsp_board_led_invert(BSP_LED_INDICATE_ETH);
}

static void alert_led_timer_handler(void * p_context)
{
	static uint8_t indicate = 0;
	
	if (indicate++ == 0)
		bsp_board_leds_on();
	else
		bsp_board_leds_off();
}

/**@brief Configure indicators to required state.
 */
uint32_t bsp_indication_set(bsp_indication_t indicate)
{
    uint32_t err_code = NRF_SUCCESS;
    uint32_t next_delay = 0;

    if(m_leds_clear)
    {
        m_leds_clear = false;
        bsp_board_leds_off();
    }
	
	switch ((uint8_t)indicate)
	{
        case BSP_INDICATE_IDLE:
            bsp_board_led_off(BSP_LED_INDICATE_INDICATE_ADVERTISING);
            m_stable_state = indicate;
            break;
		case BSP_INDICATE_READY :
			//play_buzzer(SOUND_BEEP, 1);
			break;
        case BSP_INDICATE_ADVERTISING:
            // in advertising blink LED_0
            if (bsp_board_led_state_get(BSP_LED_INDICATE_INDICATE_ADVERTISING))
            {
                bsp_board_led_off(BSP_LED_INDICATE_INDICATE_ADVERTISING);
                next_delay = ADVERTISING_LED_OFF_INTERVAL;
				//trace("---> OFF");
            }
            else
            {
                bsp_board_led_on(BSP_LED_INDICATE_INDICATE_ADVERTISING);
                next_delay = ADVERTISING_LED_ON_INTERVAL;
				//trace("---> ON");
            }

            m_stable_state = indicate;
            app_timer_stop(m_leds_timer_id);
            err_code = app_timer_start(m_leds_timer_id, APP_TIMER_TICKS(next_delay), NULL);
            break;
			
        case BSP_INDICATE_CONNECTED:
            bsp_board_led_on(BSP_LED_INDICATE_CONNECTED);
            m_stable_state = indicate;
            break;
		case BSP_INDICATE_ALERT:
            app_timer_stop(m_alert_timer_id);
            err_code = app_timer_start(m_alert_timer_id, APP_TIMER_TICKS(ALERT_INTERVAL), NULL);
			bsp_board_leds_on();
			break;
		case BSP_INDICATE_DETECTED:
            app_timer_stop(m_det_timer_id);
            err_code = app_timer_start(m_det_timer_id, APP_TIMER_TICKS(DETECT_INTERVAL), NULL);
			bsp_board_led_on(BSP_LED_INDICATE_DETECT);
			break;
		// ethernet connect status
		case BSP_INDICATE_ETH_NOT_CONNECTED:
			app_timer_stop(m_eth_con_timer_id);
            app_timer_start(m_eth_con_timer_id, APP_TIMER_TICKS(ETH_CON_LED_INTERVAL), NULL);
			bsp_board_led_off(BSP_LED_INDICATE_ETH);
			break;
		case BSP_INDICATE_ETH_CONNECTED:
			app_timer_stop(m_eth_con_timer_id);
			bsp_board_led_on(BSP_LED_INDICATE_ETH);
			break;
		case BSP_INDICATE_ETH:
			app_timer_stop(m_eth_timer_id);
            app_timer_start(m_eth_timer_id, APP_TIMER_TICKS(ETH_LED_INTERVAL), NULL);
			bsp_board_led_invert(BSP_LED_INDICATE_ETH);
			break;
	}
	
    return err_code;
}

#if BUTTONS_NUMBER > 0
////////////////////////////////////////////////////////////////////////////////////////
// initialize buttons
////////////////////////////////////////////////////////////////////////////////////////
// initialize buttons
static void buttons_gpiote_evt_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
	uint32_t idx = bsp_board_pin_to_button_idx(pin); 
	bsp_button_cfg_t const * cfg = &app_buttons[idx];
	
	uint8_t state;
	
	state = ((cfg->active_state == nrf_gpio_pin_read(pin) ? APP_BUTTON_PUSH : APP_BUTTON_RELEASE));
	NRF_LOG_INFO("%d : %d, %d\n", pin, state, action);
	
	if (cfg->button_handler)
	{
		cfg->button_handler(pin, state);
	}
}

uint32_t bsp_buttons_init(void)
{
    uint32_t err_code;

    if (!nrf_drv_gpiote_is_init())
    {
        err_code = nrf_drv_gpiote_init();
        VERIFY_SUCCESS(err_code);
    }

	for (uint32_t i=0; i<BUTTONS_NUMBER; i++)
	{
        bsp_button_cfg_t const * p_btn = &app_buttons[i];
		nrf_drv_gpiote_in_config_t config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(false);
		config.pull = p_btn->pull_cfg;

		err_code = nrf_drv_gpiote_in_init(p_btn->pin_no, &config, buttons_gpiote_evt_handler);
		VERIFY_SUCCESS(err_code);
		
		nrf_drv_gpiote_in_event_enable(p_btn->pin_no, true);
    }
	
	return NRF_SUCCESS;
}

#endif

static void leds_timer_handler(void * p_context)
{
	trace("--- Toggle");
	UNUSED_VARIABLE(bsp_indication_set(m_stable_state));
}

////////////////////////////////////////////////////////////////////////////////////////
// initialize board support packages
uint32_t bsp_init(void)
{
    uint32_t err_code = NRF_SUCCESS;

#if BUTTONS_NUMBER > 0
	///////////////////////////////////////////////////////////////////////////////////
	// initialize buttons
	for (uint32_t i=0; i<BUTTONS_NUMBER; i++)	
	{
		m_buttons[i].state = bs_init;
		m_buttons[i].button_state = APP_BUTTON_RELEASE;
	}
	
	bsp_buttons_init();

	err_code = app_timer_create(&m_long_timer_id,
								APP_TIMER_MODE_REPEATED,
								long_timer_handler);
	RETURN_IF_ERROR(err_code);
	
	err_code = app_timer_create(&m_dblclk_timer_id,
								APP_TIMER_MODE_REPEATED,
								dblclk_timer_handler);
	RETURN_IF_ERROR(err_code);
#endif
	
	// initialize LEDs
	bsp_board_init(BSP_INIT_LEDS);
	
	err_code = app_timer_create(&m_leds_timer_id, 
								APP_TIMER_MODE_SINGLE_SHOT, 
								leds_timer_handler);
	RETURN_IF_ERROR(err_code);

	// detect
	err_code = app_timer_create(&m_det_timer_id, APP_TIMER_MODE_SINGLE_SHOT, detect_timer_handler);
	RETURN_IF_ERROR(err_code);

	// eth
	err_code = app_timer_create(&m_eth_timer_id, APP_TIMER_MODE_SINGLE_SHOT, eth_timer_handler);
	RETURN_IF_ERROR(err_code);

	// eth conn
	err_code = app_timer_create(&m_eth_con_timer_id, APP_TIMER_MODE_REPEATED, eth_con_timer_handler);
	RETURN_IF_ERROR(err_code);

	// alert
	err_code = app_timer_create(&m_alert_timer_id, 
		APP_TIMER_MODE_REPEATED, 
		alert_led_timer_handler);
	RETURN_IF_ERROR(err_code);
		
	m_pwm_buzzer_init(NULL);
		
    return NRF_SUCCESS;
}

#if BUTTONS_NUMBER > 0

uint32_t bsp_buttons_enable()
{
    ASSERT(app_buttons);

    uint32_t i;
    for (i = 0; i < BUTTONS_NUMBER; i++)
    {
        nrf_drv_gpiote_in_event_enable(app_buttons[i].pin_no, true);
    }

    return NRF_SUCCESS;
}

uint32_t bsp_buttons_disable()
{
    ASSERT(app_buttons);

    uint32_t i;
    for (i = 0; i < BUTTONS_NUMBER; i++)
    {
        nrf_drv_gpiote_in_event_disable(app_buttons[i].pin_no);
    }

    return NRF_SUCCESS;
}

extern uint32_t *pFuncHandle;
uint32_t bsp_wakeup_button_enable(uint32_t button_idx)
{	
    nrf_gpio_cfg_sense_set(button_idx,
            BUTTONS_ACTIVE_STATE ? NRF_GPIO_PIN_SENSE_HIGH :NRF_GPIO_PIN_SENSE_LOW);	
	
    return NRF_SUCCESS;
}

uint32_t bsp_wakeup_button_disable(uint32_t pin)
{
    nrf_gpio_cfg_sense_set(pin,
                           NRF_GPIO_PIN_NOSENSE);
    return NRF_SUCCESS;
}

uint32_t bsp_sleep_mode_prepare(void)
{
    ret_code_t err_code = bsp_buttons_disable();
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    err_code = bsp_wakeup_button_enable(PWR_ON);
    if (err_code == NRF_ERROR_NOT_SUPPORTED)
    {
        return NRF_SUCCESS;
    }

    return err_code;
}

#endif
