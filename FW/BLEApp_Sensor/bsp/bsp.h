#ifndef BSP_H__
#define BSP_H__

#include <stdint.h>
#include <stdbool.h>
#include "boards.h"

#if !defined(BSP_DEFINES_ONLY) && !defined(BSP_SIMPLE)
#include "app_button.h"

#define BSP_BUTTON_ACTION_PUSH      (APP_BUTTON_PUSH)    /**< Represents pushing a button. See @ref bsp_button_action_t. */
#define BSP_BUTTON_ACTION_RELEASE   (APP_BUTTON_RELEASE) /**< Represents releasing a button. See @ref bsp_button_action_t. */
#define BSP_BUTTON_ACTION_LONG_PUSH (2)                  /**< Represents pushing and holding a button for @ref BSP_LONG_PUSH_TIMEOUT_MS milliseconds. See also @ref bsp_button_action_t. */

#endif

/* BSP_UART_SUPPORT
 * This define enables UART support module.
 */

#ifdef __cplusplus
extern "C" {
#endif

typedef uint8_t bsp_button_action_t; /**< The different actions that can be performed on a button. */

typedef enum
{
    BSP_INDICATE_FIRST = 0,
    BSP_INDICATE_IDLE  = BSP_INDICATE_FIRST, 
	BSP_INDICATE_READY,
    BSP_INDICATE_SCANNING,                   
    BSP_INDICATE_ADVERTISING,                
	BSP_INDICATE_CONNECTED,
    BSP_INDICATE_PRE_ALERT,                    
    BSP_INDICATE_ALERT_ON,                    
    BSP_INDICATE_ALERT_OFF,

	BSP_INDICATE_ETH_NOT_CONNECTED,
	BSP_INDICATE_ETH_CONNECTED,
	
	BSP_INDICATE_UART,
	BSP_INDICATE_ETH,
	
	BSP_INDICATE_DETECTED,
	
    BSP_INDICATE_ALERT,                    
} bsp_indication_t;

uint32_t bsp_init(void);

bool bsp_button_is_pressed(uint32_t button);
uint32_t bsp_indication_set(bsp_indication_t indicate);

uint32_t bsp_buttons_enable(void);
uint32_t bsp_buttons_disable(void);

uint32_t bsp_wakeup_button_enable(uint32_t button_idx);
uint32_t bsp_wakeup_button_disable(uint32_t button_idx);

uint32_t bsp_sleep_mode_prepare(void);

#ifdef __cplusplus
}
#endif

#endif // BSP_H__

/** @} */
