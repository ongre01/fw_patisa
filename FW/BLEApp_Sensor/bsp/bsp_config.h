#ifndef BSP_CONFIG_H__
#define BSP_CONFIG_H__

#include <stdint.h>
#include <stdbool.h>
#include "boards.h"

#ifdef __cplusplus
extern "C" {
#endif

#if !defined(BSP_DEFINES_ONLY) && !defined(BSP_SIMPLE)
#include "app_button.h"

#define BSP_BUTTON_ACTION_PUSH      (APP_BUTTON_PUSH)    /**< Represents pushing a button. See @ref bsp_button_action_t. */
#define BSP_BUTTON_ACTION_RELEASE   (APP_BUTTON_RELEASE) /**< Represents releasing a button. See @ref bsp_button_action_t. */
#define BSP_BUTTON_ACTION_LONG_PUSH (2)                  /**< Represents pushing and holding a button for @ref BSP_LONG_PUSH_TIMEOUT_MS milliseconds. See also @ref bsp_button_action_t. */
#endif

#define BSP_MS_TO_TICK(MS) (m_app_ticks_per_100ms * (MS / 100))


#define BUTTON_ERASE_BONDING BSP_BUTTON_0_MASK
#define BUTTON_ERASE_ALL     BSP_BUTTON_1_MASK
#define BUTTON_ADVERTISE     BSP_BUTTON_0_MASK
#define BUTTON_CLEAR_EVT     BSP_BUTTON_1_MASK
#define BUTTON_CAPSLOCK      BSP_BUTTON_2_MASK
#define BSP_BUTTONS_ALL      0xFFFFFFFF
#define BSP_BUTTONS_NONE     0

#define BSP_LONG_PUSH_TIMEOUT_MS (3000) /**< The time to hold for a long push (in milliseconds). */
/**@brief Types of BSP initialization.
 */

#define ADVERTISING_LED_ON_INTERVAL   			100
#define ADVERTISING_LED_OFF_INTERVAL  			900

#define ADVERTISING_DIRECTED_LED_ON_INTERVAL   200
#define ADVERTISING_DIRECTED_LED_OFF_INTERVAL  200

#define ADVERTISING_WHITELIST_LED_ON_INTERVAL  200
#define ADVERTISING_WHITELIST_LED_OFF_INTERVAL 800

#define ADVERTISING_SLOW_LED_ON_INTERVAL       400
#define ADVERTISING_SLOW_LED_OFF_INTERVAL      4000

#define BONDING_INTERVAL                       100
 
#define TX_OK_INTERVAL                        10
#define RX_OK_INTERVAL                        10

#define LTE_CONNECT_INTERVAL                   500
#define LTE_SIM_ERROR_INTERVAL	               50
#define LTE_NET_ERROR_INTERVAL	               100
#define LTE_ERROR_INTERVAL	                   50

#define ALERT_INTERVAL                         200
#define DETECT_INTERVAL                        200
#define UART_LED_INTERVAL                      10
#define ETH_LED_INTERVAL                       100
#define ALERT_INTERVAL                         200

#define ETH_CON_LED_INTERVAL                   100

#define BSP_LED_INDICATE_SENT_OK               BSP_BOARD_LED_0
#define BSP_LED_INDICATE_SEND_ERROR            BSP_BOARD_LED_0
#define BSP_LED_INDICATE_RCV_OK                BSP_BOARD_LED_0
#define BSP_LED_INDICATE_RCV_ERROR             BSP_BOARD_LED_0
#define BSP_LED_INDICATE_CONNECTED             BSP_BOARD_LED_0
#define BSP_LED_INDICATE_BONDING               BSP_BOARD_LED_0
#define BSP_LED_INDICATE_ADVERTISING_DIRECTED  BSP_BOARD_LED_0
#define BSP_LED_INDICATE_ADVERTISING_SLOW      BSP_BOARD_LED_0
#define BSP_LED_INDICATE_ADVERTISING_WHITELIST BSP_BOARD_LED_0
#define BSP_LED_INDICATE_INDICATE_ADVERTISING  BSP_BOARD_LED_0

#define BSP_LED_INDICATE_DETECT   	       	   BSP_BOARD_LED_0
#define BSP_LED_INDICATE_ETH                   BSP_BOARD_LED_1

#ifdef __cplusplus
}
#endif

#endif // BSP_CONFIG_H__

/** @} */
