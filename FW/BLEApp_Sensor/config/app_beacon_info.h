#ifndef _app_beacon_info_h
#define _app_beacon_info_h

#include "ble_beacon_info.h"
#include "app_packet.h"

#define APP_BEACON_IDENTIFIER	0x4D523443				// Mobilian RS-485 Converter

#pragma pack(1)

ANON_UNIONS_ENABLE;		

typedef	union
{
	uint8_t		buffer[BLE_BEACON_INFO_LENGTH];
	struct
	{
		uint8_t 	device_type;						// manufacturer specific information, specifies the device type in this implementation
		uint8_t 	adv_data_length;					// manufacturer specific information, specifies the length of the manufacturer specific data in this implementation

		// UUID
		union
		{
			uint8_t uuid[BLE_BEACON_UUID_LENGTH];
		};
		
		uint16_t	major, minor;
		
		// Measured RSSI
		int8_t  	measured_rssi;						// manufacturere specific informatio, the beacon's measured TX power
	};
} app_beacon_info_t;
ANON_UNIONS_DISABLE;

typedef struct
{
	uint16_t 			company_identifier;
	app_beacon_info_t 	bi;
} app_beacon_manuf_t;

#pragma pack()

#endif // _app_beacon_info_h
