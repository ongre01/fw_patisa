#pragma once

#include "sdk_common.h"
#include "ble_gap.h"
#include "app_util_platform.h"

#define MAX_PACKET		64
#define STX				0x02
#define ETX				0x03
#define CRC_LEN			2

#pragma pack(1)

#define ADDR_BLE		0x01
#define ADDR_APP		0x02

#define CMD_SETTING		0x01
#define CMD_START		0x02
#define CMD_STOP		0x03
#define CMD_PAUSE		0x04
#define CMD_DATA		0x011

#define ACK				0x06
#define NAK				0x15

typedef struct
{
	uint8_t addr;
	uint8_t len;
	uint8_t cmd;
} app_packet_header_t;

typedef struct
{
	uint16_t start, step, stop, step_period, duration;
} app_packet_set_t;

typedef struct
{
	app_packet_set_t s1, s2;
} app_packet_setting_t;

typedef struct __attribute__((packed))
{
	uint32_t temperature;
	float tphase;
	uint32_t pressure;
	float pphase;
  uint32_t impedance;
	uint16_t imp_voltage;
	uint8_t  imp_freq[3];
	float iphase;
} app_packet_data_t;

ANON_UNIONS_ENABLE;
typedef struct
{
	app_packet_header_t header;
	union
	{
		uint8_t              buf[1];
		app_packet_setting_t setting;
		app_packet_data_t    data; 
	};
} app_packet_t;
ANON_UNIONS_DISABLE;

#pragma pack()

