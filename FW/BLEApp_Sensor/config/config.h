#ifndef _config_h
#define _config_h

#include "sdk_common.h"
#include "boards.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

#include "m_event_queue.h"

#define SW_VERSION						0x01000000	// (1.0.0.0)
#define CONFIG_SAVE_DELAY_MS			1000

#define DEF_WDT_RELOAD_MS				2000

///////////////////////////////////////////////////////////////////////////////////
#define APP_BLE_CONN_CFG_TAG            1                                           // A tag identifying the SoftDevice BLE configuration. 

#define DEF_ADV_NAME	              	"DU-001" 	                           		// Name of device. Will be included in the advertising data. 
#define DEF_COMPANY_IDENTIFIER			0x4253 // BS

#define NUS_SERVICE_UUID_TYPE           BLE_UUID_TYPE_VENDOR_BEGIN                  // UUID type for the Nordic UART Service (vendor specific). 

#define APP_BLE_OBSERVER_PRIO           3                                           // Application's BLE observer priority. You shouldn't need to modify this value. 

#define DEF_ADV_INTERVAL_MS             20                                          // The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). 
#define APP_ADV_INTERVAL                MSEC_TO_UNITS(DEF_ADV_INTERVAL_MS, UNIT_0_625_MS)                                          // The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). 
#define APP_ADV_UPDATE_INTERVAL_MS		1000

#define ADV_DURATION_INFINITE       	0xFFFFFFFFU
#define APP_ADV_DURATION                (APP_ADV_DURATION_INFINITE) //180000 - 2020.01.29 무한 advertising으로 변경                                     // The advertising duration (180 seconds)

#define DEF_MIN_CONN_INTERVAL_MS        7.5f
#define DEF_MAX_CONN_INTERVAL_MS        1000
#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(DEF_MIN_CONN_INTERVAL_MS, UNIT_1_25_MS)             // Minimum acceptable connection interval (20 ms), Connection interval uses 1.25 ms units. 
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(DEF_MAX_CONN_INTERVAL_MS, UNIT_1_25_MS)             // Maximum acceptable connection interval (75 ms), Connection interval uses 1.25 ms units. 

#define DEF_SLAVE_LATENCY               0                                           // Slave latency. 
#define DEF_CONN_SUP_TIMEOUT			4000
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(DEF_CONN_SUP_TIMEOUT, UNIT_10_MS)             // Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. 
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000)                       // Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). 
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000)                      // Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). 
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           // Number of attempts before giving up the connection parameter negotiation. 

#define DEF_TX_POWER        			0

#define SEC_PARAM_BOND                  1                                           // Perform bonding. 
#define SEC_PARAM_MITM                  0                                          // Man In The Middle protection not required. 
#define SEC_PARAM_LESC                  0                                           // LE Secure Connections not enabled. 
#define SEC_PARAM_KEYPRESS              0                                           // Keypress notifications not enabled. 
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_DISPLAY_ONLY                        // No I/O capabilities. 
#define SEC_PARAM_OOB                   0                                           // Out Of Band data not available. 
#define SEC_PARAM_MIN_KEY_SIZE          7                                           // Minimum encryption key size. 
#define SEC_PARAM_MAX_KEY_SIZE          16                                          // Maximum encryption key size. 

///////////////////////////////////////////////////////////////////////////////////
// BLE services
#define BLE_SERVICES_MAX				4

#define BLE_SERVICE_NUS     			0
#define BLE_SERVICE_NUS_C     			1
#define BLE_SERVICE_DFU          		2
#define BLE_SERVICE_DIS          		3

///////////////////////////////////////////////////////////////////////////////////
#define DEAD_BEEF                       0xDEADBEEF                                  // Value used as error code on stack dump, can be used to identify stack location on stack unwind. 

#define SCHED_MAX_EVENT_DATA_SIZE      	64              // Maximum size of scheduler events. 
#define SCHED_QUEUE_SIZE           		16                                         	// Maximum number of events in the scheduler queue. More is needed in case of Serialization. 

///////////////////////////////////////////////////////////////////////////////////
#define UART_TX_BUF_SIZE            1024                                        // UART TX buffer size. 
#define UART_RX_BUF_SIZE            1024                                        // UART RX buffer size. 

#define SERIAL_FIFO_TX_SIZE 		UART_TX_BUF_SIZE
#define SERIAL_FIFO_RX_SIZE 		UART_RX_BUF_SIZE

#define SERIAL_BUFF_TX_SIZE 		1
#define SERIAL_BUFF_RX_SIZE 		1

#define SERIAL_UART_INSTANCE_NUMBER  	0

#define COMM_BLOCK_MAX_DATA_LEN	 	 	64
#define COMM_BLOCK_BLE_MAX_DATA_LEN	  	128

#define COMM_BLOCK_UART_TRAN_TOL_MS 	10
#define COMM_BLOCK_USB_TRAN_TOL_MS 		5
#define COMM_BLOCK_BLE_TRAN_TOL_MS 		5

#define NUS_RX_BUF_SIZE					512

///////////////////////////////////////////////////////////////////////////////////
// config save latency
#define CONFIG_SAVE_DELAY				1000

///////////////////////////////////////////////////////////////////////////////////
#define DEF_RESET_REQ_MS				500
#define DEF_SLEEP_REQ_MS				500

///////////////////////////////////////////////////////////////////////////////////
#define APP_GPIOTE_MAX_USERS			10

///////////////////////////////////////////////////////////////////////////////////////
// Scanner parameter
#define SCAN_INTERVAL			20
#define SCAN_WINDOW				20
#define SCAN_ACTIVE             1      // If 1, performe active scanning (scan requests).
#define SCAN_SELECTIVE          0      // If 1, ignore unknown devices (non whitelisted).
#define SCAN_TIMEOUT            0x0000 // Timout when scanning. 0x0000 disables timeout.

#define DEF_NEAR_TARGET_RSSI	-50
#define DEF_SCAN_PREFIX			DEF_ADV_NAME_PERI						// Used by central scanner(dongle) to determine the target
#define DEF_SCAN_COMP_COUNT		10										// Used by central scanner(dongle) to determine the target
#define DEF_SCAN_COMP_TIMEOUT	3000									// Used by central scanner(dongle) to determine the target

#define TIMESLOT_ALIVE_TOLERANCE_MS (3 * 1000)
#define TIMESLOT_CHECK_INTERVAL_MS	1000

/////////////////////////////////////////////////////////////
#define DEF_UNDISCOVER_REMOVE_SEC	(10 * 1000)
#define UNDISCOVER_CHECK_PERIOD_MS  1000		// undiscover timer period ms

/////////////////////////////////////////////////////////////
// ETH
#define ETH_SPI_INSTANCE 	0
#define SOCKET_DHCP			3
#define TCP_CLI_SN			1
#define TST_CLI_SN			0
#define UDP_CLI_SN			2

#define TCP_CLIENT_RX_SIZE		1024
#define TCP_CLIENT_TX_SIZE		2048

#if DEBUG
	#define TCP_CLIENT_PERMIT_SEC	(10*1000)
#else
	#define TCP_CLIENT_PERMIT_SEC	(5*60*1000)
#endif

#define COMM_CHANNEL_ID_SYS_MANAGER	(comm_channel_id_user + 1)

#define STACK_SIZE_APP_THREAD	1024
#define STACK_SIZE_ADC			1024

///////////////////////////////////////////////////////////////////////////////////
#define trace(...) NRF_LOG_INFO(__VA_ARGS__);

#define LIGHT_BUTTON			BUTTON_1
#define RESET_BUTTON			BUTTON_2

///////////////////////////////////////////////////////////////////////////////////
// Eent handler priorities
#define MAX_EVENT_COUNT					64

#define M_APP_EVENT_PRIORITIES			0
#define M_BLE_EVENT_PRIORITIES			1
#define BLE_PACKET_EVENT_PRIORITIES		2
#define M_USB_EVENT_PRIORITIES			2
#define M_NUS_EVENT_PRIORITIES			2
#define M_MENU_EVENT_PRIORITIES			3


#define DEV_INVISIBLE_MS				(2 * 1000)

///////////////////////////////////////////////////////////////////////////////////
// PWM definition
#define PWM_DRV_CH_CNT					1			// PWM0사용
#define PWM_CHANNEL_COUNT				1			// 1개 사용
#define DRV_PWM_BASE_CLOCK				16000000	// 16MHz
/////////////////////////////////////////////////////////////
#define PWM0_0							BUZZER
#define PWM0_1							NRFX_PWM_PIN_NOT_USED
#define PWM0_2							NRFX_PWM_PIN_NOT_USED
#define PWM0_3							NRFX_PWM_PIN_NOT_USED
#define PWM0_PIN						{PWM0_0, PWM0_1, PWM0_2, PWM0_3}

#define SPI_INSTANCE_AD594x				0
#define TWI_INSTANCE_MAX17330			1

#endif // _config_h
