#ifndef _event_def_h
#define _event_def_h

typedef enum
{
	EVT_none				,
	EVT_menu_changed		,
	EVT_change_menu			,
	
	EVT_connected			,
	EVT_disconnected		,
	EVT_adv_changed			,
	EVT_set_tx_power		,
	
	EVT_pressed				,
	EVT_released			,
	EVT_click				,
	EVT_double_click		,
	EVT_long_click			,
	
	EVT_erase_config		,
	EVT_config_changed		,
	EVT_flash_result		,
	EVT_reset_config		,
	EVT_save_config			,
	
	EVT_reset				,
	EVT_go_sleep			,
	
	EVT_delay_event			,
		
	EVT_set_adv				,			// advertising ����
	EVT_adv_param_changed	,
	EVT_app_ready			,
	
	EVT_nus_comm_stat		,

	EVT_acm_open			,
	EVT_acm_closed			,
	EVT_acm_tx_done			,
	EVT_acm_rx_done			,
	EVT_usb_ready			,
	
	EVT_net_changed			,	// MAC or IP changed
	EVT_server_changed		, 	// server info changed
} event_type_t;

#endif // _event_def_h
