#ifndef BOARD_LIFT_MAIN_52840_H
#define BOARD_LIFT_MAIN_52840_H

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_gpio.h"
#define PCB_V04
/////////////////////////////////////////////////////////////////////
#define BUTTONS_NUMBER 			1

#define BUTTON_PULL    			GPIO_PIN_CNF_PULL_Disabled
#define BUTTONS_ACTIVE_STATE	1		// Active High	
#define PWR_ON				NRF_GPIO_PIN_MAP(0, 15)
#define BUTTONS_LIST {PWR_ON}

/////////////////////////////////////////////////////////////////////
#ifdef PCB_V04
#define LEDS_NUMBER			6
#else
#define LEDS_NUMBER			3
#endif

#define LED_1		NRF_GPIO_PIN_MAP(1, 0)	// B
#define LED_2		NRF_GPIO_PIN_MAP(0, 24)	// G
#define LED_3		NRF_GPIO_PIN_MAP(0, 21)	// R

#ifdef PCB_V04
#define LED_4   NRF_GPIO_PIN_MAP(0, 13)	// R
#define LED_5   NRF_GPIO_PIN_MAP(0, 11)	// G
#define LED_6   NRF_GPIO_PIN_MAP(0, 16)	// B

#define LEDS_LIST	{LED_1, LED_2, LED_3, LED_4, LED_5, LED_6}
#else
#define LEDS_LIST	{LED_1, LED_2, LED_3}
#endif

#define LEDS_ACTIVE_STATE	0
#define LEDS_INV_MASK  LEDS_MASK


/////////////////////////////////////////////////////////////////////
// UART
#define TX_PIN_NUMBER		NRF_GPIO_PIN_MAP(0, 19)
#define RX_PIN_NUMBER		NRF_GPIO_PIN_MAP(0, 22)

/////////////////////////////////////////////////////////////////////
// MAX17330
#define MAX17330_ALERT		NRF_GPIO_PIN_MAP(0, 7)
#define MAX17330_SDA		NRF_GPIO_PIN_MAP(0, 6)
#define MAX17330_SCL		NRF_GPIO_PIN_MAP(0, 27)
#define MAX17330_I2C_ADDR_MODEL_GAUGE		(0x36)
#define MAX17330_I2C_ADDR_NONVOLATILE_DATA	(0x0B)
#ifdef PCB_V04
#define MAX17330_ALRT_PIO NRF_GPIO_PIN_MAP(1, 2)
#endif

/////////////////////////////////////////////////////////////////////
// AD5941
#define AD5941_RESET		NRF_GPIO_PIN_MAP(1, 8)
#define AD5941_MISO			NRF_GPIO_PIN_MAP(0, 12)
#define AD5941_MOSI			NRF_GPIO_PIN_MAP(1, 9)
#define AD5941_SCLK			NRF_GPIO_PIN_MAP(0, 8)
#define AD5941_CS			NRF_GPIO_PIN_MAP(0, 5)

#ifdef PCB_V04
#define AD5941_GP0			NRF_GPIO_PIN_MAP(0, 4)
#define AD5941_GP1			NRF_GPIO_PIN_MAP(0, 26)
#define AD5941_GP2			NRF_GPIO_PIN_MAP(0, 31)
#else
#define AD5941_GP0			NRF_GPIO_PIN_MAP(0, 16)
#endif

#define VDD_3V3_EN			NRF_GPIO_PIN_MAP(0, 14)

#define VBUS_SENS			NRF_GPIO_PIN_MAP(0, 20)

#define BUZZER				NRF_GPIO_PIN_MAP(0, 17)

#ifdef __cplusplus
}
#endif

#endif // BOARD_LIFT_MAIN_52840_H
