#include "nrf_drv_spi.h"
#include "nrf_drv_gpiote.h"
#include "macros_common.h"
#include "nrf_delay.h"
#include "utility.h"
#include "app_scheduler.h"
#include "config.h"

#define  NRF_LOG_MODULE_NAME AD5940
#include "nrf_log.h"

NRF_LOG_MODULE_REGISTER();

#include "ad5940.h"
#include "stdio.h"

volatile static uint8_t ucInterrupted = 0;      

static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE_AD594x);


void AD5940_ReadWriteNBytes(unsigned char *pSendBuffer,unsigned char *pRecvBuff,unsigned long length)
{
	uint32_t err_code;
	
	err_code = nrf_drv_spi_transfer(&spi, 
									pSendBuffer, length, 
									pRecvBuff, length);
	REPORT_IF_ERROR(err_code);
}

void AD5940_CsClr(void)
{
	nrf_gpio_pin_write(AD5941_CS, 0);
}

void AD5940_CsSet(void)
{
	nrf_gpio_pin_write(AD5941_CS, 1);
}

void AD5940_RstSet(void)
{
	nrf_gpio_pin_write(AD5941_RESET, 1);
}

void AD5940_RstClr(void)
{
	nrf_gpio_pin_write(AD5941_RESET, 0);
}

void AD5940_Delay10us(uint32_t time)
{
	nrf_delay_us(time * 10);
}

uint32_t AD5940_GetMCUIntFlag(void)
{
	return ucInterrupted;
}

uint32_t AD5940_ClrMCUIntFlag(void)
{
	ucInterrupted = 0;
	return 1;
}

static void ad5940_gpiote_evt_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
	ucInterrupted = 1;
}

uint32_t AD5940_MCUResourceInit(void *pCfg)
{
	uint32_t err_code;
	
	// gpiote
    if (!nrf_drv_gpiote_is_init())
    {
        err_code = nrf_drv_gpiote_init();
        VERIFY_SUCCESS(err_code);
    }
	
	// init spi
	nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
	spi_config.frequency = NRF_DRV_SPI_FREQ_8M;
	spi_config.mode      = NRF_DRV_SPI_MODE_0;
	
	spi_config.ss_pin   = NRF_DRV_SPI_PIN_NOT_USED;
	spi_config.miso_pin = AD5941_MISO;
	spi_config.mosi_pin = AD5941_MOSI;
	spi_config.sck_pin  = AD5941_SCLK;
	err_code = nrf_drv_spi_init(&spi, &spi_config, NULL, NULL);	
	RETURN_IF_ERROR(err_code);

	nrf_gpio_cfg_output(AD5941_CS);
	nrf_gpio_cfg_output(AD5941_RESET);
	
	AD5940_CsSet();
	AD5940_RstSet();
  
	// AD5940 - GP-0 Falling edige
	nrf_drv_gpiote_in_config_t config = GPIOTE_CONFIG_IN_SENSE_HITOLO(false);
	config.pull = NRF_GPIO_PIN_PULLUP;

	err_code = nrf_drv_gpiote_in_init(AD5941_GP0, &config, ad5940_gpiote_evt_handler);
	VERIFY_SUCCESS(err_code);
	
	nrf_drv_gpiote_in_event_enable(AD5941_GP0, true);
	
	return NRF_SUCCESS;
}
