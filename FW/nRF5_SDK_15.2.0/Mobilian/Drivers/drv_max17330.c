#include "drv_max17330.h"
#include "nrf_drv_gpiote.h"
#include "twi_manager.h"
#include "macros_common.h"
#include "nrf_delay.h"
#include "utility.h"
#include "app_scheduler.h"

#include "m_usb_com.h"

#define  NRF_LOG_MODULE_NAME drv_max17730
#include "nrf_log.h"

NRF_LOG_MODULE_REGISTER();

static uint32_t max17330_nvmem_show(void);
static uint32_t max17330_copy_shwmem_to_nvm(void);
static uint16_t max17330_NVM_Remain_Update(void);

static struct max17330_cfg_typedef
{
	uint8_t                             twi_addr_model_gauge;
	uint8_t                             twi_addr_nonvolatile_data;
	uint8_t                             int_pin;
	drv_max17330_int_handler_t			int_handler;
    nrf_drv_twi_t        const * 		p_twi_instance;    // TWI instance.
    nrf_drv_twi_config_t const * 		p_twi_config;     	// TWI configuraion.
	
	drv_max17330_config_t               config;
} m_max17330_cfg;

static __inline uint32_t twi_open(void)
{
    uint32_t err_code;

    err_code = twi_manager_request(m_max17330_cfg.p_twi_instance,
                                   m_max17330_cfg.p_twi_config,
                                   NULL,
                                   NULL);
	
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(m_max17330_cfg.p_twi_instance);

    return NRF_SUCCESS;
}


static __inline uint32_t twi_close(void)
{
    nrf_drv_twi_disable(m_max17330_cfg.p_twi_instance);

    nrf_drv_twi_uninit(m_max17330_cfg.p_twi_instance);

    return NRF_SUCCESS;
}

static __inline uint8_t get_twi_addr(uint16_t reg_addr)
{
	if ((reg_addr & 0x100) == 0x100)
		return m_max17330_cfg.twi_addr_nonvolatile_data;
	return m_max17330_cfg.twi_addr_model_gauge;
}


static uint32_t i2c_read(uint8_t twi_addr, uint8_t reg_addr, uint16_t* val)
{
    uint32_t err_code;
	
    err_code = nrf_drv_twi_tx( m_max17330_cfg.p_twi_instance,
                               twi_addr,
                               &reg_addr,
                               sizeof(reg_addr),
                               true );
    RETURN_IF_ERROR(err_code);

    err_code = nrf_drv_twi_rx( m_max17330_cfg.p_twi_instance,
                               twi_addr,
                               (uint8_t*)val,
                               sizeof(*val));
    RETURN_IF_ERROR(err_code);

    return NRF_SUCCESS;
}


static uint32_t i2c_write(uint8_t twi_addr, uint8_t reg_addr, uint16_t val)
{
    uint32_t err_code;
    uint8_t buffer[1 + sizeof(val)];

	buffer[0] = reg_addr;
	memcpy(buffer+1, &val, sizeof(val));

    err_code = nrf_drv_twi_tx( m_max17330_cfg.p_twi_instance,
                               twi_addr,
                               buffer,
                               sizeof(buffer),
                               false );
    RETURN_IF_ERROR(err_code);

    return NRF_SUCCESS;
}

static uint32_t i2c_update(uint8_t twi_addr, uint8_t reg_addr, uint16_t mask, uint16_t val)
{
    uint32_t err_code;
	uint16_t data;

	err_code = i2c_read(twi_addr, reg_addr, &data);
	RETURN_IF_ERROR(err_code);
	
	data &= ~mask;
	data |= val;
	
	err_code = i2c_write(twi_addr, reg_addr, data);
	RETURN_IF_ERROR(err_code);
	
    return NRF_SUCCESS;
}

static uint32_t reg_read(uint16_t reg_addr, uint16_t* val)
{
    uint32_t err_code;
	uint8_t  twi_addr = get_twi_addr(reg_addr);
	uint8_t  reg      = (reg_addr & 0xFF);
	
    err_code = i2c_read(twi_addr, reg, val);
    RETURN_IF_ERROR(err_code);

    return NRF_SUCCESS;
}


static uint32_t reg_write(uint16_t reg_addr, uint16_t val)
{
    uint32_t err_code;

	uint8_t  twi_addr = get_twi_addr(reg_addr);
	uint8_t  reg      = (reg_addr & 0xFF);
		
	uint16_t stat;
	uint16_t attempt = 0;
	
	err_code = reg_read(COMMSTAT_REG, &stat);
    RETURN_IF_ERROR(err_code);
	
	// Unlock write protection
	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, stat & 0xFF06);
	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, stat & 0xFF06);

	// write data to the specified reg
	do
	{
		err_code = i2c_write(twi_addr, reg, val);
		RETURN_IF_ERROR(err_code);	
		err_code = reg_read(COMMSTAT_REG, &stat);
		RETURN_IF_ERROR(err_code);	
	}while(val != stat && attempt++ < 3);

  	// lock write protection
	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, stat | 0x00F9);
	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, stat | 0x00F9);

	
    return NRF_SUCCESS;
}

static void max17330_app_sched_evt_handler(void* p_data, uint16_t size)
{
	drv_max17330_evt_t evt;	
	
	// get status
	drv_max17330_get_events(&evt);
	
	// Clear alerts
	reg_write(STATUS_REG, evt.status & MAX17330_STATUS_ALRT_CLR_MASK);
	
	if (m_max17330_cfg.int_handler)
		m_max17330_cfg.int_handler(evt);
}

static void gpiote_evt_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
	NRF_LOG_INFO("MAX17330 interrupt : 0x%x", nrf_gpio_pin_read(pin));
	app_sched_event_put(NULL, 0, max17330_app_sched_evt_handler);
}
 
static void max17330_set_alert_thresholds(void)
{
	uint16_t val;
	
	/* Set VAlrtTh */
	val  = (m_max17330_cfg.config.volt_min / 20);
	val |= ((m_max17330_cfg.config.volt_max / 20) << 8);
	reg_write(VALRTTH_REG, val);

	/* Set TAlrtTh */
	val  = m_max17330_cfg.config.temp_min & 0xFF;
	val |= ((m_max17330_cfg.config.temp_max & 0xFF) << 8);
	reg_write(TALRTTH_REG, val);

	/* Set SAlrtTh */
	val  = m_max17330_cfg.config.soc_min;
	val |= (m_max17330_cfg.config.soc_max << 8);
	reg_write(SALRTTH_REG, val);

	/* Set IAlrtTh */
	val  = (m_max17330_cfg.config.curr_min * m_max17330_cfg.config.rsense / 400) & 0xFF;
	val |= (((m_max17330_cfg.config.curr_max * m_max17330_cfg.config.rsense / 400) & 0xFF) << 8);
	reg_write(IALRTTH_REG, val);
}

#define MAX17330_RSENSE 10
#define ROOM_CHG_CUR ((uint16_t)(550/ 100 * MAX17330_RSENSE + 0.5)-1)
#define CHG_CUR_WARMCOEF 0
#define CHG_CUR_COLDCOEF 0
#define CHG_CUR_HOTCOEF  0
#define NICHGCFG_REG_VAL (CHG_CUR_HOTCOEF & 0x7) | ((CHG_CUR_COLDCOEF & 0x7) << 3) | ((CHG_CUR_WARMCOEF & 0x3) << 6) | ((ROOM_CHG_CUR & 0xFF) << 8)


static uint32_t max17330_reg_init(void)
{

	static uint16_t max17330_nvm_defaults[][2] = 
	{
		{NICHGTERM_REG, 	0x05D5}, //Full/3 in 15.625 uA for rsense 100 mohm 
		{NRCOMP0_REG, 		0x04B8},
		{NPACKCFG_REG, 		0x6000}, //hibernate during shipmode 0x4000, ship 0x0000, THR2 disable 0x2000
		{NNVCFG1_REG, 		0x2102},
		{NFULLSOCTHR_REG, 0x5005}, //0x5005 : 80%
		{NCHGCFG1_REG, 		0x3FFF},
		{NRSENSE_REG, 		0x2710}, //rsense in 10 uohm
		{NUVPRTTH_REG, 		0x0008},
		{NTPRTTH1_REG, 		0x5000},
		{NPROTMISCTH_REG, 	0x7A5F},
		{NPROTCFG_REG, 		0x3429}, //13:Fullen 1,/10:CmOvrdEn: 1 /5:DeepShipEn 1, /4:OvrdEn 0, /0:DeepShp2En 1
		{NICHGCFG_REG, 		0x8BFF},
		{NVCHGCFG_REG, 		0x2800},
		{NOVPRTTH_REG, 		0xF3F4},
		{NSTEPCHG_REG, 		0xFF00},
		{NDELAYCFG_REG, 	0x1B3C}, //1,0:UVPTimer 0
		{NCHECKSUM_REG, 	0x1065},
		{NCONFIG_REG,     0xA695}, //15:PAen 1,  10:PBen 1
		//{NNVCFG0_REG,     0x0A80},
	};
	
	uint32_t err_code;
	uint16_t val;
	
	err_code = twi_open();
	RETURN_IF_ERROR(err_code);
	
	static char     buf[200];
	
	do
	{
		do
		{
			nrf_delay_ms(10);
			err_code = reg_read(FSTAT_REG, &val);
			BREAK_IF_ERROR(err_code);			
		}while(val & 0x0001);
		
		err_code = reg_read(VERSION_REG, &val);
		BREAK_IF_ERROR(err_code);
		
		NRF_LOG_DEBUG("IC version : 0x%04x", val);
		sprintf(buf, "IC version : 0x%04x \n", val);
		m_usb_com_write_line(buf);
			
		nrf_delay_ms(10);
		
		/* Optional step - alert threshold initialization */
		max17330_set_alert_thresholds();
		
		/* Clear Status.POR */
		err_code = reg_read(STATUS_REG, &val);
		BREAK_IF_ERROR(err_code);
		err_code = reg_write(STATUS_REG, val & ~MAX1720X_STATUS_POR);
		BREAK_IF_ERROR(err_code);

		err_code=reg_read(COMMSTAT_REG,&val);
		BREAK_IF_ERROR(err_code);	
		if(val&0x0300)
			max17330_fet_on();
		
		
		// Unlock Write Protection
		err_code = reg_read(COMMSTAT_REG, &val);
		BREAK_IF_ERROR(err_code);
		
		// Unkock write protection
		i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, val & 0xFF06);
		i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, val & 0xFF06);
		
		// write NVM default
		for (int i=0; i<ARRAY_SIZE(max17330_nvm_defaults); i++)
		{
			i2c_write(m_max17330_cfg.twi_addr_nonvolatile_data, 
			          max17330_nvm_defaults[i][0] & 0xFF, 
			          max17330_nvm_defaults[i][1]);
		}
	
		//max17330_nvmem_show();
		
	i2c_update(m_max17330_cfg.twi_addr_model_gauge, CONFIG2_REG, MAX1720X_CONFIG2_POR_CMD, MAX1720X_CONFIG2_POR_CMD);
		
		// Wait 500ms for POR_CMD to clear
		nrf_delay_ms(500);

		// enable interrupt
		if (m_max17330_cfg.int_pin != 0xff)
		{
			// enable max17330 interrupt
			i2c_update(m_max17330_cfg.twi_addr_model_gauge, 
			           CONFIG_REG, 
			           MAX1720X_CONFIG_ALRT_EN, 
			           MAX1720X_CONFIG_ALRT_EN);
		}
		
		// lock write protection
		i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, val | 0x00F9);
		i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, val | 0x00F9);
		
	} while (0);
	
//	max17330_copy_shwmem_to_nvm();
	max17330_NVM_Remain_Update();

#if 0
		err_code = reg_read(NHIBCFG_REG, &val);
		RETURN_IF_ERROR(err_code);
		NRF_LOG_INFO("Hibernated");
		err_code = reg_write(NHIBCFG_REG, val & ~MAX17330_HIBCFG_ENHIB);
		RETURN_IF_ERROR(err_code);	
	
	  /* Set PBEN */		
	  i2c_update(m_max17330_cfg.twi_addr_model_gauge, 
					 CONFIG_REG, 
					 MAX17330_CONFIG_PBEN | MAX17330_CONFIG_SHIP, 
					 MAX17330_CONFIG_PBEN | MAX17330_CONFIG_SHIP);
#endif	

	twi_close();
	
	return err_code;
}

uint32_t drv_max17330_close(void)
{
	uint32_t err_code;
	
	err_code = twi_open();
	RETURN_IF_ERROR(err_code);
	
	do
	{		
		// enter sleep mode
		//err_code = reg_set(REG_SOFT_RST, 0x01);
		//BREAK_IF_ERROR(err_code);
	} while (0);
	twi_close();
	
	return err_code;
}

uint32_t drv_max17330_get_events(drv_max17330_evt_t* evt)
{
	uint32_t err_code;
	uint16_t val;
	float Bat_volt_mv;
	float Bat_curr_mA;
	static char     buf[200];
	
	NULL_PARAM_CHECK(evt);
	
	memset(evt, 0x00, sizeof(*evt));
	
	err_code = twi_open();
	do
	{
		err_code = reg_read(STATUS_REG, &evt->status);
		BREAK_IF_ERROR(err_code);	
		
		// Check Protection Alert type
		err_code = reg_read(PROTALRTS_REG, &evt->protection_alert);
		BREAK_IF_ERROR(err_code);
		
		// Check charging type
		err_code = reg_read(CHGSTAT_REG, &evt->charging_type);
		BREAK_IF_ERROR(err_code);	
		
		err_code = reg_read(VCELL_REG, &val);
		RETURN_IF_ERROR(err_code);

		Bat_volt_mv = ( val * 625 ) /8;
		
		sprintf(buf, "BAT Volt : %f  [ mv ] \n", Bat_volt_mv/1000);
 		m_usb_com_write_line(buf);
		
	} while (0);
	twi_close();
	
	return err_code;
}

uint32_t drv_max17330_init(drv_max17330_init_t const* p_init)
{
	uint32_t err_code;
	
	NULL_PARAM_CHECK(p_init);
	NULL_PARAM_CHECK(p_init->p_twi_instance);
	NULL_PARAM_CHECK(p_init->p_twi_config);
	
  m_max17330_cfg.p_twi_instance = p_init->p_twi_instance;
  m_max17330_cfg.p_twi_config	 = p_init->p_twi_config;
	m_max17330_cfg.int_pin        = p_init->int_pin;
	m_max17330_cfg.int_handler    = p_init->int_handler;
	m_max17330_cfg.twi_addr_model_gauge       = p_init->addr_model_gauge;
	m_max17330_cfg.twi_addr_nonvolatile_data  = p_init->addr_nonvolatile_data;
	memcpy(&(m_max17330_cfg.config),&(p_init->config),sizeof(drv_max17330_config_t));

	err_code = max17330_reg_init();
	
	// initialize gpiote when interrupt pin is assigned
	if (m_max17330_cfg.int_pin != 0xff)
	{
		if (!nrf_drv_gpiote_is_init())
		{
			err_code = nrf_drv_gpiote_init();
			VERIFY_SUCCESS(err_code);
		}
		
		nrf_drv_gpiote_in_config_t config = NRFX_GPIOTE_CONFIG_IN_SENSE_TOGGLE(false);
		config.pull = GPIO_PIN_CNF_PULL_Disabled;

		err_code = nrf_drv_gpiote_in_init(m_max17330_cfg.int_pin, &config, gpiote_evt_handler);
		VERIFY_SUCCESS(err_code);
		
		nrf_drv_gpiote_in_event_enable(m_max17330_cfg.int_pin, true);
	}	
	
	return NRF_SUCCESS;	
}

static uint32_t max17330_nvmem_show(void)
{
	uint16_t rc = 0, reg = 0;
	uint16_t val = 0;
	uint32_t err_code;
	int i;

	char buf[300]={0,};
	
	for (reg = 0x180; reg < MAX17330_REG_ADDR_NVMEM_HIGH; reg += 16)
	{
	  NRF_LOG_INFO("Page %03Xh: \r\n",(reg) >> 4);
		for (i = 0; i < 16; i++)
		{
			err_code = reg_read(reg + i, &val);
			if(err_code != NRF_SUCCESS)
			{
				NRF_LOG_INFO("NV memory reading failed (%d)\n",	err_code);
				RETURN_IF_ERROR(err_code);
			}
			NRF_LOG_INFO("0x%04X ", val);
			rc++;
		}
		NRF_LOG_INFO("\n");
	}

	return rc;
}

static uint32_t max17330_copy_shwmem_to_nvm(void)
{
  
	uint16_t val = MAX17330_COMMSTAT_NVBUSY;
	uint32_t err_code;
	
	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, 0x0000);
	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, 0x0000);
	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, 0x0000);
	
	NRF_LOG_INFO("Max17330 copy shadow mem to NVM, wait for 10sec");
	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMAND_REG, MAX17330_COMMAND_COPY_NVM); //block copy SDW ram to NVM
	nrf_delay_ms(10000);
	
	while(val &	MAX17330_COMMSTAT_NVBUSY)
	{	
		err_code=reg_read(COMMSTAT_REG, &val); 
		RETURN_IF_ERROR(err_code);
		__NOP();
	}
	NRF_LOG_INFO("NVM copying was completed");
	
	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMAND_REG, MAX17330_COMMAND_FULL_RESET);
	nrf_delay_ms(20);
	
	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, 0x0000);
	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, 0x0000);
	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, 0x0000);
	nrf_delay_ms(20);	

	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, 0x00F9);
	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, 0x00F9);
}

uint16_t max17330_NVM_Remain_Update(void)
{
	uint16_t val=0;
	uint16_t ret=0;
	uint16_t cnt=0;
	
	reg_write(COMMAND_REG, MAX17330_COMMAND_RECALL_HISTORY_REMAINING_WRITES);
	nrf_delay_ms(10);
  reg_read(REMAINING_UPDATES_REG, &val);
	ret = ((val >> 16) & 0x00FF)  | (val & 0x00FF);
	
	for(int id=0;id<8;id++)
	{
		if(((ret >> id) & 0x01) == 1)
			cnt++;
	}
	
	NRF_LOG_INFO("Number of Remaining Updates : %d", 8-cnt);
	nrf_delay_ms(1);
	
	return ret;
	
}

void max17330_ship_enable(void)
{
	uint32_t err_code;
	uint16_t val=0;
	
	err_code = twi_open();	

	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, 0x0000);
	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, 0x0000);
	
#if 1
			  /* SHIP Configuration  */		
//		err_code = reg_write(NHIBCFG_REG,0x0909);
	
		  /* SHIP Configuration  */		
	
//		nProtCfg.DeepShpEn
//		nDelayCfg.UVPTimer
	
	  /* Ship Enable */	
		err_code = i2c_update(m_max17330_cfg.twi_addr_model_gauge, CONFIG_REG,
	               (MAX17330_CONFIG_PBEN | MAX17330_CONFIG_SHIP),(MAX17330_CONFIG_PBEN | MAX17330_CONFIG_SHIP));
#endif		

		i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, 0x00F9);
		i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, 0x00F9);

	err_code = twi_close();
}

void max17330_por_set(void)
{
	uint32_t err_code;
	uint16_t val=0;
	
	err_code = twi_open();
	REPORT_IF_ERROR(err_code);

	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, 0x0000);
	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, 0x0000);
	
	err_code=i2c_update(m_max17330_cfg.twi_addr_model_gauge, CONFIG2_REG, MAX1720X_CONFIG2_POR_CMD, MAX1720X_CONFIG2_POR_CMD);
	REPORT_IF_ERROR(err_code);
	
	nrf_delay_ms(10);
	
	while(1)
	{	
		err_code=reg_read(CONFIG2_REG,&val);
		BREAK_IF_ERROR(err_code);
		if(val & MAX1720X_CONFIG2_POR_CMD)
			break;
	}
	
	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, 0x00F9);
	i2c_write(m_max17330_cfg.twi_addr_model_gauge, COMMSTAT_REG, 0x00F9);
	
	err_code = twi_close();
	REPORT_IF_ERROR(err_code);	
}

void max17330_full_reset(void)
{
	uint32_t err_code;
	uint16_t val=0;
	
	err_code = twi_open();	
	REPORT_IF_ERROR(err_code);
	
	err_code=reg_write(COMMAND_REG, MAX17330_COMMAND_FULL_RESET);
	REPORT_IF_ERROR(err_code);
	
	nrf_delay_ms(10);
	
	while(1)
	{	
		err_code=reg_read(CONFIG2_REG,&val);
		BREAK_IF_ERROR(err_code);
		if(val & MAX1720X_CONFIG2_POR_CMD)
			break;
	}

	err_code = twi_close();
	REPORT_IF_ERROR(err_code);
}

void max17330_fet_off(void)
{
	uint32_t err_code;
	uint16_t val=0;
	
	err_code = twi_open();	
	REPORT_IF_ERROR(err_code);	
	
	err_code = reg_write(COMMSTAT_REG, 0x0200);
	REPORT_IF_ERROR(err_code);		

	//max17330_por_set();
	
	err_code = twi_close();
	REPORT_IF_ERROR(err_code);
}

void max17330_fet_on(void)
{
	uint32_t err_code;
	uint16_t val=0;
	
	err_code = reg_write(COMMSTAT_REG, 0x0000);
	REPORT_IF_ERROR(err_code);		
}	
