#ifndef _drv_max17330_h
#define _drv_max17330_h

// max17330 Driver

#include "nrf_drv_twi.h"
#include "sdk_common.h"
#include "drv_max17330_reg.h"

typedef struct
{
	uint16_t status;
	
	uint16_t protection_alert;
	uint16_t charging_type;
} drv_max17330_evt_t;

typedef void (*drv_max17330_int_handler_t)(drv_max17330_evt_t evt);

typedef struct
{                      // Disable 
	int32_t rsense  ;  // 10      = <5>; 	   Rsense register value in miliOhm
	int32_t temp_min;  // -128    = <0>;       Lower temperature limit(in DegreeC) that generates ALRT1 pin interrupt
	int32_t temp_max;  // 127     = <50>;      Upper temperature limit(in DegreeC) that generates ALRT1 pin interrupt
	int32_t volt_min;  // 0       = <3000>;    Lower voltage limit(in mV) that generates ALRT1 pin interrupt
	int32_t volt_max;  // 5100    = <4500>;    Upper voltage limit(in mV) that generates ALRT1 pin interrupt
	int32_t soc_min ;  // 0       = <1>;       Lower state of charge limit(in percent) that generates ALRT1 pin interrupt
	int32_t soc_max ;  // 255     = <99>;      Upper state of charge limit(in percent) that generates ALRT1 pin interrupt
	int32_t curr_min;  // -5120   = <(-5000)>; Lower current limit(in mA) that generates ALRT1 pin interrupt
	int32_t curr_max;  // 5080    = <5000>;    Upper current limit(in mA) that generates ALRT1 pin interrupt
} drv_max17330_config_t;

#define DEF_MAX17330_CONFIG			\
{									\
	.rsense   = 100		,			\
	.temp_min = 0       ,			\
	.temp_max = 50		,			\
	.volt_min = 3000	,			\
	.volt_max = 4000	,			\
	.soc_min  = 1		,			\
	.soc_max  = 99		,			\
	.curr_min = -5000	,			\
	.curr_max = 5000	,			\
}

#define MAX17330_REG_ADDR_NVMEM_HIGH (0x1EF)
#define MAX17330_REG_PAGE_SIZE       (2*16)

typedef struct
{
    uint8_t 			 addr_model_gauge;				// I2C address
    uint8_t 			 addr_nonvolatile_data;			// I2C address
	uint8_t 			 int_pin;						// interrupt pin
    nrf_drv_twi_t        const * 	p_twi_instance;    	// TWI instance.
    nrf_drv_twi_config_t const * 	p_twi_config;     	// TWI configuraion.
	
	drv_max17330_int_handler_t   int_handler;			// interrupt handler
	drv_max17330_config_t config;
} drv_max17330_init_t;

uint32_t drv_max17330_init(drv_max17330_init_t const* p_init);
uint32_t drv_max17330_get_events(drv_max17330_evt_t* evt);
uint32_t drv_max17330_close(void);
void max17330_ship_enable(void);
void max17330_por_set(void);
void max17330_full_reset(void);
uint16_t max17330_NVM_Remain_Update(void);
void max17330_fet_off(void);
void max17330_fet_on(void);

#endif //_drv_max17330_h
