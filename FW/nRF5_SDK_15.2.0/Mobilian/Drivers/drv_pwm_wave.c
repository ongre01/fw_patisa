#include "drv_pwm_wave.h"
#include "config.h"
#include "nrf_drv_pwm.h"
#include "utility.h"
#include "macros_common.h"

//#define MOTOR_LOG_ENABLED // test

#ifndef MOTOR_LOG_ENABLED
	#define NRF_LOG_LEVEL 0
#endif

#define NRF_LOG_MODULE_NAME PWM_W
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

#define CALC_PWM_FREQ(i)	(16000000 >> (i)) 
#define CH_VAL_CNT			3		// wave 방식에서는 1ch 당 3개의 pwm을 조종할수 있음
#define PWM_VALUES_COUNT	(PWM_DRV_CH_CNT*CH_VAL_CNT)

static struct
{
	bool				initialized;
	nrf_pwm_clk_t		pwm_clk;
	uint32_t			pwm_freq;
	
	pwm_wave_value_t 	default_value[PWM_VALUES_COUNT];
	pwm_wave_value_t 	value[PWM_VALUES_COUNT];
} m_cfg = {
	.initialized = false,
};

typedef struct
{
	uint32_t freq;				
	uint16_t counter_top;		
	uint16_t off, on;			
} drv_pwm_chars_t;

static drv_pwm_chars_t 		dc_chars;

static nrf_drv_pwm_t m_pwm[PWM_DRV_CH_CNT] = {
#if PWM_DRV_CH_CNT > 0	
	NRF_DRV_PWM_INSTANCE(0), 
#endif	
#if PWM_DRV_CH_CNT > 1
	NRF_DRV_PWM_INSTANCE(1),
#endif	
#if PWM_DRV_CH_CNT > 2	
	NRF_DRV_PWM_INSTANCE(2),
#endif	
#ifdef NRF52840_XXAA
#if PWM_DRV_CH_CNT > 3	
	NRF_DRV_PWM_INSTANCE(3),
#endif	
#endif
};

static const uint8_t m_pins[PWM_DRV_CH_CNT][NRF_PWM_CHANNEL_COUNT] = {
#if PWM_DRV_CH_CNT > 0	
	PWM0_PIN,
#endif	
#if PWM_DRV_CH_CNT > 1	
	PWM1_PIN,
#endif	
#if PWM_DRV_CH_CNT > 2	
	PWM2_PIN,
#endif	
#ifdef NRF52840_XXAA
#if PWM_DRV_CH_CNT > 3	
	PWM3_PIN,
#endif	
#endif	
};


static nrf_pwm_values_wave_form_t seq_values[PWM_CHANNEL_COUNT] = {
	{ 0, 0, 0, 0 }, 
};

static uint32_t _pwm_init(uint8_t index, const uint8_t pin[NRF_PWM_CHANNEL_COUNT])
{
    uint32_t err_code;
	
    nrf_drv_pwm_config_t const config =
    {
        .output_pins =
        {
            pin[0], 
			pin[1], 
			pin[2], 
			pin[3],
        },
        .irq_priority = APP_IRQ_PRIORITY_LOW,
        .base_clock   = m_cfg.pwm_clk,
        .count_mode   = NRF_PWM_MODE_UP,
        .top_value    = 0,
        .load_mode    = NRF_PWM_LOAD_WAVE_FORM,
        .step_mode    = NRF_PWM_STEP_AUTO
    };
    err_code = nrfx_pwm_init(&m_pwm[index], &config, NULL);
    RETURN_IF_ERROR(err_code);

	return NRF_SUCCESS;
}


__STATIC_INLINE uint16_t get_pwm_value(pwm_wave_value_t value)
{
	if (dc_chars.freq == 0)
		return dc_chars.off;
	
	return fmap(MIN(1.0f, MAX(0, value)), 0, 1.0f, dc_chars.off, dc_chars.on);
}

__STATIC_INLINE uint16_t set(uint32_t gidx, uint32_t pos, pwm_wave_value_t value)
{
	ASSERT(gidx <PWM_DRV_CH_CNT && pos <NRF_PWM_CHANNEL_COUNT);

	uint16_t* p_value = (uint16_t*)&seq_values[gidx];
	uint16_t  new_value = get_pwm_value(value);
	if (p_value[pos] != new_value)
	{
		p_value[pos] = new_value;
	}
	m_cfg.value[gidx*CH_VAL_CNT + pos] = value;
	
	return new_value;
}


/////////////////////////////////////////////////////////////////////////////
// dc value    : -1 ~ 1
// servo value : -100 ~ 100
void drv_pwm_wave_set(pwm_wave_value_t value[PWM_CHANNEL_COUNT])
{
	uint8_t gidx, pos;
	
	for (int index=0; index<PWM_CHANNEL_COUNT; index++)
	{
		gidx = index / CH_VAL_CNT;
		pos  = index % CH_VAL_CNT;
		
		set(gidx, pos, value[index]);
	}
}

uint16_t drv_pwm_wave_set_index(uint32_t index, pwm_wave_value_t value)
{
	uint8_t gidx = index / CH_VAL_CNT;
	uint8_t pos  = index % CH_VAL_CNT;
	
	ASSERT(gidx <PWM_DRV_CH_CNT && pos <NRF_PWM_CHANNEL_COUNT);

	return set(gidx, pos, value);
}

uint16_t drv_pwm_wave_set_pin(uint32_t pin, pwm_wave_value_t value)
{
	value = MAX(0, MIN(1, value));
	
	for (int gidx=0; gidx<PWM_DRV_CH_CNT; gidx++)
	{
		for (int pos=0; pos<NRF_PWM_CHANNEL_COUNT; pos++)
		{
			if (m_pins[gidx][pos] == pin)
			{
				return set(gidx, pos, value);
			}
		}
	}
	
	return 0;
}

void drv_pwm_wave_set_duty(pwm_wave_value_t value)
{
	for (int i=0; i<PWM_CHANNEL_COUNT; i++)
	{
		drv_pwm_wave_set_index(i, value);
	}
}

void drv_pwm_wave_reset(void)
{
	drv_pwm_wave_set_default();
}


uint32_t drv_pwm_wave_set_frequency(uint32_t freq)
{
	// top value를 구한다
	uint32_t counter_top = INT16_MAX; // UINT16_MAX가 아니네
	if (freq != 0)
		counter_top = (m_cfg.pwm_freq / freq);
	
	if (counter_top == 0 || counter_top > INT16_MAX)
	{
		return NRF_ERROR_INVALID_PARAM;
	}
	
	dc_chars.freq		  = freq;
	dc_chars.counter_top  = (uint16_t)counter_top;
	dc_chars.off          = counter_top;
	dc_chars.on           = 0;//counter_top;
		
	// recalculate channel value
	for (uint8_t i=0; i<PWM_DRV_CH_CNT; i++)
	{		
		seq_values[i].channel_0   = get_pwm_value(m_cfg.value[i*CH_VAL_CNT+0]);
		seq_values[i].channel_1   = get_pwm_value(m_cfg.value[i*CH_VAL_CNT+1]);
		seq_values[i].channel_2   = get_pwm_value(m_cfg.value[i*CH_VAL_CNT+2]);
		seq_values[i].counter_top = counter_top;
		
		trace("%d / %d", seq_values[i].channel_0 , counter_top);
	}
	
	return NRF_SUCCESS;
}

void drv_pwm_wave_set_default_value(pwm_wave_value_t value[PWM_CHANNEL_COUNT])
{
	memcpy(m_cfg.default_value, value, sizeof(m_cfg.default_value));
}

void drv_pwm_wave_set_default(void)
{
	drv_pwm_wave_set(m_cfg.default_value);
}


uint32_t drv_pwm_wave_get_frequency(void)
{
	return dc_chars.freq;
}


uint32_t drv_pwm_wave_start(void)
{
	uint32_t err_code;
	
	for (int i=0; i<PWM_DRV_CH_CNT; i++)
	{
		nrf_pwm_sequence_t const seq =
		{
			.values.p_wave_form   = &seq_values[i],
			.length               = sizeof(seq_values[i]) / sizeof(uint16_t),
			.repeats              = 0,
			.end_delay            = 0
		};
	
		err_code = nrfx_pwm_simple_playback(&m_pwm[i], &seq, 1, NRFX_PWM_FLAG_LOOP);
    	RETURN_IF_ERROR(err_code);
	}
	
	return NRF_SUCCESS;
}

void drv_pwm_wave_stop(bool wait_until_stop)
{
	uint32_t err_code;
	
	drv_pwm_wave_reset();
	
	for (int i=0; i<PWM_DRV_CH_CNT; i++)
	{
		err_code = nrfx_pwm_stop(&m_pwm[i], wait_until_stop);
    	REPORT_IF_ERROR(err_code);
	}
}

uint32_t drv_pwm_wave_init(nrf_pwm_clk_t pwm_clk)
{
	uint32_t err_code;
	
	if (m_cfg.initialized)
	{
		return NRF_ERROR_MODULE_ALREADY_INITIALIZED;
	}
	
	m_cfg.pwm_clk  = pwm_clk;
	m_cfg.pwm_freq = CALC_PWM_FREQ(pwm_clk);
	
	// set default frequency to 0
	drv_pwm_wave_set_frequency(0);

	for (int i=0; i<PWM_DRV_CH_CNT; i++)
	{
		err_code = _pwm_init(i, m_pins[i]);
		RETURN_IF_ERROR(err_code);
	}
	
	m_cfg.initialized = true;
	
	return NRF_SUCCESS;
}

void drv_pwm_wave_shutdown(void)
{
	if (m_cfg.initialized)
	{
		for (int i=0; i<PWM_DRV_CH_CNT; i++)
		{
			nrf_drv_pwm_uninit(&m_pwm[i]);
		}
		
		m_cfg.initialized = false;
	}
}

