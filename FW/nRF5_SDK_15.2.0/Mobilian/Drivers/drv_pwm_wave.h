#ifndef _drv_pwm_wave
#define _drv_pwm_wave

#include <sdk_common.h>
#include "config.h"
#include "nrf_drv_pwm.h"

#ifdef __cplusplus
extern "C" {
#endif
	
typedef float pwm_wave_value_t;

uint32_t drv_pwm_wave_init(nrf_pwm_clk_t pwm_clk);
void drv_pwm_wave_shutdown(void);

uint32_t drv_pwm_wave_start(void);
void drv_pwm_wave_stop(bool wait_until_stop);
	
uint32_t drv_pwm_wave_get_frequency(void);
uint32_t drv_pwm_wave_set_frequency(uint32_t freq);

void     drv_pwm_wave_set_duty(pwm_wave_value_t value);

void drv_pwm_wave_set_default_value(pwm_wave_value_t value[PWM_CHANNEL_COUNT]);
void drv_pwm_wave_set_default(void);
	
void 	 drv_pwm_wave_set(pwm_wave_value_t value[PWM_CHANNEL_COUNT]);
uint16_t drv_pwm_wave_set_index(uint32_t index, pwm_wave_value_t value);
uint16_t drv_pwm_wave_set_pin(uint32_t pin, pwm_wave_value_t value);
	
#ifdef __cplusplus
}
#endif

#endif // _drv_pwm_wave
