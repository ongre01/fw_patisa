#ifndef _ble_beacon_info_h
#define _ble_beacon_info_h

#include <sdk_common.h>
#include "app_util_platform.h"

#define BLE_BEACON_INFO_LENGTH          0x17 	
#define BLE_BEACON_ADV_DATA_LENGTH      0x15 	
#define BLE_BEACON_UUID_LENGTH			0x10	

#define BLE_BEACON_DEVICE_TYPE			0x02	

#define BLE_BEACON_COMPANY_NORDIC		0x0059	
#define BLE_BEACON_COMPANY_APPLE		0x004c	


#define DEF_BLE_BEACON_COMPANY_IDENTIFIER	BLE_BEACON_COMPANY_NORDIC

#define DEF_BLE_BEACON_MAJOR_VALUE		0x0000
#define DEF_BLE_BEACON_MINOR_VALUE		0x0000
										 
#define DEF_BLE_BEACON_MEASURED_RSSI	0xc3
#define DEF_BLE_BEACON_UUID {0x01, 0x02, 0x03, 0x04, }
#define DEF_BEACON_INFO {									\
		.device_type	 = BLE_BEACON_DEVICE_TYPE,			\
		.adv_data_length = BLE_BEACON_ADV_DATA_LENGTH,		\
		.adv_uuid 		 = DEF_BLE_BEACON_UUID,		\
		.major			 = DEF_BLE_BEACON_MAJOR_VALUE,		\
		.minor			 = DEF_BLE_BEACON_MINOR_VALUE,		\
		.measured_rssi	 = DEF_BLE_BEACON_MEASURED_RSSI,		\
}										 
										 
#pragma pack(1)

ANON_UNIONS_ENABLE;										 
typedef	union
{
	uint8_t		buffer[BLE_BEACON_INFO_LENGTH];
	struct
	{
		uint8_t 	device_type;						
		uint8_t 	adv_data_length;					
		uint8_t 	adv_uuid[BLE_BEACON_UUID_LENGTH];	
		uint16_t 	major, minor;						
		int8_t  	measured_rssi;						
	};
} ble_beacon_info_t;
ANON_UNIONS_DISABLE;									
#pragma pack()

#pragma pack(1)
typedef struct 
{
	uint16_t			company_identifier;
	ble_beacon_info_t	bi;
} ble_beacon_manuf_data_t;

#pragma pack()

#endif // _ble_beacon_info_h
