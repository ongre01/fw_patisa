#include "helper.h"
#include "sdk_macros.h"
#include "nordic_common.h"
#include "nrf_error.h"

#include <ctype.h>

uint32_t app_fifo_get_available(app_fifo_t * p_fifo)
{
    return p_fifo->buf_size_mask - app_fifo_get_length(p_fifo);
}

uint32_t app_fifo_get_length(app_fifo_t * p_fifo)
{
    uint32_t tmp = p_fifo->read_pos;
    return p_fifo->write_pos - tmp;
}

uint32_t app_fifo_skip(app_fifo_t * p_fifo, uint32_t size)
{
	if (size > app_fifo_get_length(p_fifo))
		return NRF_ERROR_INVALID_LENGTH;
	
	p_fifo->read_pos += size;
	
	return NRF_SUCCESS;
}

uint32_t app_fifo_peek_read(app_fifo_t * p_fifo, uint8_t * p_byte_array, uint32_t * p_size)
{
    VERIFY_PARAM_NOT_NULL(p_fifo);
    VERIFY_PARAM_NOT_NULL(p_size);

    const uint32_t byte_count    = app_fifo_get_length(p_fifo);
    const uint32_t requested_len = (*p_size);
    uint32_t       index         = 0;
    uint32_t       read_size     = MIN(requested_len, byte_count);

    (*p_size) = byte_count;

    // Check if the FIFO is empty.
    if (byte_count == 0)
    {
        return NRF_ERROR_NOT_FOUND;
    }

    // Check if application has requested only the size.
    if (p_byte_array == NULL)
    {
        return NRF_SUCCESS;
    }

    // Fetch bytes from the FIFO.
    while (index < read_size)
    {
		app_fifo_peek(p_fifo, index, &p_byte_array[index]);
		index++;
    }

    (*p_size) = read_size;

    return NRF_SUCCESS;
}

int32_t atoin(uint8_t* data, uint16_t len, uint8_t** next)
{
	char 	 buffer[20];
	uint16_t index = 0;
	int8_t   mul = 1;
	
	// move to first white char
	for (int i=0; i<len; i++)
	{
		if (*data != ' ') 
			break;
		data++;
		len--;
	}
	
	if (len == 0)
	{
		if (next != NULL) *next = NULL;
		return 0;
	}
	
	if (*data == '+') 
	{
		data++; len--;
	}
	else if (*data == '-') 
	{
		mul = -1;
		data++; len--;
	}
	
	
	for (int i=0; i<len && index < sizeof(buffer)-1; i++, data++)
	{
		if (!isdigit(*data)) 
			break;
		buffer[index++] = *data;
	}
	
	buffer[index] = NULL;
	
	if (next != NULL)
		*next = data;
	
	return atoi(buffer) * mul;
}

int32_t atoxn(uint8_t* data, uint16_t len, uint8_t** next)
{
	char 	 buffer[20];
	uint16_t index = 0;
	
	// move to first char skipping white char
	for (int i=0; i<len; i++)
	{
		if (*data != ' ') 
			break;
		data++;
		len--;
	}
	
	if (len == 0)
	{
		if (next != NULL) *next = NULL;
		return 0;
	}
	
	for (int i=0; i<len && index < sizeof(buffer)-1; i++, data++)
	{
		if (!is_hex_char(*data)) 
			break;
		buffer[index++] = *data;
	}
	
	buffer[index] = NULL;
	
	if (next != NULL)
		*next = data;
	
	return strtol(buffer, NULL, 16);
}

float atoif(uint8_t* data, uint16_t len, uint8_t** next)
{
	char 	 buffer[20];
	uint16_t index = 0;
	int8_t   mul = 1;
	
	// move to first digit erasing white char
	for (int i=0; i<len; i++)
	{
		if (*data != ' ') 
			break;
		data++;
		len--;
	}
	
	if (len == 0)
	{
		if (next != NULL) *next = NULL;
		return 0;
	}
	
	if (*data == '+') 
	{
		data++; len--;
	}
	else if (*data == '-') 
	{
		mul = -1;
		data++; len--;
	}
	
	
	for (int i=0; i<len && index < sizeof(buffer)-1; i++, data++)
	{
		if (!isdigit(*data) && *data != '.') 
			break;
		buffer[index++] = *data;
	}
	
	buffer[index] = NULL;
	
	if (next != NULL)
		*next = data;
	
	return atof(buffer) * mul;
}


uint16_t copy_buf(uint8_t* dst, uint16_t dest_len, const uint8_t* src, uint16_t src_len)
{
	uint16_t len = MIN(dest_len, src_len);
	
	memcpy(dst, src, len);
	
	return len;
}

char *strnchr(const char *s, int count, int c, int* idx)
{
	if (idx != NULL) *idx=0;
	for (; count-- && *s != '\0'; ++s)
	{
		if (*s == (char)c)
			return (char *)s;
		if (idx != NULL) (*idx)++;
	}
	return NULL;
}

bool starts_with(const char*s, int count, const char* target)
{
	int min_len = strlen(target);
	if (count < min_len)
		return false;
	
	return memcmp(s, target, min_len) == 0;
}

bool starts_with_ignore_case(const char*s, int count, const char* target)
{
	int min_len = strlen(target);
	if (count < min_len)
		return false;
	
	for (int i=0; i<min_len; i++)
	{
		if (toupper(*s++) != toupper(*target++))
			return false;
	}
	
	return true;
}

int strlenWithSize(const char* buf, int max_size)
{
	int len = 0;
	for (int i=0; i<max_size; i++)
	{
		if (*buf++ == 0)
			break;
		len++;
	}
	
	return len;
}

bool string_include(const char* src, int src_len, const char* target)
{
	int count = src_len - strlen(target) + 1;
	for (int i=0; i<count; i++)
	{
		if (starts_with(src+i, src_len-i, target))
			return true;
	}
	return false;
}

bool data_include(const uint8_t* src, int src_len, const uint8_t* target, uint8_t target_len)
{
	int count = src_len - target_len + 1;
	for (int i=0; i<count; i++)
	{
		if (memcmp(src+i, target, target_len) == 0)
			return true;
	}
	return false;
}

bool parse_mac(uint8_t* p_data, int* len, uint8_t** next, uint8_t addr[BLE_GAP_ADDR_LEN])
{
	if (p_data == NULL || len == NULL)
		return false;
	
	int mac_len = (2*BLE_GAP_ADDR_LEN + (BLE_GAP_ADDR_LEN-1));
	if (*len < mac_len)
	{
			return false;
	}

	char buf[3] = {0x00, };

	for (int i=0; i<BLE_GAP_ADDR_LEN; i++, p_data += 3)
	{
			memcpy(buf, p_data, 2);
			if (!is_hex_char(buf[0]) ||
					!is_hex_char(buf[1]) ||
					(i!=5 && p_data[2]!=':'))
			{
					return false;
			}

			addr[BLE_GAP_ADDR_LEN-i-1] = strtol(buf, NULL, 16);
	}
	
	if (next != NULL)
		*next += mac_len;
	*len  -= mac_len;
	
	return true;
}

