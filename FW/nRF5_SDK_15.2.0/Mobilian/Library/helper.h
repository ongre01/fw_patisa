#ifndef _helper_h
#define _helper_h

#ifdef __cplusplus
extern "C" {
#endif

#include "sdk_common.h"
#include "ctype.h"
#include "app_fifo.h"
#include "ble_gap.h"

#define ABS(n) ((n)<0?(-n):(n))

uint32_t app_fifo_get_available(app_fifo_t * p_fifo);
uint32_t app_fifo_get_length(app_fifo_t * p_fifo);
uint32_t app_fifo_peek_read(app_fifo_t * p_fifo, uint8_t * p_byte_array, uint32_t * p_size);
uint32_t app_fifo_skip(app_fifo_t * p_fifo, uint32_t size);

int32_t  atoin(uint8_t* data, uint16_t len, uint8_t** next);
float    atoif(uint8_t* data, uint16_t len, uint8_t** next);
int32_t  atoxn(uint8_t* data, uint16_t len, uint8_t** next);

uint16_t copy_buf(uint8_t* dst, uint16_t dest_len, const uint8_t* src, uint16_t src_len);
char *strnchr(const char *s, int count, int c, int* idx);
bool starts_with(const char*s, int count, const char* target);
bool starts_with_ignore_case(const char*s, int count, const char* target);
int strlenWithSize(const char* buf, int max_size);
bool string_include(const char* src, int src_len, const char* target);
bool data_include(const uint8_t* src, int src_len, const uint8_t* target, uint8_t target_len);

__STATIC_INLINE int is_hex_char(char c) { return (('0'<=(c) && (c)<='9') || ('A'<=toupper(c) && toupper(c)<='Z')); }
__STATIC_INLINE uint8_t to_hex(uint8_t c) { return ('0'<=(c) && (c)<='9') ? ((c)-'0') : (('A'<=toupper(c) && toupper(c)<='Z')?(toupper(c)-'A'+10):0); }
	
uint32_t safe_strlen(const char* p_str);
void trim_spaces(char * p_char);
char parse_argv(size_t * p_argc, char ** pp_argv, char * p_cmd, uint8_t max_argc);
bool parse_mac(uint8_t* p_data, int* len, uint8_t** next, uint8_t addr[6]);
	
#ifdef __cplusplus
}
#endif
	
#endif // _helper_h
