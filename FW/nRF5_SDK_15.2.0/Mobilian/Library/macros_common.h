#ifndef _macros_common_h
#define _macros_common_h

#define BREAK_IF_ERROR(err_code) \
if ((err_code) != NRF_SUCCESS) \
{ \
	NRF_LOG_ERROR(__FILE__); \
    NRF_LOG_ERROR(" - ERROR. line: %d, with error code %d \r\n", __LINE__, err_code); \
    break; \
}

#define RETURN_IF_ERROR(err_code) \
if ((err_code) != NRF_SUCCESS) \
{ \
	NRF_LOG_ERROR(__FILE__); \
    NRF_LOG_ERROR(" - ERROR. line: %d, with error code %d \r\n", __LINE__, err_code); \
    return (err_code); \
}

#define REPORT_IF_ERROR(err_code) \
if ((err_code) != NRF_SUCCESS) \
{ \
	NRF_LOG_ERROR(__FILE__); \
    NRF_LOG_ERROR(" - ERROR. line: %d, with error code %d \r\n", __LINE__, err_code); \
}

#define NULL_PARAM_CHECK(param)                                                                    \
        if ((param) == NULL)                                                                       \
        {                                                                                          \
			NRF_LOG_ERROR(__FILE__); \
			NRF_LOG_ERROR(" - NULL. line: %d\r\n", __LINE__); \
            return NRF_ERROR_NULL;                                                                 \
        }

#define ALERT_IF_ERROR(err_code) \
if ((err_code) != NRF_SUCCESS) \
{ \
	NRF_LOG_ERROR(__FILE__); \
    NRF_LOG_ERROR(" - ALERT. line: %d, with error code %d \r\n", __LINE__, err_code); \
	bsp_indication_set(BSP_INDICATE_ALERT);		\
}
#endif 
