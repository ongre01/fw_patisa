#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "app_timer.h"
#include "app_scheduler.h"
#include "rtc_timer.h"
#include "macros_common.h"
#include <time.h>

#define NRF_LOG_MODULE_NAME RTC_TIMER
#include "nrf_log.h"

#ifndef CALENDAR_UPDATE_TIMEOUT
#define CALENDAR_UPDATE_TIMEOUT 1000
#endif

NRF_LOG_MODULE_REGISTER();

static uint64_t m_millis = 0;

///////////////////////////////////////////////////////////////////////////////////
// Timer
void initTimer(timer_t* timer)
{
	*timer = app_timer_cnt_get();
}

bool isTimerAlive(timer_t* timer, uint32_t tolerance)
{
	return !isTimeout(timer, tolerance);
}

float elapseTime(timer_t* timer)
{
	uint32_t cur, diff;
	
	cur  = app_timer_cnt_get();
	diff = app_timer_cnt_diff_compute(cur, *timer);	
	
	return TICK2MS(diff);
}

bool isTimeout(timer_t* timer, uint32_t tolerance)
{
	return elapseTime(timer) >= tolerance;
}

void waitFor(uint32_t tolerance)
{
	timer_t timer;
	initTimer(&timer);
	while (!isTimeout(&timer, tolerance))
	{
#if NRF_MODULE_ENABLED(APP_SCHEDULER)
        app_sched_execute();
#endif		
	}
}

uint32_t compensate_sec(timer_t* timer)
{
	uint32_t cur  = app_timer_cnt_get();
	uint32_t diff = app_timer_cnt_diff_compute(cur, *timer);	
	
	uint32_t sec = diff / APP_TIMER_CLOCK_FREQ;
	if (sec > 0)
	{
		// calculate remainder and compensate
		uint32_t span = diff - sec * APP_TIMER_CLOCK_FREQ;
		*timer = cur - span;
	}

	return sec;
}

///////////////////////////////////////////////////////////////////////////////////
// calendar
static calendar_t curDateTime 			= {2000, 1, 1, 0, 0, 0}; // current date time
static timer_t    lastTickForCalendar	= 0;
static uint8_t	  days[12] 				= { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
static bool		  calendar_started = false;

static calendar_changed_handler_t m_cal_change_handler;
static calendar_changed_handler_t m_cal_min_change_handler;
static uint8_t 					  m_last_min;

APP_TIMER_DEF(m_timer_calendar_update);

static uint8_t getDaysOfFebruary(uint32_t year)
{
	if(((year % 400) == 0) || (((year % 100) != 0) && ((year % 4) == 0)))
	{
		return 29;
	}
	
	return 28;
}

static void add_seconds(calendar_t* date, uint32_t seconds)
{
	date->sec += seconds;
	
	if (date->sec >= 60)
	{
		date->min += (date->sec / 60);
		date->sec %= 60;
		if (date->min >= 60)
		{
			date->hour += (date->min / 60);
			date->min %= 60;
			if (date->hour >= 24)
			{
				date->day += (date->hour / 24);
				date->hour %= 24;
				
				if (date->day > cal_get_days(date->year, date->month))
				{
					date->month++;
					date->day = 1;
					if (date->month > 12)
					{
						date->year++;
						date->month = 1;
					}
				}
			}
		}
	}	
}

uint64_t get_millis(void)
{
	uint32_t cur, diff;
	uint32_t diff_ms;
	
	cur  = app_timer_cnt_get();
	diff = app_timer_cnt_diff_compute(cur, lastTickForCalendar);	
	
	diff_ms = (diff * 1000) / APP_TIMER_CLOCK_FREQ;

	return m_millis + diff_ms;
}

static void calendar_update_handler(void* p_context)
{
	uint32_t cur, diff;
	
	cur  = app_timer_cnt_get();
	diff = app_timer_cnt_diff_compute(cur, lastTickForCalendar);	
	
	uint32_t sec = diff / APP_TIMER_CLOCK_FREQ;
	if (sec < 1) return;

	// update millis
	m_millis += (sec * 1000);
	
	// update second
	calendar_t tmp = curDateTime;
	add_seconds(&tmp, sec);

	// calculate remainder and compensate
	uint32_t span       = diff - sec * APP_TIMER_CLOCK_FREQ;
	lastTickForCalendar = cur - span;
	
	curDateTime = tmp;
	
	// send calendar change to callback
	if (m_cal_change_handler)
		m_cal_change_handler(curDateTime);
	if (m_cal_min_change_handler && m_last_min != curDateTime.min)
	{
		m_last_min = curDateTime.min;
		m_cal_min_change_handler(curDateTime);
	}
}

uint16_t cal_get_days(uint32_t year, uint8_t month)
{
	if (month == 2)
	{
		days[1] = getDaysOfFebruary(year);
	}

	return days[month - 1];
}

bool cal_is_valid(const calendar_t* const cal)
{
	if (1<=cal->month && cal->month<=12)
	{
		if (1<=cal->day && cal->day<=cal_get_days(cal->year, cal->month))
		{
			if (0<=cal->hour && cal->hour<=23)
			{
				if (0<=cal->min && cal->min<=59)
				{
					if (0<=cal->sec && cal->sec<=59)
						return true;
				}
			}
		}
	}
	return false;
}

calendar_t cal_get_cur_datetime(void)
{
	return curDateTime;
}

calendar_t 	cal_get_cur_datetime_ms(uint32_t* ms)
{
	uint32_t cur, diff;
	
	cur  = app_timer_cnt_get();
	diff = app_timer_cnt_diff_compute(cur, lastTickForCalendar);	
	
	uint32_t diff_ms = (diff*1000) / APP_TIMER_CLOCK_FREQ;
	if (ms != NULL)
		*ms = diff_ms;
	
	return curDateTime;
}

void cal_set_cur_datetime(calendar_t cal)
{
	curDateTime = cal;
	m_last_min  = cal.min;
	initTimer(&lastTickForCalendar);
	
	if (m_cal_change_handler)
		m_cal_change_handler(curDateTime);
}

int cal_compare(const calendar_t* c1, const calendar_t* c2)
{
	int n;
	
	n = c1->year - c2->year;
	if (n != 0)
		return n;
	
	n = c1->month - c2->month;
	if (n != 0)
		return n;
	
	n = c1->day - c2->day;
	if (n != 0)
		return n;
	
	n = c1->hour - c2->hour;
	if (n != 0)
		return n;
	
	n = c1->min - c2->min;
	if (n != 0)
		return n;
	
	n = c1->sec - c2->sec;
	return n;
}

uint8_t cal_get_day_of_week(calendar_t cal)
{
	static const int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
	
    cal.year -= cal.month < 3;
    return (cal.year + cal.year/4 - cal.year/100 + cal.year/400 + t[cal.month-1] + cal.day) % 7;
}

timestamp_t cal_get_timestamp(const calendar_t* cal)
{
	struct tm t;
	memset(&t, 0x00, sizeof(t));
	
	t.tm_year   = cal->year  - 1900;
	t.tm_mon    = cal->month - 1;
	t.tm_mday   = cal->day;
	t.tm_hour   = cal->hour;
	t.tm_min    = cal->min;
	t.tm_sec    = cal->sec;
	
	t.tm_isdst  = -1;

	return (timestamp_t)mktime(&t);
}

calendar_t cal_convert(timestamp_t timestamp)
{
	struct tm* t;
	calendar_t cal;
	
	t = localtime(&timestamp);
	
	cal.year  = t->tm_year + 1900;
	cal.month = t->tm_mon  + 1;
	cal.day	  = t->tm_mday;
	cal.hour  = t->tm_hour;
	cal.min	  = t->tm_min ;
	cal.sec	  = t->tm_sec ;
	
	return cal;
}

const char* cal_get_str_hms(char buf[9], timestamp_t timestamp)
{
	calendar_t cal = cal_convert(timestamp);
	sprintf(buf, "%02d:%02d:%02d", cal.hour, cal.min, cal.sec);
	return buf;
}
const char* cal_get_str(char buf[20], timestamp_t timestamp)
{
	calendar_t cal = cal_convert(timestamp);
	sprintf(buf, "%d-%d-%d %02d:%02d:%02d", 
		cal.year, cal.month, cal.day,
		cal.hour, cal.min, cal.sec);
	return buf;
}

timestamp_t cal_get_cur_timestamp(void)
{
	calendar_t cal = cal_get_cur_datetime();
	return cal_get_timestamp(&cal);
}

timestamp_t cal_get_cur_timestamp_ms(uint32_t* ms)
{
	if (ms != NULL)
	{
		uint32_t cur, diff;
		
		cur  = app_timer_cnt_get();
		diff = app_timer_cnt_diff_compute(cur, lastTickForCalendar);	
		
		*ms = (diff*1000) / APP_TIMER_CLOCK_FREQ;
	}
	
	return cal_get_cur_timestamp();
}


int32_t	cal_get_diff(uint32_t timestamp)
{
	return cal_get_cur_timestamp() - timestamp;
}

uint32_t cal_start(const calendar_init_t* p_init)
{
	uint32_t err_code;

	// check if calendar is already started
	if (calendar_started)
		return NRF_SUCCESS;
	
	if (p_init != NULL)
	{
		// start with specified datetime
		curDateTime = p_init->cur;
		m_last_min  = p_init->cur.min;
		
		// save callback for datetime changes
		m_cal_change_handler 	 = p_init->cal_change_handler;
		m_cal_min_change_handler = p_init->cal_min_change_handler;
		
		// send calendar change to callback
		if (m_cal_change_handler)
			m_cal_change_handler(curDateTime);		
	}
	
	err_code = app_timer_create(&m_timer_calendar_update, APP_TIMER_MODE_REPEATED, calendar_update_handler);
	APP_ERROR_CHECK(err_code);
	
	initTimer(&lastTickForCalendar);
	
	err_code = app_timer_start(m_timer_calendar_update, CALENDAR_UPDATE_TIMEOUT, NULL);
	APP_ERROR_CHECK(err_code);
	
	calendar_started = true;	
	
	return NRF_SUCCESS;
}

