#include <stdint.h>
#include <string.h>
#include "app_timer.h"
#include "app_scheduler.h"
#include "rtc_timer.h"
#include "macros_common.h"
#include <time.h>

#ifndef CALENDAR_UPDATE_TIMEOUT
#define CALENDAR_UPDATE_TIMEOUT 1000
#endif

static const uint32_t SEC_PER_YEAR  = 365*24*60*60;
static const uint32_t SEC_PER_DAY   = 24*60*60;
static const uint32_t SEC_PER_HOUR  = 60*60;
static const uint32_t SEC_PER_MIN   = 60;
static const uint32_t EPOCH_YEAR    = 1970;

static uint64_t m_millis = 0;

///////////////////////////////////////////////////////////////////////////////////
// Timer
void initTimer(timer_t* timer)
{
	*timer = app_timer_cnt_get();
}

bool isTimerAlive(timer_t* timer, uint32_t tolerance)
{
	return !isTimeout(timer, tolerance);
}

float elapseTime(timer_t* timer)
{
	uint32_t cur, diff;
	
	cur  = app_timer_cnt_get();
	diff = app_timer_cnt_diff_compute(cur, *timer);	
	
	return TICK2MS(diff);
}

bool isTimeout(timer_t* timer, uint32_t tolerance)
{
	return elapseTime(timer) >= tolerance;
}

uint32_t compensate_sec(timer_t* timer)
{
	uint32_t cur  = app_timer_cnt_get();
	uint32_t diff = app_timer_cnt_diff_compute(cur, *timer);	
	
	uint32_t sec = diff;
	if (sec > 0)
	{
		// calculate remainder and compensate
		uint32_t span = diff - sec * APP_TIMER_CLOCK_FREQ;
		*timer = cur - span;
	}

	return sec;
}

///////////////////////////////////////////////////////////////////////////////////
// calendar
static calendar_t curDateTime 			= {EPOCH_YEAR, 1, 1, 0, 0, 0}; // current date time
static timer_t    lastTickForCalendar	= 0;
static uint8_t	  days[12] 				= { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
static bool		  calendar_started = false;

static calendar_changed_handler_t m_cal_change_handler;
static calendar_changed_handler_t m_cal_min_change_handler;
static uint8_t 					  m_last_min;

APP_TIMER_DEF(m_timer_calendar_update);

static __INLINE bool is_leap_year(uint32_t year)
{
	if(((year % 400) == 0) || (((year % 100) != 0) && ((year % 4) == 0)))
	{
		return true;
	}
	
	return false;
}

static __INLINE uint8_t getDaysOfFebruary(uint32_t year)
{
	return is_leap_year(year) ? 29 : 28;
}

static void add_seconds(calendar_t* date, uint32_t seconds)
{
	date->sec += seconds;
	
	if (date->sec >= 60)
	{
		date->min += (date->sec / 60);
		date->sec %= 60;
		if (date->min >= 60)
		{
			date->hour += (date->min / 60);
			date->min %= 60;
			if (date->hour >= 24)
			{
				date->day += (date->hour / 24);
				date->hour %= 24;
				
				if (date->day > cal_get_days(date->year, date->month))
				{
					date->month++;
					date->day = 1;
					if (date->month > 12)
					{
						date->year++;
						date->month = 1;
					}
				}
			}
		}
	}	
}

uint64_t get_millis(void)
{
	uint32_t cur, diff;
	uint32_t diff_ms;
	
	cur  = app_timer_cnt_get();
	diff = app_timer_cnt_diff_compute(cur, lastTickForCalendar);	
	
	diff_ms = (diff * 1000) / APP_TIMER_CLOCK_FREQ;

	return m_millis + diff_ms;
}

void set_millis(uint64_t millis)
{
	lastTickForCalendar = app_timer_cnt_get();
	m_millis = millis;
}

int64_t get_millis_dif(uint64_t timestamp)
{
	return get_millis() - timestamp;
}

static void calendar_update_handler(void* p_context)
{
	uint32_t cur, diff;
	
	cur  = app_timer_cnt_get();
	diff = app_timer_cnt_diff_compute(cur, lastTickForCalendar);	
	
	uint32_t sec = diff / APP_TIMER_CLOCK_FREQ;
	if (sec < 1) return;

	// update millis
	m_millis += (sec * 1000);
	
	// update second
	calendar_t tmp = curDateTime;
	add_seconds(&tmp, sec);

	// calculate remainder and compensate
	uint32_t span       = diff - sec * APP_TIMER_CLOCK_FREQ;
	lastTickForCalendar = cur - span;
	
	curDateTime = tmp;
	
	// send calendar change to callback
	if (m_cal_change_handler)
		m_cal_change_handler(curDateTime);
	if (m_cal_min_change_handler && m_last_min != curDateTime.min)
	{
		m_last_min = curDateTime.min;
		m_cal_min_change_handler(curDateTime);
	}
}

uint16_t cal_get_days(uint32_t year, uint8_t month)
{
	if (month == 2)
	{
		days[1] = getDaysOfFebruary(year);
	}

	return days[month - 1];
}

bool cal_is_valid(const calendar_t* const cal)
{
	if (1<=cal->month && cal->month<=12)
	{
		if (1<=cal->day && cal->day<=cal_get_days(cal->year, cal->month))
		{
			if (0<=cal->hour && cal->hour<=23)
			{
				if (0<=cal->min && cal->min<=59)
				{
					if (0<=cal->sec && cal->sec<=59)
						return true;
				}
			}
		}
	}
	return false;
}

calendar_t cal_get_cur_datetime(void)
{
	return curDateTime;
}

calendar_t 	cal_get_cur_datetime_ms(uint32_t* ms)
{
	uint32_t cur, diff;
	
	cur  = app_timer_cnt_get();
	diff = app_timer_cnt_diff_compute(cur, lastTickForCalendar);	
	
	uint32_t diff_ms = (diff*1000) / APP_TIMER_CLOCK_FREQ;
	if (ms != NULL)
		*ms = diff_ms;
	
	return curDateTime;
}

void cal_set_cur_datetime(calendar_t cal)
{
	curDateTime = cal;
	m_last_min  = cal.min;
	initTimer(&lastTickForCalendar);

	if (m_cal_change_handler)
		m_cal_change_handler(curDateTime);
}

int cal_compare(const calendar_t* c1, const calendar_t* c2)
{
	timestamp_t t1 = cal_get_timestamp(c1);
	timestamp_t t2 = cal_get_timestamp(c2);
	
	if (t1 == t2) return 0;
	
	return (((t1-t2) > 0) ? 1 : -1);
}

uint8_t cal_get_day_of_week(calendar_t cal)
{
	static const int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
	
    cal.year -= cal.month < 3;
    return (cal.year + cal.year/4 - cal.year/100 + cal.year/400 + t[cal.month-1] + cal.day) % 7;
}

timestamp_t cal_get_timestamp(const calendar_t* cal)
{
	timestamp_t t = 0;
	
	// year
	for (int i=EPOCH_YEAR; i<cal->year; i++)
		t += cal_get_total_sec_for_year(i);

	// month
	for (int i=1; i<cal->month; i++)
		t += cal_get_total_sec_for_month(cal->year, i);
	
	// day
	t += ((cal->day - 1) * SEC_PER_DAY);

	// hour
	t += ((cal->hour) * SEC_PER_HOUR);
	
	// min
	t += (cal->min * SEC_PER_MIN);
	
	// sec
	t += cal->sec;

	return t * 1000;
}


calendar_t cal_convert(timestamp_t timestamp)
{
	calendar_t cal = {
		.year  = EPOCH_YEAR,
		.month = 1,
		.day   = 1,
		.hour  = 0,
		.min   = 0,
		.sec   = 0,
	};
	uint64_t t;

	// year
	while(true)
	{
		t = cal_get_total_sec_for_year(cal.year) * 1000;
		if (t > timestamp)
			break;
		timestamp -= t;
		cal.year++;
	}
	
	// month
	while(true)
	{
		t = cal_get_total_sec_for_month(cal.year, cal.month) * 1000;
		if (t > timestamp)
			break;
		timestamp -= t;
		cal.month++;
	}
	
	// day
	t = (timestamp / (SEC_PER_DAY*1000));
	cal.day += t;
	timestamp -= (t*SEC_PER_DAY*1000);;

	// hour
	t = (timestamp / (SEC_PER_HOUR*1000));
	cal.hour += t;
	timestamp -= (t*SEC_PER_HOUR*1000);

	// minute
	t = (timestamp / (SEC_PER_MIN*1000));
	cal.min += t;
	timestamp -= (t*SEC_PER_MIN*1000);
	
	cal.sec = timestamp / 1000;
		
	return cal;
}

const char* cal_get_str_hms(char buf[9], timestamp_t timestamp)
{
	calendar_t cal = cal_convert(timestamp);
	sprintf(buf, "%02d:%02d:%02d", cal.hour, cal.min, cal.sec);
	return buf;
}
const char* cal_get_str(char buf[20], timestamp_t timestamp)
{
	calendar_t cal = cal_convert(timestamp);
	sprintf(buf, "%d-%d-%d %02d:%02d:%02d", 
		cal.year, cal.month, cal.day,
		cal.hour, cal.min, cal.sec);
	return buf;
}

timestamp_t cal_get_cur_timestamp(void)
{
	calendar_t cal = cal_get_cur_datetime();
	timestamp_t timestamp = cal_get_timestamp(&cal);
	
	// milliseconds difference
	{
		uint32_t cur, diff;
		
		cur  = app_timer_cnt_get();
		diff = app_timer_cnt_diff_compute(cur, lastTickForCalendar);	
		
		uint32_t diff_ms = (diff*1000) / APP_TIMER_CLOCK_FREQ;
		timestamp += diff_ms;
	}
	
	return timestamp;
}


timestamp_t cal_get_cur_timestamp_ms(uint32_t* ms)
{
	if (ms != NULL)
	{
		uint32_t cur, diff;
		
		cur  = app_timer_cnt_get();
		diff = app_timer_cnt_diff_compute(cur, lastTickForCalendar);	
		
		*ms = (diff*1000) / APP_TIMER_CLOCK_FREQ;
	}
	
	return cal_get_cur_timestamp();
}

timestamp_t	cal_get_total_sec_for_year(uint32_t year)
{
	timestamp_t t = SEC_PER_YEAR;
	if (is_leap_year(year))
		t += SEC_PER_DAY;
	return t;
}

timestamp_t	cal_get_total_sec_for_month(uint32_t year, uint8_t month)
{
	timestamp_t days = cal_get_days(year, month);
	return days * SEC_PER_DAY;
}

time_span_t	cal_get_diff(timestamp_t timestamp)
{
	return cal_get_cur_timestamp() - timestamp;
}

uint32_t cal_start(const calendar_init_t* p_init)
{
	uint32_t err_code;

	// check if calendar is already started
	if (calendar_started)
		return NRF_SUCCESS;
	
	if (p_init != NULL)
	{
		// start with specified datetime
		curDateTime = p_init->cur;
		m_last_min  = p_init->cur.min;
	
		// save callback for datetime changes
		m_cal_change_handler 	 = p_init->cal_change_handler;
		m_cal_min_change_handler = p_init->cal_min_change_handler;
		
		// send calendar change to callback
		if (m_cal_change_handler)
			m_cal_change_handler(curDateTime);		
	}
		
	err_code = app_timer_create(&m_timer_calendar_update, APP_TIMER_MODE_REPEATED, calendar_update_handler);
	APP_ERROR_CHECK(err_code);
	
	initTimer(&lastTickForCalendar);
	
	err_code = app_timer_start(m_timer_calendar_update, CALENDAR_UPDATE_TIMEOUT, NULL);
	APP_ERROR_CHECK(err_code);
	
	calendar_started = true;	
	
	return NRF_SUCCESS;
}

