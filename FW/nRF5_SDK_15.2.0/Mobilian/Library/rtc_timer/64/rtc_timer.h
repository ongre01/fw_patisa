#ifndef _RTC_Timer
#define _RTC_Timer

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

#define TICK2MS(tick) 						((tick) * (1000.0f / APP_TIMER_CLOCK_FREQ))
#define TICK2SEC(tick) 						(TICK2MS(tick) / 1000.0f)
#define MS2TICK(ms)   						((ms) * APP_TIMER_CLOCK_FREQ / 1000.0f)
#define SEC2TICK(sec) 						(MS2TICK((sec)*1000))

// simple timer 
typedef uint32_t	timer_t;

void 	initTimer(timer_t* timer);
bool 	isTimeout(timer_t* timer, uint32_t tolerance);
bool 	isTimerAlive(timer_t* timer, uint32_t tolerance);
void 	waitFor(uint32_t tolerance);
float 	elapseTime(timer_t* timer);
uint32_t compensate_sec(timer_t* timer);
uint64_t get_millis(void);
void     set_millis(uint64_t millis);
int64_t  get_millis_dif(uint64_t timestamp);

// Calendar
typedef struct
{
	int16_t year, month, day;
	int16_t hour, min, sec;
} calendar_t;

typedef uint64_t timestamp_t;
typedef int64_t  time_span_t;

#define TIMESTAMP_MS(t)  ( (t) )
#define TIMESTAMP_SEC(t) ( TIMESTAMP_MS(t) / 1000)

typedef void (*calendar_changed_handler_t)(calendar_t cal);
typedef struct
{
	calendar_t 					cur;
	calendar_changed_handler_t 	cal_change_handler;
	calendar_changed_handler_t	cal_min_change_handler;
} calendar_init_t;

uint32_t	cal_start(const calendar_init_t* p_init);	// sart calendar module
calendar_t 	cal_get_cur_datetime(void);					// get current datetime
calendar_t 	cal_get_cur_datetime_ms(uint32_t* ms);		// get current datetime
void 		cal_set_cur_datetime(calendar_t cal);		// get current datetime
uint16_t 	cal_get_days(uint32_t year, uint8_t month);
bool 		cal_is_valid(const calendar_t* const cal);
uint8_t	    cal_get_day_of_week(calendar_t cal);
int         cal_compare(const calendar_t* c1, const calendar_t* c2);
timestamp_t	cal_get_timestamp(const calendar_t* cal);	// get total seconds from 1900
calendar_t  cal_convert(timestamp_t timestamp);			// convert local time to calendar
time_span_t	cal_get_diff(timestamp_t timestamp);
timestamp_t	cal_get_cur_timestamp(void);
timestamp_t cal_get_cur_timestamp_ms(uint32_t* ms);
const char* cal_get_str_hms(char buf[9], timestamp_t timestamp);
const char* cal_get_str(char buf[20], timestamp_t timestamp);

timestamp_t	cal_get_total_sec_for_year(uint32_t year);
timestamp_t	cal_get_total_sec_for_month(uint32_t year, uint8_t month);

#ifdef __cplusplus
}
#endif


#endif // _RTC_Timer

