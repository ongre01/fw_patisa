#include "utility.h"
#include <stdio.h>

int32_t map(int32_t x, int32_t in_min, int32_t in_max, int32_t out_min, int32_t out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

float fmap(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

float add_to_mean(float mean, uint32_t n, float new_value)
{
    return mean + ((new_value - mean) / (n + 1));
}

const char* to_hex_string(char* dest, uint16_t dest_len, const void* src, uint16_t src_len, uint8_t separator)
{
	const char* org_dest = dest;
	const uint8_t* src_buf = (uint8_t*)src;
	uint16_t size = 0;
		
	for (int i=0; i<src_len && dest_len >= 3; i++) // NULL을 포함해서
	{
		sprintf(dest, "%02X", *src_buf++);
		dest     += 2;
		dest_len -= 2;
		size     += 2;
		
		// separator가 있고, 마지막이 아니라면 separator를 삽입한다
		if (separator != 0 && i!=src_len-1)
		{
			if (dest_len >= 2)
			{
				sprintf(dest, "%c", separator);
				dest     += 1;
				dest_len -= 1;
				size     += 1;
			}
			else
			{
				break;
			}
		}
	}
	
	return org_dest;
}

