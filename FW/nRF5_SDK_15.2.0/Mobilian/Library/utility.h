#ifndef _utility_h
#define _utility_h

#ifdef __cplusplus
extern "C" {
#endif

#include "sdk_common.h"
#include <float.h>
#include <math.h>
	
#define htonl(v) 				( ((v) >> 24) | (((v) >> 8) & 0x0000ff00) | (((v) << 8) & 0x00ff0000) | (((v) << 24) & 0xff000000) )
#define htons(v) 				( (((v) >> 8) & 0x00ff) | (((v) << 8) & 0xff00) )
	
#define ntohs(n)				((((n)&0xff00)>>8) | (((n)&0x00ff) << 8))
#define ntohl(n)				((((n)&0xff000000)>>24) | (((n)&0x00ff0000) >> 8)| ((n)&0x0000ff00) << 8| ((n)&0x000000ff) << 24)

#define bin2bcd(b)				(((b)%10) | (((b)/10) << 4))
#define bcd2bin(b)				(((b)&0x0f) + (((b)>>4) * 10))
	
#define BIT(n) 					(1<<(n))
	
int32_t map(int32_t x, int32_t in_min, int32_t in_max, int32_t out_min, int32_t out_max);
float   fmap(float x, float in_min, float in_max, float out_min, float out_max);
float   add_to_mean(float mean, uint32_t n, float new_value);
const char* to_hex_string(char* dest, uint16_t dest_len, const void* src, uint16_t src_len, uint8_t separator);

__STATIC_INLINE bool	is_float_zero(float value)
{
	return (fabsf(value) <= FLT_EPSILON);
}

__STATIC_INLINE bool	is_float_positive(float value)
{
	return (value >= FLT_EPSILON);
}
	
__STATIC_INLINE bool	is_float_negative(float value)
{
	return (value <= (-1.0f * FLT_EPSILON));
}
	
// measured_power : expected RSSI as a distance of 1 meter
// rssi           : detected rssi
// n              : environment factor(2~4)
__STATIC_INLINE float calc_distance_from_rssi(float measured_power, float rssi, int n)
{
	return pow(10, (measured_power - rssi) / (10 * n));
}

#ifdef __cplusplus
}
#endif

#endif // _utility_h
