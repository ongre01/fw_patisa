#include <stdlib.h>
#include <string.h>
#include "macros_common.h"

#include "m_menu.h"
#include "m_menu_internal_defs.h"
#include "menu_ids.h"

#include "nrf_delay.h"
#include "app_timer.h"
#include "config.h"

#ifndef MENU_LOG_ENABLED
	#define NRF_LOG_LEVEL 0
#endif

#define NRF_LOG_MODULE_NAME	MENU
#include "nrf_log.h"

// Helper macros for section variables.
#define MENU_SECTION_ITEM_GET(i)      NRF_SECTION_ITEM_GET(menu_data, menu_config_t, (i))
#define MENU_SECTION_ITEM_COUNT       NRF_SECTION_ITEM_COUNT(menu_data, menu_config_t)
#define MENU_SECTION_START_ADDR       NRF_SECTION_START_ADDR(menu_data)
#define MENU_SECTION_END_ADDR         NRF_SECTION_END_ADDR(menu_data)

// Create section 'menu__data'.
//lint -esym(526, menu_dataBase) -esym(526, menu_dataLimit)
NRF_SECTION_DEF(menu_data, menu_config_t);

static struct
{
	bool initialized;
	menu_config_t const* p_menu_current;
} m_cfg;

#define execute(fn_name) do {\
							err_code = NRF_SUCCESS; \
							if (m_cfg.p_menu_current != NULL && m_cfg.p_menu_current->fn_name != NULL) \
								err_code = m_cfg.p_menu_current->fn_name(); \
						} while(0)
#define execute_param(fn_name, param) do {\
							err_code = NRF_SUCCESS; \
							if (m_cfg.p_menu_current != NULL && m_cfg.p_menu_current->fn_name != NULL) \
								err_code = m_cfg.p_menu_current->fn_name(param); \
						} while(0)
#define execute_param2(fn_name, param1, param2) do {\
							err_code = NRF_SUCCESS; \
							if (m_cfg.p_menu_current != NULL && m_cfg.p_menu_current->fn_name != NULL) \
								err_code = m_cfg.p_menu_current->fn_name(param1, param2); \
						} while(0)

#define execute_evt(e, context) do {\
							err_code = NRF_SUCCESS; \
							if (m_cfg.p_menu_current != NULL && m_cfg.p_menu_current->evt_handler) \
								err_code = m_cfg.p_menu_current->evt_handler(e, context); \
						} while(0)

static menu_config_t const* get_menu(uint16_t id)
{
	uint32_t const menu_count = MENU_SECTION_ITEM_COUNT;
	
	for (uint32_t i=0; i<menu_count; i++)
	{
        menu_config_t const * const p_config = MENU_SECTION_ITEM_GET(i);
		if (p_config->menu_id == id)
			return p_config;
	}	
	
	NRF_LOG_ERROR("No menu id found : %d\n", id);
	
	return NULL;
}

static uint32_t set_menu(menu_config_t const* p_new_menu, uint32_t enter_param)
{
	uint32_t err_code;
	menu_id_type_t prev_menu_id = menu_id_unknown;

	if (m_cfg.p_menu_current != NULL)
		prev_menu_id = m_cfg.p_menu_current->menu_id;
	
	// leave
	execute(leave_handler);		
	if (err_code != NRF_SUCCESS)
	{
		NRF_LOG_INFO("Error on menu %d, leave()\n", err_code);
	}		

	// change current menu to specified
	m_cfg.p_menu_current = p_new_menu;
	
	// enter
	execute_param2(enter_handler, enter_param, prev_menu_id);		
	if (err_code != NRF_SUCCESS)
	{
		NRF_LOG_INFO("Error on menu %d, enter()\n", err_code);
	}		
	
	// notify event
	pushEvent2_force(EVT_menu_changed, 
		m_cfg.p_menu_current!=NULL?m_cfg.p_menu_current->menu_id:menu_id_none, 
		prev_menu_id);
	
	return err_code;
}

// run idle-time process
uint32_t m_menu_idle(void)
{
	uint32_t const menu_count = MENU_SECTION_ITEM_COUNT;
	uint32_t err_code;
	
	// run current idle handler
	execute(idle_handler);
	
	// run aws idle handler
	for (uint32_t i=0; i<menu_count; i++)
	{
        menu_config_t const * const p_config = MENU_SECTION_ITEM_GET(i);
		if (p_config->idle_handler != NULL
			&& p_config->call_idle)
		{
			p_config->idle_handler();
		}
	}
	
	return err_code;
}

void m_menu_boradcast_event(T_Event const * e, void* p_context)
{
	uint32_t const menu_count = MENU_SECTION_ITEM_COUNT;
	
	for (uint32_t i=0; i<menu_count; i++)
	{
        menu_config_t const * const p_config = MENU_SECTION_ITEM_GET(i);

		if (p_config->evt_handler)
			p_config->evt_handler(e, p_context);
	}		
}

// change menu to specified menu id
uint32_t m_menu_change(uint16_t menu_id, uint32_t enter_param)
{
	if (m_cfg.p_menu_current != NULL && m_cfg.p_menu_current->menu_id == menu_id)
	{
		NRF_LOG_INFO("Already in the menu : %d\n", menu_id);
		return NRF_SUCCESS;
	}
	
	menu_config_t const* new_menu = get_menu(menu_id);
	if(new_menu == NULL)
		return NRF_ERROR_NOT_FOUND;

	return set_menu(new_menu, enter_param);
}

// test currently select menu is specified menu
bool m_menu_is_current_menu(uint32_t menu_id)
{
	if (m_cfg.p_menu_current != NULL && m_cfg.p_menu_current->menu_id == menu_id)
		return true;
	return false;
}

menu_config_t const* m_menu_get_current(void)
{
	return m_cfg.p_menu_current;
}

__WEAK menu_id_type_t menu_get_initial_menu(void)
{
	return menu_id_none;
}

void m_menu_close(void)
{
	if (m_cfg.p_menu_current != NULL)
		set_menu(NULL, 0);
}

// initialize menu module
uint32_t m_menu_init(void)
{
	uint32_t const menu_count = MENU_SECTION_ITEM_COUNT;
	uint32_t err_code;
	
	UNUSED_VARIABLE(err_code);
	
	menu_config_t const* primary_menu  = NULL;
	
	for (uint32_t i=0; i<menu_count; i++)
	{
        menu_config_t const * const p_config = MENU_SECTION_ITEM_GET(i);
		
		if (p_config->init_handler)
			p_config->init_handler();
		
		// check primary
		if (p_config->primary)
		{
			if (primary_menu != NULL)
			{
				NRF_LOG_ERROR("Primary menu already exists(new id=%d)!\n", 
					p_config->menu_id);
			}
			else
			{
				primary_menu = p_config;
			}
		}
	}		
	
	m_cfg.initialized = true;
	
	// retrieve initial menu from realized instance
	menu_id_type_t initial_menu = menu_get_initial_menu();
	if (initial_menu != menu_id_none)
	{
		primary_menu = get_menu(initial_menu);
	}

	if (primary_menu)
		set_menu(primary_menu, 0);
	
	return NRF_SUCCESS;
}

//////////////////////////////////////////////////////////////////
static void evt_handler(T_Event const * evt, void* p_context)
{
	uint32_t err_code;
	
	UNUSED_VARIABLE(err_code);
	
	if (!m_cfg.initialized)
		return;
	
	if (evt->event == EVT_change_menu)
	{
		m_menu_change(evt->param1, evt->param2);
		return;
	}
			
	execute_evt(evt, p_context);
}

#ifdef FREERTOS
EVENT_QUEUE_OBSERVER(evt, M_MEMU_EVENT_PRIORITIES) =
{
    .evt_handler = evt_handler,
    .p_context = NULL,
};
#else
EVENT_QUEUE_REGISTER_CFG(evt_config) = {
	evt_handler,
};
#endif
