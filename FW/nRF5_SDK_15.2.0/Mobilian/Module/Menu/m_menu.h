#ifndef _m_menu_h
#define _m_menu_h

#include "sdk_common.h"
#include "nrf_section.h"

#ifdef FREERTOS
	#include "m_event_queue.h"
#else
	#include "eventQueue.h"
#endif	

#include "menu_ids.h"

typedef uint8_t	menu_id_type_t;

typedef uint32_t (*menu_handler_t)(void);
typedef uint32_t (*menu_handler_enter_t)(uint32_t param, menu_id_type_t prev_menu_id);
typedef uint32_t (*menu_handler_evt_t)(T_Event const * e, void* p_context);

#define menu_id_unknown 	0xff

typedef struct
{
	menu_id_type_t			menu_id;		// menu id
	const char*				name;
	
	uint8_t					primary:1;		// indicates primary service, only one instance can have primary
	uint8_t					call_idle:1;	// indicates idle handler must be called when not in the menu
	uint8_t					reserved:7;		
	
	menu_handler_t			init_handler;	// called when the menu is being initialized
	menu_handler_enter_t	enter_handler;	// called when menu is being selected
	menu_handler_t 			leave_handler;	// called when menu is being leaved
	menu_handler_evt_t		evt_handler;	// event handler
	menu_handler_t 			idle_handler;	// called when the system is in idle state
} menu_config_t;

#define MENU_REGISTER_CFG(cfg_var) NRF_SECTION_ITEM_REGISTER(menu_data, static menu_config_t const cfg_var)

uint32_t m_menu_init(void);
void     m_menu_close(void);
uint32_t m_menu_idle(void);
uint32_t m_menu_change(uint16_t menu_id, uint32_t enter_param);
void 	 m_menu_boradcast_event(T_Event const * e, void* p_context);
bool     m_menu_is_current_menu(uint32_t menu_id);

menu_config_t const* m_menu_get_current(void);

#define change_menu(id)					pushEvent1_force(EVT_change_menu, id)
#define change_menu_param(id, param)	pushEvent2_force(EVT_change_menu, id, param)

#endif // _m_menu_h
