#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"

#include "app_scanner.h"
#include "app_state.h"
#include "app_timer.h"
#include "m_ble.h"

#include "macros_common.h"
#include "config.h"
#include "eventQueue.h"
#include "stdlib.h"
#include "nrf_delay.h"
#include "ble_advdata.h"

//#define EXTENDED_FEATURE

#define UUID16_SIZE     2   /**< Size of 16 bit UUID. */
#define UUID32_SIZE     4   /**< Size of 32 bit UUID. */
#define UUID128_SIZE    16  /**< Size of 128 bit UUID. */

#define APP_DISC_LOG_ENABLED //test
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
#ifndef APP_DISC_LOG_ENABLED
    #define NRF_LOG_LEVEL 0
#endif

#define NRF_LOG_MODULE_NAME SCAN
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

#define NUS_BASE_UUID                   {{0x9E, 0xCA, 0xDC, 0x24, 0x0E, 0xE5, 0xA9, 0xE0, 0x93, 0xF3, 0xA3, 0xB5, 0x00, 0x00, 0x40, 0x6E}} /**< Used vendor specific UUID. */

#define BLE_UUID_NUS_SERVICE            0x0001                      /**< The UUID of the Nordic UART Service. */
#define BLE_UUID_NUS_RX_CHARACTERISTIC  0x0002                      /**< The UUID of the RX Characteristic. */
#define BLE_UUID_NUS_TX_CHARACTERISTIC  0x0003                      /**< The UUID of the TX Characteristic. */

// enable extended feature
#define SCAN_BUFFER_SIZE (BLE_GAP_SCAN_BUFFER_EXTENDED_MAX)
//#define SCAN_BUFFER_SIZE (BLE_GAP_SCAN_BUFFER_MIN)

//////////////////////////////////////////////////////////////////////////////////////
// section definition
// Helper macros for section variables.
#define EVT_SECTION_VARS_GET(i)          NRF_SECTION_ITEM_GET(app_scanner_report_data, app_scanner_report_config_t, (i))
#define EVT_SECTION_VARS_COUNT           NRF_SECTION_ITEM_COUNT(app_scanner_report_data, app_scanner_report_config_t)
#define EVT_SECTION_VARS_START_ADDR      NRF_SECTION_ITEM_START_ADDR(app_scanner_report_data)
#define EVT_SECTION_VARS_END_ADDR        NRF_SECTION_ITEM_END_ADDR(app_scanner_report_data)


// Create section 'event_queue_data'.
NRF_SECTION_DEF(app_scanner_report_data, app_scanner_report_config_t);

/////////////////////////////////////////////////////////////////////////////////////////////////
static struct
{
	app_scanner_handler_t scan_handler;
	uint8_t               phy;
	
	uint8_t				  initialized:1;
	uint8_t               started:1;
} m_cfg;

// Register a handler for BLE events.
static void app_scanner_on_ble_evt(ble_evt_t const * p_ble_evt, void* p_context);
NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, app_scanner_on_ble_evt, NULL);

/////////////////////////////////////////////////////////////////////////////////////////////////
// scan buffer - buffer where advertising reports will be stored by the SoftDevice.
static uint8_t m_scan_buffer_data[SCAN_BUFFER_SIZE];
static ble_data_t m_scan_buffer =
{
    .p_data = m_scan_buffer_data,
    .len    = SCAN_BUFFER_SIZE,
};

// Parameters used when scanning.
static ble_gap_scan_params_t m_scan_params =
{
    .active   = 1,
    .interval = SCAN_INTERVAL,
    .window   = SCAN_WINDOW, 
    .timeout  = SCAN_TIMEOUT,				//Scan timeout in 10 ms units. @sa BLE_GAP_SCAN_TIMEOUT. */
	.filter_policy = BLE_GAP_SCAN_FP_ACCEPT_ALL,
	.extended  = false,
	.scan_phys = BLE_GAP_PHY_1MBPS,
	//.scan_phys = BLE_GAP_PHY_1MBPS,
	//.scan_phys = BLE_GAP_PHYS_SUPPORTED,
	//.scan_phys = BLE_GAP_PHY_CODED,
	//.scan_phys = BLE_GAP_PHY_1MBPS | BLE_GAP_PHY_2MBPS,
	//.scan_phys = BLE_GAP_PHY_1MBPS,
};


static void update_phy(void)
{
	uint8_t phy = m_cfg.phy;
	
	// 1MBPS 전용이라면 extended를 활성화 하지 않음
	if (phy == BLE_GAP_PHY_1MBPS)
	{
		m_scan_params.extended = false;
	}
	else
	{
		m_scan_params.extended = true;
	}
	
	if (phy & BLE_GAP_PHY_2MBPS)
	{
		phy &= (~BLE_GAP_PHY_2MBPS);
		phy |= BLE_GAP_PHY_1MBPS;        // ToDo : 이게 맞는것인지 확인
	}
	
	m_scan_params.scan_phys = phy;
}

bool app_scanner_find_adv_item(const uint8_t* data, uint8_t length, uint8_t find_item_type, const uint8_t** p_item_data, uint8_t* p_item_len)
{
	while (length > 2)
	{
		uint8_t item_len  = *data++; length--;
		uint8_t item_type = *data++; length--;
		
		uint8_t item_dlen = item_len - 1;
		
		if (length < item_dlen)
			break;

		if (item_type == find_item_type)
		{
			*p_item_data = data;
			*p_item_len  = item_dlen;
			
			return true;
		}
		
		data   += item_dlen;
		length -= item_dlen;
	}
	
	return false;
}

bool app_scanner_parse_name(const ble_gap_evt_adv_report_t * rep, const uint8_t** p_item_data, uint8_t* p_item_len)
{
	if (app_scanner_find_adv_item(rep->data.p_data, rep->data.len, BLE_GAP_AD_TYPE_COMPLETE_LOCAL_NAME, p_item_data, p_item_len))
		return true;
	
	if (app_scanner_find_adv_item(rep->data.p_data, rep->data.len, BLE_GAP_AD_TYPE_SHORT_LOCAL_NAME, p_item_data, p_item_len))
		return true;
	
	return false;
}

static void dispatch_scan_report(const ble_gap_evt_adv_report_t * rep)
{
	uint32_t const total_users = EVT_SECTION_VARS_COUNT;
	for (uint32_t i = 0; i < total_users; i++)
	{
		app_scanner_report_config_t const * const p_config = EVT_SECTION_VARS_GET(i);
		if (p_config->report_handler)
			p_config->report_handler(rep);
	}
}

static void on_report_adv_data(const ble_gap_evt_adv_report_t * rep)
{	
	uint8_t target_phy = 0;

	// filter phy
	if (rep->primary_phy != BLE_GAP_PHY_NOT_SET)
		target_phy |= rep->primary_phy;
	if (rep->secondary_phy != BLE_GAP_PHY_NOT_SET)
		target_phy |= rep->secondary_phy;
	
	if ((target_phy & m_cfg.phy) == 0)
		return;
	
	if (m_cfg.scan_handler)
	{
		m_cfg.scan_handler(rep);
	}
	
	// dispatch to the listeners
	dispatch_scan_report(rep);
}

static void app_scanner_on_ble_evt(ble_evt_t const * p_ble_evt, void* p_context)
{
    const ble_gap_evt_t * p_gap_evt = &p_ble_evt->evt.gap_evt;
	
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_ADV_REPORT:
        {
            const ble_gap_evt_adv_report_t * p_adv_report = &p_gap_evt->params.adv_report;
			on_report_adv_data(p_adv_report);
			
			// resume the scanning
			sd_ble_gap_scan_start(NULL, &m_scan_buffer);
        }
		break;
	}
}

uint32_t app_scanner_stop(void)
{
	uint32_t err_code=NRF_SUCCESS;
	
	if (m_cfg.started)
	{
		err_code = sd_ble_gap_scan_stop();
		REPORT_IF_ERROR(err_code);
		
		m_cfg.started = false;
	}
	
	return err_code;
}

uint32_t app_scanner_start(void)
{
    uint32_t err_code;

	if (!m_cfg.initialized)
		return NRF_ERROR_INVALID_STATE;
	
	if (!m_cfg.started)
	{
		NRF_LOG_INFO("Start scan");
		// start scan
		err_code = sd_ble_gap_scan_start(&m_scan_params, &m_scan_buffer);
		RETURN_IF_ERROR(err_code);
		
		m_cfg.started = true;
	}
	
	return NRF_SUCCESS;
}

const ble_gap_scan_params_t* app_scanner_scan_params_get(void)
{
	return &m_scan_params;
}

uint32_t app_scanner_update_phy(uint8_t phy)
{
	uint32_t err_code = NRF_SUCCESS;
	bool restart = m_cfg.started;
	
	if (m_cfg.started)
	{
		app_scanner_stop();
	}

	m_cfg.phy = phy;
	update_phy();
	
	if (restart)
		err_code = app_scanner_start();
	
	return err_code;
}

bool app_scanner_is_scanning(void)
{
	return m_cfg.started;
}

uint32_t app_scanner_init(app_scanner_cfg_t const* p_cfg)
{
    NULL_PARAM_CHECK(p_cfg);
	
	m_cfg.initialized  = true;
	m_cfg.scan_handler = p_cfg->scan_callback;
	m_cfg.phy          = p_cfg->phy;
	
	update_phy();
	
	return NRF_SUCCESS;
}
