#ifndef _app_scanner
#define _app_scanner

#include "sdk_common.h"
#include "ble_gap.h"

#include "nrf_section.h"

typedef void (*app_scanner_handler_t)(const ble_gap_evt_adv_report_t * rep);

typedef struct
{
	uint8_t					phy;
	app_scanner_handler_t	scan_callback;
} app_scanner_cfg_t;

uint32_t app_scanner_init(app_scanner_cfg_t const* p_cfg);
uint32_t app_scanner_start(void);
uint32_t app_scanner_stop(void);
uint32_t app_scanner_update_phy(uint8_t phy);
bool     app_scanner_is_scanning(void);

const ble_gap_scan_params_t*    app_scanner_scan_params_get(void);
bool app_scanner_find_adv_item(const uint8_t* data, uint8_t length, uint8_t find_item_type, const uint8_t** p_item_data, uint8_t* p_item_len);
bool app_scanner_parse_name(const ble_gap_evt_adv_report_t * rep, const uint8_t** p_item_data, uint8_t* p_item_len);

//////////////////////////////////////////////////////////////////////////////////////
// section definition
typedef void (*app_scanner_report_handler_t)(const ble_gap_evt_adv_report_t * rep);
typedef struct
{
	app_scanner_report_handler_t report_handler;
} app_scanner_report_config_t;

#define APP_SCANNER_REPORT_REGISTER_CFG(cfg_var) NRF_SECTION_ITEM_REGISTER(app_scanner_report_data, static app_scanner_report_config_t const cfg_var)

#endif  // _app_scanner
