#include "m_adc_meas.h"
#include "sdk_config.h"
#include "nrf_gpio.h"
#include "nrf_drv_saadc.h"
#include "nrf_drv_gpiote.h"
#include "app_timer.h"
#include "math.h"
#include "nrf_gpio.h"
#include "app_scheduler.h"
#include "app_fifo.h"
#include "macros_common.h"
#include "nrf_drv_saadc.h"
#include "app_scheduler.h"
#include "nrf_delay.h"
#include "app_state.h"
#include "config.h"

#include <stdint.h>
#include <string.h>

#define M_ADC_LOG_ENABLED //test
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
#ifndef M_ADC_LOG_ENABLED
	#define NRF_LOG_LEVEL 0
#endif

#define NRF_LOG_MODULE_NAME m_adc

#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();
#include "nrf_log_ctrl.h"

#define ADC_GAIN                    NRF_SAADC_GAIN1_6     // ADC gain.
#define ADC_REFERENCE_VOLTAGE       (0.6f)              // The standard internal ADC reference voltage.
#define ADC_RESOLUTION_BITS         (8 + (SAADC_CONFIG_RESOLUTION * 2)) //ADC resolution [bits].
#define ADC_BUF_SIZE                (1)                 // Size of each ADC buffer.
#define INVALID_BATTERY_LEVEL       (0xFF)              // Invalid/default battery level.

static m_adc_meas_event_handler_t   m_evt_handler;                  // Event handler function pointer.
static nrf_saadc_value_t            m_buffer[ADC_BUF_SIZE];         //
static volatile bool                m_adc_cal_in_progress;          //
static volatile bool                m_adc_in_progress;          	//
static m_adc_param_t 				m_param;
static int32_t						m_voltage;
static int16_t 						m_raw;

// Converts ADC gain register values to actual gain.
static uint32_t adc_gain_enum_to_real_gain(nrf_saadc_gain_t gain_reg, float * real_val)
{
    switch(gain_reg)
    {
        case NRF_SAADC_GAIN1_6: *real_val = 1 / (float)6;
        break;
        case NRF_SAADC_GAIN1_5: *real_val = 1 / (float)5;
        break;
        case NRF_SAADC_GAIN1_4: *real_val = 1 / (float)4;
        break;
        case NRF_SAADC_GAIN1_3: *real_val = 1 / (float)3;
        break;
        case NRF_SAADC_GAIN1_2: *real_val = 1 / (float)2;
        break;
        case NRF_SAADC_GAIN1:   *real_val = 1;
        break;
        case NRF_SAADC_GAIN2:   *real_val = 2;
        break;
        case NRF_SAADC_GAIN4:   *real_val = 3;
        break;
        default: return NRF_ERROR_INVALID_PARAM;
    };

    return NRF_SUCCESS;
}

// Converts an ADC reading to battery voltage. Returned as an integer in millivolts.
static uint32_t adc_to_voltage(uint32_t adc_val, int16_t * const voltage)
{
    uint32_t err_code;
    float    adc_gain;

    err_code = adc_gain_enum_to_real_gain(m_param.gain, &adc_gain);
    RETURN_IF_ERROR(err_code);

    float tmp = adc_val / ((adc_gain / ADC_REFERENCE_VOLTAGE) * pow(2, ADC_RESOLUTION_BITS));
	*voltage = (int16_t)(tmp * 1000 / m_param.divider_factor);

    return NRF_SUCCESS;
}

//  The batt_event_handler event handler for ADC conversions, executed in main context.
static void app_sched_event_handler_adc(void * p_event_data, uint16_t size)
{
	uint16_t* voltage = (uint16_t*)p_event_data;
	
    m_adc_meas_event_t evt = {
		.voltage_mv = *voltage,
		.p_param    = &m_param,
	};
	
	if (m_evt_handler != NULL)
		m_evt_handler(&evt);
}


// The SAADC event handler, executed in interrupt context.
static void saadc_event_handler_interrupt(nrf_drv_saadc_evt_t const * const p_event)
{
    uint32_t err_code;
    int16_t  voltage;

    if (p_event->type == NRF_DRV_SAADC_EVT_CALIBRATEDONE)
    {
        m_adc_cal_in_progress = false;
    }
    else if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {
		nrf_saadc_value_t value = *p_event->data.done.p_buffer;
		
		err_code = adc_to_voltage(value, &voltage);
		APP_ERROR_CHECK(err_code);
	
		// save current value
		m_voltage = voltage;
		m_raw     = value;
		
		if (m_evt_handler)
		{
			err_code = app_sched_event_put(&voltage, sizeof(uint16_t), app_sched_event_handler_adc);
			APP_ERROR_CHECK(err_code);
		}
    }

    nrf_drv_saadc_uninit();
	m_adc_in_progress = false;
}


//  SAADC calibration.
static uint32_t saadc_calibrate(void)
{
    uint32_t err_code;

    nrf_drv_saadc_config_t saadc_config = NRF_DRV_SAADC_DEFAULT_CONFIG;

    err_code = nrf_drv_saadc_init(&saadc_config, saadc_event_handler_interrupt);
    RETURN_IF_ERROR(err_code);

    m_adc_cal_in_progress = true;
    err_code = nrf_drv_saadc_calibrate_offset();
    RETURN_IF_ERROR(err_code);

    while(m_adc_cal_in_progress)
    {
        /* Wait for SAADC calibration to finish. */
    }

    return NRF_SUCCESS;
}


// Function for converting a GPIO pin number to an analog input pin number used in the channel
//        configuration.
static nrf_saadc_input_t nrf_drv_saadc_gpio_to_ain(uint32_t pin)
{
    // AIN0 - AIN3
    if (pin >= 2 && pin <= 5)
    {
        // 0 means "not connected", hence this "+ 1"
        return (nrf_saadc_input_t)(pin - 2 + 1);
    }
    // AIN4 - AIN7
    else if (pin >= 28 && pin <= 31)
    {
        return (nrf_saadc_input_t)(pin - 24 + 1);
    }
	else if (pin == 0xff)
	{
		return NRF_SAADC_INPUT_VDD;
	}
    else
    {
        return NRF_SAADC_INPUT_DISABLED;
    }
}

//  Basic configuration of the SAADC.
static uint32_t saadc_init(void)
{
    uint32_t err_code;

    nrf_drv_saadc_config_t saadc_config = NRF_DRV_SAADC_DEFAULT_CONFIG;

    err_code = nrf_drv_saadc_init(&saadc_config, saadc_event_handler_interrupt);
    RETURN_IF_ERROR(err_code);

    nrf_saadc_channel_config_t channel_config =
		NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(nrf_drv_saadc_gpio_to_ain(m_param.pin));

    /* Burst enabled to oversample the SAADC. */
    channel_config.burst    = NRF_SAADC_BURST_DISABLED;
    channel_config.gain     = m_param.gain;
    channel_config.acq_time = m_param.acq_time;

    err_code = nrf_drv_saadc_channel_init(0, &channel_config);
    RETURN_IF_ERROR(err_code);

    err_code = nrf_drv_saadc_buffer_convert(m_buffer, ADC_BUF_SIZE);
    RETURN_IF_ERROR(err_code);

	m_adc_in_progress = true;
	
    return NRF_SUCCESS;
}

static uint32_t saadc_get_sample(void)
{
    uint32_t err_code;

	do
	{
		if (m_adc_in_progress)
		{
			NRF_LOG_INFO("SAADC in progress");
			err_code = NRF_ERROR_INVALID_STATE;
			break;
		}
	
		err_code = saadc_init();
		if (err_code != NRF_SUCCESS)
		{
			NRF_LOG_INFO("SAADC init error");
		}
		BREAK_IF_ERROR(err_code);

		err_code = nrf_drv_saadc_sample();
		BREAK_IF_ERROR(err_code);
		
		return NRF_SUCCESS;
	}
	while (0);
	
	nrf_drv_saadc_uninit();
	
	return err_code;
}

uint32_t m_adc_meas_get(m_adc_param_t const * const param)
{
	uint32_t err_code;
	
	// update param changes
	if (param != NULL)
	{
		if (param->divider_factor == 0)
		{
			return NRF_ERROR_INVALID_PARAM;
		}
		
		m_param = *param;
	}
	
	err_code = saadc_get_sample();
	RETURN_IF_ERROR(err_code);
	
	return NRF_SUCCESS;
}

uint32_t m_adc_meas_get_sample(m_adc_param_t const * const param, int32_t* p_value)
{
	uint32_t err_code;

	err_code = m_adc_meas_get(param);
	RETURN_IF_ERROR(err_code);
	
	while (m_adc_in_progress)
		;
	
	*p_value = m_voltage;
	
	return NRF_SUCCESS;
}

uint32_t m_adc_meas_get_sample_raw(m_adc_param_t const * const param, int16_t* p_value)
{
	uint32_t err_code;

	err_code = m_adc_meas_get(param);
	RETURN_IF_ERROR(err_code);
	
	while (m_adc_in_progress)
		;
	
	*p_value = m_raw;
	
	return NRF_SUCCESS;
}
uint32_t m_adc_meas_init(m_adc_meas_init_t const * const p_init)
{
    uint32_t err_code;

    NRF_LOG_INFO("ADC measurement init \r\n");

	// save param
	if (p_init)
	{
		if (p_init->p_param)
			m_param = *p_init->p_param;
		
		m_evt_handler = p_init->evt_handler;
	}

    err_code = saadc_calibrate();
    RETURN_IF_ERROR(err_code);

    return NRF_SUCCESS;
}

