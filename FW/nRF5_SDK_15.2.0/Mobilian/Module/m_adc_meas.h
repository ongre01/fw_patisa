// ADC measurement module

#ifndef __M_ADC_MEAS_H__
#define __M_ADC_MEAS_H__

#include "sdk_common.h"
#include "nrf_drv_saadc.h"

typedef struct
{
	uint32_t            pin;
	nrf_saadc_gain_t    gain;		// NRF_SAADC_GAIN1_6
	nrf_saadc_acqtime_t	acq_time;	// NRF_SAADC_ACQTIME_10US
	float               divider_factor;
} m_adc_param_t;


typedef struct
{
    uint16_t                    voltage_mv;         ///< Battery voltage given in millivolts.
	m_adc_param_t*              p_param;
} m_adc_meas_event_t;


typedef void (*m_adc_meas_event_handler_t)(m_adc_meas_event_t const * p_event);

// Init parameters for m_batt_meas.
typedef struct
{
	m_adc_param_t*                 p_param;
    m_adc_meas_event_handler_t     evt_handler;        ///< Function pointer to the event handler (executed in main context).
} m_adc_meas_init_t;

uint32_t m_adc_meas_init(m_adc_meas_init_t const * const p_batt_meas_init);
uint32_t m_adc_meas_get_sample(m_adc_param_t const * const param, int32_t* p_value);
uint32_t m_adc_meas_get_sample_raw(m_adc_param_t const * const param, int16_t* p_value);
uint32_t m_adc_meas_get(m_adc_param_t const * const param);

#endif

