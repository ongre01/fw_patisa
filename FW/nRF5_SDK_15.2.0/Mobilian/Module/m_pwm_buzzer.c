#include "drv_pwm_wave.h"
#include "m_pwm_buzzer.h"
#include "macros_common.h"

#include "app_timer.h"
#include "note.h"

//#define BUZZER_LOG_ENABLED //test
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
#ifndef BUZZER_LOG_ENABLED
    #define NRF_LOG_LEVEL 0
#endif

#define NRF_LOG_MODULE_NAME	buz
#include "nrf_log.h"

NRF_LOG_MODULE_REGISTER();

// defines buzzer duty
#define DEF_BUZZER_DUTY				0.05f		// 5% default duty
#define DEF_BUZZER_FADE_RATE		0.1f		// 10% fade
#define DEF_BUZZER_FADE_INTERVAL_MS	5			// 50ms

#ifndef BUZZER_PWM_CLK
	#define BUZZER_PWM_CLK		NRF_PWM_CLK_500kHz
#endif

#ifndef BUZZER_DUTY
	#define BUZZER_DUTY			DEF_BUZZER_DUTY
#endif

#ifndef BUZZER_FADE_RATE
	#define BUZZER_FADE_RATE	DEF_BUZZER_FADE_RATE
#endif

#ifndef BUZZER_FADE_INTERVAL_MS
	#define BUZZER_FADE_INTERVAL_MS	DEF_BUZZER_FADE_INTERVAL_MS
#endif

enum
{
	state_play_note,
	state_fade,
};

static struct
{
	bool 						initialized;
	m_pwm_buzzer_play_handler_t callback;
	bool 						playing;
	m_pwm_buzzer_note_t const* 	p_note;
	uint16_t					current;
	uint16_t 					repeat;
	
	float						duty;
	uint8_t						state;
} m_cfg;

APP_TIMER_DEF(play_timer);

static __INLINE void stop_play_timer(void)
{
	app_timer_stop(play_timer);
}

static void	report_event(m_pwm_buzzer_evt_type_t e, uint16_t cur)
{
	if (m_cfg.callback == NULL)
		return;
	
	m_pwm_buzzer_event_t event;
	
	event.evt = e;
	event.cur = cur;
	
	m_cfg.callback(&event);
}

// stop playingg
void m_pwm_buzzer_play_stop(void)
{
	if (!m_cfg.playing)
		return;
	
	stop_play_timer();
	drv_pwm_wave_stop(false);
	
	m_cfg.playing = false;
	m_cfg.p_note  = NULL;
	m_cfg.current = m_cfg.repeat = 0;
	
	// report event
	report_event(pwm_buzzer_event_end, 0);
}

static void play_current_note(void)
{
	uint32_t err_code;
	
	m_cfg.duty  = BUZZER_DUTY;						// save duty
	m_cfg.state = state_play_note;					// state as play note
	
	m_pwm_buzzer_note_t const* ni = &(m_cfg.p_note[m_cfg.current]);
	if (ni->duration == 0)
	{
		trace("Invalid data");
		return;
	}
	
	if (ni->note != 0)
	{
		if (ni->note != NOTE_OFF)
		{
			drv_pwm_wave_set_duty(BUZZER_DUTY);				// set duty
			drv_pwm_wave_set_frequency(ni->note);			// set frequency
			
			// start PWM
			drv_pwm_wave_start();
		}
	}
	
	// start timer
	err_code = app_timer_start(play_timer, APP_TIMER_TICKS(ni->duration), NULL);
	REPORT_IF_ERROR(err_code);

	// report event
	report_event(pwm_buzzer_event_note, m_cfg.current);
	
	trace("play %d note(%d)/%dms", m_cfg.current, 
			ni->note, ni->duration);
}


static void move_next(void)
{
	m_cfg.current++;
	
	// check for end sign
	if (m_cfg.p_note[m_cfg.current].duration == 0)
	{				
		if (m_cfg.repeat   != BUZ_REPEAT_INFINITE &&
			--m_cfg.repeat == 0)
		{
			m_pwm_buzzer_play_stop();
			return;
		}
		
		// reset current position
		m_cfg.current = 0;
	}
	
	play_current_note();
}

static void play_timeout_handler(void* p_context)
{
	if (m_cfg.state == state_play_note)
	{
		if (m_cfg.p_note[m_cfg.current].note == NOTE_OFF)
		{
			move_next();
			return;
		}
		
		trace("fade");
		// fade 상태로 이동
		m_cfg.state = state_fade;
	}

	if (m_cfg.state == state_fade)
	{
		m_cfg.duty = MAX(m_cfg.duty - BUZZER_DUTY * BUZZER_FADE_RATE, 0);
		
		if (m_cfg.p_note[m_cfg.current].note != 0)
			drv_pwm_wave_set_duty(m_cfg.duty);
		
		// when duty reaches to 0, play next note
		if (m_cfg.duty == 0)
		{
			move_next();
		}
		else
		{
			app_timer_start(play_timer, APP_TIMER_TICKS(BUZZER_FADE_INTERVAL_MS), NULL);
		}
	}
}

// start playing
uint32_t m_pwm_buzzer_play_start(m_pwm_buzzer_note_t const* p_note, uint16_t repeat)
{
	if (!m_cfg.initialized)
		return NRF_ERROR_INVALID_STATE;
	
	NULL_PARAM_CHECK(p_note);
	
	// stop previously playing notes
	m_pwm_buzzer_play_stop();
	
	m_cfg.p_note  = p_note;
	m_cfg.repeat  = repeat;
	m_cfg.current = 0;
	
	m_cfg.playing = true;

	play_current_note();
	
	return NRF_SUCCESS;
}


void m_pwm_buzzer_close(void)
{
	if (!m_cfg.initialized)
		return;
	
	m_pwm_buzzer_play_stop();
	drv_pwm_wave_shutdown();
	
	m_cfg.initialized = false;
}

uint32_t m_pwm_buzzer_init(m_pwm_buzzer_play_handler_t handler)
{
	uint32_t err_code;
	
	if (m_cfg.initialized)
	{
		return NRF_SUCCESS;
	}
	
	do
	{
#ifdef BUZ_POWER
		nrf_gpio_cfg_output(BUZ_POWER);
		nrf_gpio_pin_set(BUZ_POWER);
#endif
		
		// set callback
		m_cfg.callback = handler;
		
		// 20 ~ 20Khz 까지 조정해야 하므로
		// 500000 / 20  = 25,000
		// 1M / 20K = 25
		err_code = drv_pwm_wave_init(BUZZER_PWM_CLK);
		BREAK_IF_ERROR(err_code);
		
		drv_pwm_wave_set_duty(BUZZER_DUTY);
		
		err_code = app_timer_create(&play_timer, APP_TIMER_MODE_SINGLE_SHOT, play_timeout_handler);
		BREAK_IF_ERROR(err_code);
	
		m_cfg.initialized = true;
	}
	while (0);
	
	return err_code;
}
