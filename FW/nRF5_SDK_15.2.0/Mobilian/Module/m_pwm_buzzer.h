#ifndef _m_pwm_buzzer
#define _m_pwm_buzzer

#include "sdk_common.h"

typedef enum
{
	pwm_buzzer_event_note,
	pwm_buzzer_event_end,
} m_pwm_buzzer_evt_type_t;

typedef struct
{
	m_pwm_buzzer_evt_type_t evt;
	uint16_t cur;			// indicates current position when evt is pwm_buzzer_event_note
} m_pwm_buzzer_event_t;

typedef void (*m_pwm_buzzer_play_handler_t)(m_pwm_buzzer_event_t const* event);

typedef struct
{
	uint16_t	note;
	uint32_t	duration;
} m_pwm_buzzer_note_t;

#define BUZ_REPEAT_INFINITE	0xffff

uint32_t m_pwm_buzzer_init(m_pwm_buzzer_play_handler_t handler);
void     m_pwm_buzzer_close(void);

uint32_t m_pwm_buzzer_play_start(m_pwm_buzzer_note_t const* p_note, uint16_t repeats);
void     m_pwm_buzzer_play_stop(void);

#endif // _m_pwm_buzzer
