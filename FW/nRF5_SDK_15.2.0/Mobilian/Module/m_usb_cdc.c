#include "m_usb_cdc.h"
#include "macros_common.h"

#include "app_util.h"
#include "app_usbd_core.h"
#include "app_usbd.h"
#include "app_usbd_string_desc.h"
#include "app_usbd_cdc_acm.h"
#include "app_usbd_serial_num.h"
#include "nrf_drv_clock.h"
#include "nrf_drv_power.h"

//#define NRF_LOG_LEVEL 0

#define NRF_LOG_MODULE_NAME	M_USB_CDC
#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

static struct
{
	bool connected;
	bool ready;
	usb_acm_data_receive_callback_t rcv_callback;
	usb_acm_status_callback_t   acm_status_callback;
} m_config = {
	.connected = false
};

// USB DEFINES START
static void cdc_acm_user_ev_handler(app_usbd_class_inst_t const * p_inst,
                                    app_usbd_cdc_acm_user_event_t event);
// 2019.01.21 nRFConnect Driver
#ifdef NRF_CDC_DRIVER
	// 2019.01.21 CDC-ACM Driver
	#define CDC_ACM_COMM_INTERFACE  0
	#define CDC_ACM_COMM_EPIN       NRF_DRV_USBD_EPIN2

	#define CDC_ACM_DATA_INTERFACE  1
	#define CDC_ACM_DATA_EPIN       NRF_DRV_USBD_EPIN1
	#define CDC_ACM_DATA_EPOUT      NRF_DRV_USBD_EPOUT1
#else
	#define CDC_ACM_COMM_INTERFACE  1
	#define CDC_ACM_COMM_EPIN       NRF_DRV_USBD_EPIN2

	#define CDC_ACM_DATA_INTERFACE  2
	#define CDC_ACM_DATA_EPIN       NRF_DRV_USBD_EPIN1
	#define CDC_ACM_DATA_EPOUT      NRF_DRV_USBD_EPOUT1
#endif

// CDC_ACM class instance
APP_USBD_CDC_ACM_GLOBAL_DEF(m_usb_cdc_acm,
                            cdc_acm_user_ev_handler,
                            CDC_ACM_COMM_INTERFACE,
                            CDC_ACM_DATA_INTERFACE,
                            CDC_ACM_COMM_EPIN,
                            CDC_ACM_DATA_EPIN,
                            CDC_ACM_DATA_EPOUT,
// 2020.09.10 ����          APP_USBD_CDC_COMM_PROTOCOL_AT_V250);
                            APP_USBD_CDC_COMM_PROTOCOL_NONE);

static void report_acm_status(app_usbd_cdc_acm_user_event_t event)
{
	if (m_config.acm_status_callback!=NULL)
		m_config.acm_status_callback(event);
}

/** @brief User event handler @ref app_usbd_cdc_acm_user_ev_handler_t */
static void cdc_acm_user_ev_handler(app_usbd_class_inst_t const * p_inst,
                                    app_usbd_cdc_acm_user_event_t event)
{
	static uint8_t ch;
	ret_code_t ret;
    app_usbd_cdc_acm_t const * p_cdc_acm = app_usbd_cdc_acm_class_get(p_inst);
	
	
    switch (event)
    {
        case APP_USBD_CDC_ACM_USER_EVT_PORT_OPEN:
            //Set up the first transfer
            ret = app_usbd_cdc_acm_read(&m_usb_cdc_acm, &ch, 1);
            UNUSED_VARIABLE(ret);
			m_config.ready = true;
            NRF_LOG_INFO("CDC ACM port opened");
            break;
        case APP_USBD_CDC_ACM_USER_EVT_PORT_CLOSE:
			m_config.ready = false;
            NRF_LOG_INFO("CDC ACM port closed");
            break;

        case APP_USBD_CDC_ACM_USER_EVT_TX_DONE:
            break;

        case APP_USBD_CDC_ACM_USER_EVT_RX_DONE:
            do
            {
                //Get amount of data transferred to user buffer
                size_t size = app_usbd_cdc_acm_rx_size(p_cdc_acm);
                // NRF_LOG_DEBUG("RX: size: %lu ch=%02x", size, ch);

				// call the receive callback
				if (m_config.rcv_callback)
					m_config.rcv_callback(&ch, 1);
				
                // Fetch data until internal buffer is empty
                ret = app_usbd_cdc_acm_read(&m_usb_cdc_acm, &ch, 1);
            }
            while (ret == NRF_SUCCESS);
            break;
    }
	
	report_acm_status(event);
}

static void usbd_user_ev_handler(app_usbd_event_type_t event)
{
    switch ((uint32_t)event)
    {
        case APP_USBD_EVT_DRV_SUSPEND: 
            NRF_LOG_INFO("USB Suspend");
			break;
        case APP_USBD_EVT_DRV_RESUME:   
            NRF_LOG_INFO("USB resume");
			break;
        case APP_USBD_EVT_STARTED:      
            NRF_LOG_INFO("USB Started");
			break;
        case APP_USBD_EVT_STOPPED:
            app_usbd_disable();
            NRF_LOG_INFO("USB Stopped");
            break;
        case APP_USBD_EVT_POWER_DETECTED:
            NRF_LOG_INFO("USB power detected");

            if (!nrf_drv_usbd_is_enabled())
            {
                app_usbd_enable();
            }
            break;
        case APP_USBD_EVT_POWER_REMOVED:
            NRF_LOG_INFO("USB power removed");
            m_config.connected = false;
			m_config.ready     = false;
            app_usbd_stop();
            break;
        case APP_USBD_EVT_POWER_READY:
            NRF_LOG_INFO("USB ready");
            m_config.connected = true;
			m_config.ready     = false;
            app_usbd_start();
            break;
    }
}

bool m_usb_cdc_is_ready(void)
{
	return m_config.ready;
}

bool m_usb_cdc_is_connected(void)
{
	return m_config.connected;
}

uint32_t m_usb_cdc_write(const void* p_data, uint32_t len)
{
	if (!m_config.ready)
		return NRF_ERROR_INVALID_STATE;
	
	// Send data through CDC ACM
	return app_usbd_cdc_acm_write(&m_usb_cdc_acm,
								  p_data,
								  len);
}

uint32_t m_usb_cdc_write_byte(uint8_t ch)
{
	return m_usb_cdc_write(&ch, 1);
}

void m_usb_cdc_run(void)
{
#if (APP_USBD_CONFIG_EVENT_QUEUE_ENABLE)
	while(app_usbd_event_queue_process())
		;
#endif
}

void m_usb_cdc_close(void)
{
	app_usbd_disable();
	app_usbd_stop();
	
	app_usbd_uninit();
}

uint32_t m_usb_cdc_power_events_enable(void)
{
#if APP_USBD_CONFIG_POWER_EVENTS_PROCESS == 1	
	uint32_t err_code;
	
    err_code = app_usbd_power_events_enable();
    REPORT_IF_ERROR(err_code);
#else
	NRF_LOG_INFO("USB Power events are not enabled");
#endif
	
	return NRF_SUCCESS;
}

uint32_t m_usb_cdc_init(const m_usb_cdc_init_t* p_init)
{
	uint32_t err_code;
	
	m_config.rcv_callback        = p_init->acm_rcv_callback;
	m_config.acm_status_callback = p_init->acm_status_callback;
	
    static const app_usbd_config_t usbd_config = {
        .ev_state_proc = usbd_user_ev_handler
    };
	
    err_code = nrf_drv_clock_init();
    if (err_code != NRF_ERROR_MODULE_ALREADY_INITIALIZED)
    {
        VERIFY_SUCCESS(err_code);
    }

    err_code = nrf_drv_power_init(NULL);
    VERIFY_SUCCESS(err_code);

    app_usbd_serial_num_generate();

    err_code = app_usbd_init(&usbd_config);
    RETURN_IF_ERROR(err_code);

    app_usbd_class_inst_t const * class_cdc_acm = app_usbd_cdc_acm_class_inst_get(&m_usb_cdc_acm);
    err_code = app_usbd_class_append(class_cdc_acm);
    RETURN_IF_ERROR(err_code);

    //err_code = app_usbd_power_events_enable();
    //RETURN_IF_ERROR(err_code);
	
    return NRF_SUCCESS;
}

