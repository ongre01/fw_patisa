#ifndef _m_usb_cdc_h
#define _m_usb_cdc_h

#include "sdk_common.h"
#include "app_usbd_cdc_acm.h"

typedef void (*usb_acm_data_receive_callback_t)(const void* p_data, uint32_t len);
typedef void (*usb_acm_status_callback_t)(app_usbd_cdc_acm_user_event_t event);

typedef struct
{
	usb_acm_status_callback_t       acm_status_callback;
	usb_acm_data_receive_callback_t	acm_rcv_callback;
} m_usb_cdc_init_t;

uint32_t m_usb_cdc_init(const m_usb_cdc_init_t* p_init);
uint32_t m_usb_cdc_power_events_enable(void);
uint32_t m_usb_cdc_write(const void* p_data, uint32_t len);
uint32_t m_usb_cdc_write_byte(uint8_t ch);
void     m_usb_cdc_run(void);
bool     m_usb_cdc_is_ready(void);			// port is opened
bool 	 m_usb_cdc_is_connected(void);		// port is attached
void 	 m_usb_cdc_close(void);

#endif // _m_usb_h
