#ifndef PCA10056_H
#define PCA10056_H

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_gpio.h"

// LEDs definitions for PCA10056
#define LEDS_NUMBER    6

#define LED_1          NRF_GPIO_PIN_MAP(0,6)
#define LED_2          NRF_GPIO_PIN_MAP(0,7)
#define LED_3          NRF_GPIO_PIN_MAP(0,8)
#define LED_4          NRF_GPIO_PIN_MAP(0,9)
#define LED_5          NRF_GPIO_PIN_MAP(0,10)
#define LED_6          NRF_GPIO_PIN_MAP(0,11)

#define LEDS_ACTIVE_STATE 0

#define LEDS_LIST { LED_1, LED_2, LED_3, LED_4, LED_5, LED_6 }

#define LEDS_INV_MASK  LEDS_MASK

#define BUTTONS_NUMBER 2

#define BUTTON_1       NRF_GPIO_PIN_MAP(1,11)
#define BUTTON_2       NRF_GPIO_PIN_MAP(1,12)
#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP

#define BUTTONS_ACTIVE_STATE 0

#define BUTTONS_LIST { BUTTON_1, BUTTON_2 }

#define BSP_BUTTON_0   BUTTON_1
#define BSP_BUTTON_1   BUTTON_2

#define RX_PIN_NUMBER  NRF_GPIO_PIN_MAP(0,31)
#define TX_PIN_NUMBER  NRF_GPIO_PIN_MAP(0,29)
#define CTS_PIN_NUMBER 7
#define RTS_PIN_NUMBER 5
#define HWFC           false


#define MUX_OE			NRF_GPIO_PIN_MAP(0,25)
#define MUX_INT			NRF_GPIO_PIN_MAP(0,24)
#define MUX_RESET		NRF_GPIO_PIN_MAP(1,0)
#define MUX_SDA			NRF_GPIO_PIN_MAP(0,22)
#define MUX_SCL			NRF_GPIO_PIN_MAP(0,23)

#ifdef __cplusplus
}
#endif

#endif // PCA10056_H
