#ifndef _mobilian_neoFlash_h
#define _mobilian_neoFlash_h

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_gpio.h"

// Buzzer definition
#define BUZZER			24

// SWD interface definition
#define SWD_DIO		 	22
#define SWD_CLK		 	23

// I2C  definitions
#define I2C_CLK 		7
#define I2C_DIO  		11

// LEDs definitions for PCA10040
#define LEDS_NUMBER    4

#define LED_START      17
#define LED_1          17
#define LED_2          18
#define LED_3          19
#define LED_4          20
#define LED_STOP       20

#define LEDS_ACTIVE_STATE 1

#define LEDS_INV_MASK  (~LEDS_MASK)

#define LEDS_LIST { LED_1, LED_2, LED_3, LED_4 }

#define BSP_LED_0      LED_1
#define BSP_LED_1      LED_2
#define BSP_LED_2      LED_3
#define BSP_LED_3      LED_4

#define BUTTONS_NUMBER 4

#define BUTTON_START   13
#define BUTTON_1       13
#define BUTTON_2       14
#define BUTTON_3       15
#define BUTTON_4       16
#define BUTTON_STOP    16
#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP

#define BUTTONS_ACTIVE_STATE 0

#define BUTTONS_LIST { BUTTON_1, BUTTON_2, BUTTON_3, BUTTON_4 }

#define BSP_BUTTON_0   BUTTON_1
#define BSP_BUTTON_1   BUTTON_2
#define BSP_BUTTON_2   BUTTON_3
#define BSP_BUTTON_3   BUTTON_4

#define RX_PIN_NUMBER  8
#define TX_PIN_NUMBER  6
#define CTS_PIN_NUMBER 7
#define RTS_PIN_NUMBER 5
#define HWFC           flash

///////////////////////////////////////////////////////////////////////////
// 1-PORT Program kit
///////////////////////////////////////////////////////////////////////////
#define KIT1_POWER						3

///////////////////////////////////////////////////////////////////////////
// 8-PORT Program kit
///////////////////////////////////////////////////////////////////////////
#define MUX1_POWER						3
#define MUX2_POWER						4

///////////////////////////////////////////////////////////////////////////
// Sensor kit
///////////////////////////////////////////////////////////////////////////

// sensor gpio definition
#define SENSOR_CCS811_nWAKE				12
#define SENSOR_CCS811_nRESET			9
#define SENSOR_LED1						3
#define SENSOR_LED2						5
								 
// I2S mic defnition
#define I2S_SCK							25
#define I2S_WS							26
#define I2S_SD							27

///////////////////////////////////////////////////////////////////////////
// LED kit
///////////////////////////////////////////////////////////////////////////
#define LED_CS							28
#define LED_WR							29
#define LED_RD							30
#define LED_DATA						31


#ifdef __cplusplus
}
#endif

#endif // _mobilian_neoFlash_h
