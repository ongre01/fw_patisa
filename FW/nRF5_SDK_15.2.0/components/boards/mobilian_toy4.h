#ifndef MOBILIAN_TOY4_H
#define MOBILIAN_TOY4_H

///////////////////////////////////////////////////////////////////////////
// ELIO definition (2018-03-06)
///////////////////////////////////////////////////////////////////////////

// I2C  definitions
//#define I2C_CLK 		23
//#define I2C_DIO  		22

// LEDs definitions
#define LED_ON			false
#define LED_OFF			true
	
#define LEDS_NUMBER    	4

#define LED_1          	30	// BT_LED		- bt board
#define LED_2          	31	// BAT_LED		- bt board
#define LED_3          	9	// LED_B		- motor board
#define LED_4          	10	// LED_R		- motor board

#define LEDS_LIST { LED_1, LED_2, LED_3, LED_4 }

#define BSP_LED_0      LED_1
#define BSP_LED_1      LED_2
#define BSP_LED_2      LED_3
#define BSP_LED_3      LED_4

// all LEDs are lit when GPIO is low 
#define LEDS_INV_MASK  LEDS_MASK

#define LEDS_ACTIVE_STATE 0

// BUTTON definition
#define BUTTONS_NUMBER 1

#define BUTTON_START   6
#define BUTTON_1       6
#define BUTTON_STOP    6
#define BUTTON_PULL    NRF_GPIO_PIN_PULLDOWN

#define BUTTONS_LIST { BUTTON_1 }

#define BSP_BUTTON_0   BUTTON_1

#define BSP_BUTTON_0_MASK (1<<BSP_BUTTON_0)

#define BUTTONS_ACTIVE_STATE	1

// UART definition
#define RX_PIN_NUMBER  11//26    // UART RX pin number.
#define TX_PIN_NUMBER  12//25    // UART TX pin number.
#define CTS_PIN_NUMBER 0     // UART CTS pin number. Not used if HWFC is set to false. 
#define RTS_PIN_NUMBER 0     // UART RTS pin number. Not used if HWFC is set to false. 
#define HWFC           false // UART hardware flow control.

// Low frequency clock source to be used by the SoftDevice
#define XTAL_INTERNAL

#ifdef XTAL_INTERNAL
#define NRF_CLOCK_LFCLKSRC      {.source        = NRF_CLOCK_LF_SRC_RC,              \
                                 .rc_ctiv       = 16,                                \
                                 .rc_temp_ctiv  = 2,                                \
                                 .xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_20_PPM}
#else
#define NRF_CLOCK_LFCLKSRC      {.source        = NRF_CLOCK_LF_SRC_XTAL,              \
                                 .rc_ctiv       = 0,                                \
                                 .rc_temp_ctiv  = 0,                                \
                                 .xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_20_PPM}
#endif
//////////////////////////////////////////////////////////////////////////////////////
// Power
#define POWER_SW			6
#define POWER_HOLD			7

//////////////////////////////////////////////////////////////////////////////////////
// Battery
#define BAT_ADC				2 // NRF_SAADC_INPUT_AIN0
#define BATT_VOLTAGE_DIVIDER_R1		10000000		// 10M Mohm	
#define BATT_VOLTAGE_DIVIDER_R2		 4000000		// 6.8 KOhm
						 
//////////////////////////////////////////////////////////////////////////////////////
// Motor
#define M_M1_IN1			19
#define M_M1_IN2			18
#define M_M2_IN1			16
#define M_M2_IN2			17
#define SERVO_1				22
#define SERVO_2				21
#define M_SLEEP				15	// (out)
#define M_NFAULT1			4	// fault (in)
#define M_NFAULT2			5	// fault (in)
#define M_FAULT_CNT			2
#define M_FAULT_PINS		{M_NFAULT1, M_NFAULT2}

#define EXT_G_START			25
#define EXT_G_0				28
#define EXT_G_1				27
#define EXT_G_2				26
#define EXT_G_3				25
#define EXT_G_END			28

#define EXT_5V				13
#define EXT_3V				23

#endif // MOBILIAN_TOY_H
