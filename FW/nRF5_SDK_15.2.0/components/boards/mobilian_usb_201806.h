#ifndef MOBILIAN_USB_H
#define MOBILIAN_USB_H

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_gpio.h"

// LEDs definitions for PCA10056
#define LEDS_NUMBER    3

#define LED_1          NRF_GPIO_PIN_MAP(1,2)
#define LED_2          NRF_GPIO_PIN_MAP(1,4)
#define LED_3          NRF_GPIO_PIN_MAP(1,3)
#define LED_START      LED_1
#define LED_STOP       LED_3

#define LEDS_ACTIVE_STATE 0

#define LEDS_LIST { LED_1, LED_2, LED_3 }

#define LEDS_INV_MASK  LEDS_MASK

#define BUTTONS_NUMBER 0

#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP

#define BUTTONS_ACTIVE_STATE 0

#define RX_PIN_NUMBER  NRF_GPIO_PIN_MAP(0, 25)
#define TX_PIN_NUMBER  NRF_GPIO_PIN_MAP(0, 24)
#define CTS_PIN_NUMBER 7
#define RTS_PIN_NUMBER 5
#define HWFC           false

#ifdef __cplusplus
}
#endif

#endif // MOBILIAN_USB_H
