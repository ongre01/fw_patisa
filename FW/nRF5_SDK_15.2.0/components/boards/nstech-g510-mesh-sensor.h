#ifndef nstech_g510_mesh_sensor_h
#define nstech_g510_mesh_sensor_h

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_gpio.h"

// usb detection	
#define USB_DET			13

#define SPWR_ON			12		// sensor power
	
// RF - OP amp
#define OP_AMP_TX_EN	9
#define OP_AMP_RX_EN	10
	
// I2C address
//#define I2C_ADDR_SHT31	0x44
//#define I2C_ADDR_CCS811 	0x5A
#define I2C_ADDR_LPS22H 	0x5C
#define I2C_ADDR_HDC2010 	0x40
#define I2C_ADDR_ICS20948 	0x68
	
/////////////////////////////////////////////////////
// MPU9250 9Axis sensor

// I2C interface
#define LPS_INT			27 //23   2019.02.01

#define ICS_INT			6
#define ICS_I2C_SCL		8
#define ICS_I2C_SDA		7

#define I2C_SCL			8
#define I2C_SDA			7

// CCS811 - AMS TVOC
#define CCS811_NWAKE	30

#define TEMPUS_CO2_RX	14
#define TEMPUS_CO2_TX	15
#define TEMPUS_CO2_PWM	22

// WiFi
#define WIFI_TX			16
#define WIFI_RX			17
#define WIFI_BOOT		18
#define WIFI_RESETEN	19

// UART
#define DTM_TX			20
#define DTM_RX			21

// LED definition
#define BT_AD			29		// BLUE

// BQ24075T, TI - USB Charger
#define CE				31		// active low
#define SYSOFF			27
#define EN1				25
#define EN2				26


// LEDs definitions for PCA10040
#define LEDS_NUMBER    1

#define LED_START      29
#define LED_1          BT_AD	
#define LED_STOP       29

#define LEDS_ACTIVE_STATE 1

#define LEDS_INV_MASK  LEDS_MASK

#define LEDS_LIST { LED_1 }

#define BSP_LED_0      LED_1

#define BUTTONS_NUMBER 1

#define BUTTON_START   11
#define BUTTON_1       11
#define BUTTON_STOP    11
#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP

#define BUTTONS_ACTIVE_STATE 0

#define BUTTONS_LIST { BUTTON_1 }

#define BSP_BUTTON_0   BUTTON_1

#define RX_PIN_NUMBER  DTM_RX
#define TX_PIN_NUMBER  DTM_TX
#define CTS_PIN_NUMBER 0
#define RTS_PIN_NUMBER 0
#define HWFC           false
	
#define R1						1000000
#define R2						1000000
#define BAT_DIVIDER_FACTOR		((R2) / (float)((R1) + (R2)))
#define BAT_DROP				300

#ifdef __cplusplus
}
#endif

#endif // nstech_g510_mesh_sensor_h
