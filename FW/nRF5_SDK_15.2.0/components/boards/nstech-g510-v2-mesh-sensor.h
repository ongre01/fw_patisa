#ifndef G510_V2_MESH_H
#define G510_V2_MESH_H

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_gpio.h"
	// usb detection	
	#define USB_DET			6

	// I2C address
	#define I2C_ADDR_SHT31	0x44
	#define I2C_ADDR_CCS811 0x5A
	#define I2C_ADDR_LPS25H 0x5D
		
	/////////////////////////////////////////////////////
	// MPU9250 9Axis sensor
	
	// I2C interface
	#define MPU_I2C_SCL		19
	#define MPU_I2C_SDA		20
	#define MPU_SA0			15
	#define MPU_INT			22
	
	// SPI interface
	#define MPU_SCLK		19
	#define MPU_SDI			20
	#define MPU_SDO			15
	#define MPU_nCS			18
	
	// CCS811 - AMS TVOC
	#define CCS811_NWAKE	7
	#define CCS811_NRESET	8

	// ICS43432 - Invensense MIC
	#define ICS43432_SCK	12
	#define ICS43432_WS		13
	#define ICS43432_SD		14

	#define I2C_SCL			19
	#define I2C_SDA			20

	// UART
	#define DTM_RX			23
	#define DTM_TX			24

	// BAT
	#define BAT_ADC			28

	// LED definition
	#define BAT_LOW			29		// RED
	#define BT_AD			30		// BLUE

	// BQ24075T, TI - USB Charger
	#define CE				31		// active low
	#define SYSOFF			27
	#define EN1				25
	#define EN2				26


	// LEDs definitions for PCA10040
	#define LEDS_NUMBER    2

	#define LED_START      29
	#define LED_1          BT_AD	
	#define LED_2          BAT_LOW	
	#define LED_STOP       30

	#define LEDS_ACTIVE_STATE 1

	#define LEDS_INV_MASK  LEDS_MASK

	#define LEDS_LIST { LED_1, LED_2 }

	#define BSP_LED_0      LED_1
	#define BSP_LED_1      LED_2

	#define BUTTONS_NUMBER 1

	#define BUTTON_START   11
	#define BUTTON_1       11
	#define BUTTON_STOP    11
	#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP

	#define BUTTONS_ACTIVE_STATE 0

	#define BUTTONS_LIST { BUTTON_1 }

	#define BSP_BUTTON_0   BUTTON_1

	#define RX_PIN_NUMBER  DTM_RX
	#define TX_PIN_NUMBER  DTM_TX
	#define CTS_PIN_NUMBER 0
	#define RTS_PIN_NUMBER 0
	#define HWFC           false
	
#define NRF_CLOCK_LFCLKSRC      {.source       		= NRF_CLOCK_LF_SRC_XTAL,      \
                                 .rc_ctiv      		= 0,                          \
                                 .rc_temp_ctiv 		= 0,                          \
                                 .xtal_accuracy     = NRF_CLOCK_LF_XTAL_ACCURACY_20_PPM}
#ifdef __cplusplus
}
#endif

#endif // G510_V2_MESH_H
