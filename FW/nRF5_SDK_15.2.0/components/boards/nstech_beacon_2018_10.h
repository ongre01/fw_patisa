#ifndef NSTECH_BEACON_2018_10_H
#define NSTECH_BEACON_2018_10_H

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_gpio.h"

//////////////////////////////////////////
#define I2C_SCL				8
#define I2C_SDA				7
	
#define I2C_MOTION_SCL		16
#define I2C_MOTION_SDA		17

#define I2C_LIS3_SCL		15
#define I2C_LIS3_SDA		14
#define LIS3_INT			12
	
#define I2C_ADDR_LPS22H 	0x5C
#define I2C_ADDR_HDC2010 	0x40
#define I2C_ADDR_ICS20948 	0x68
#define I2C_ADDR_LIS3 		0x19

#define LPS_INT			6		// LPS22HB interrupt pin
	
#define ICS_INT			10
#define ICS_I2C_SCL		16
#define ICS_I2C_SDA		17
	
/////////////////////////////////////////
#define BAT_ADC			28		// AIN4
#define BAT_LOW_MV		1700
#define BAT_FULL_MV		3000
#define BAT_DIVIDER_R1	1000000
#define BAT_DIVIDER_R2	1000000
	
// LEDs definitions for beacon
#define LEDS_NUMBER    2

#define LED_1          13
#define LED_2          31

#define LEDS_ACTIVE_STATE 1

#define LEDS_INV_MASK  LEDS_MASK

#define LEDS_LIST { LED_1, LED_2 }

#define BSP_LED_0      LED_1
#define BSP_LED_1      LED_2

#define BUTTONS_NUMBER 1

#define BUTTON_START   11
#define BUTTON_1       11
#define BUTTON_STOP    11
#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP
#define BUTTONS_ACTIVE_STATE 0

//#define BUTTON_PULL    NRF_GPIO_PIN_PULLDOWN
//#define BUTTONS_ACTIVE_STATE 1

#define BUTTONS_LIST { BUTTON_1 }

#define BSP_BUTTON_0   BUTTON_1

#define RX_PIN_NUMBER  21
#define TX_PIN_NUMBER  20
#define CTS_PIN_NUMBER 0
#define RTS_PIN_NUMBER 0
#define HWFC           false

#ifdef __cplusplus
}
#endif

#endif // NSTECH_BEACON_2018_10_H
