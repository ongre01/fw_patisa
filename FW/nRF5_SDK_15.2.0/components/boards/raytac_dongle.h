#ifndef RAYTAC_DONGLE_H
#define RAYTAC_DONGLE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_gpio.h"

#define LEDS_NUMBER    			2

#define LED_1          			NRF_GPIO_PIN_MAP(1,11)
#define LED_2          			NRF_GPIO_PIN_MAP(1,13)

#define LEDS_ACTIVE_STATE 		0

#define LEDS_LIST 				{ LED_1, LED_2 }

#define LEDS_INV_MASK  			LEDS_MASK

#define BUTTONS_NUMBER 			1
#define BUTTON_1	   			NRF_GPIO_PIN_MAP(0,15)
#define ERASE_BOND_BTN	   		BUTTON_1
#define BUTTON_PULL    			NRF_GPIO_PIN_PULLUP
#define BUTTONS_LIST   			{BUTTON_1}

#define BUTTONS_ACTIVE_STATE 	0

#define BSP_BUTTON_0			BUTTON_1

#ifdef __cplusplus
}
#endif

#endif // RAYTAC_DONGLE_H
