#ifndef APP_USBD_MIDI_H__
#define APP_USBD_MIDI_H__

#include <stdint.h>
#include <stdbool.h>

#include "nrf_drv_usbd.h"
#include "app_usbd_class_base.h"
#include "app_usbd.h"
#include "app_usbd_core.h"
#include "app_usbd_descriptor.h"

#include "app_usbd_midi_types.h"
#include "app_usbd_midi_desc.h"
#include "app_usbd_midi_internal.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef DOXYGEN
/**
 * @brief Audio class instance type
 *
 * @ref APP_USBD_CLASS_TYPEDEF
 */
typedef struct { } app_usbd_audio_t;
#else
/*lint -save -e10 -e26 -e123 -e505 */
APP_USBD_CLASS_TYPEDEF(app_usbd_audio,    \
    APP_USBD_MIDI_CONFIG(0, 0, 0, 0, 0),          \
    APP_USBD_AUDIO_INSTANCE_SPECIFIC_DEC, \
    APP_USBD_AUDIO_DATA_SPECIFIC_DEC      \
);
/*lint -restore*/
#endif


/*lint -save -e407 */

/**
 * @brief Events passed to user event handler
 *
 * @note Example prototype of user event handler:
 *
 * @code
   void audio_user_ev_handler(app_usbd_class_inst_t const * p_inst,
                              app_usbd_audio_user_event_t   event);
 * @endcode
 */
typedef enum app_usbd_audio_user_event_e {
    APP_USBD_AUDIO_USER_EVT_CLASS_REQ,   /**< User event CLASS_REQ */
    APP_USBD_AUDIO_USER_EVT_RX_DONE,     /**< User event RX_DONE   */
    APP_USBD_AUDIO_USER_EVT_TX_DONE,     /**< User event TX_DONE   */
} app_usbd_audio_user_event_t;

/*lint -restore*/

/**
 * @brief Global definition of app_usbd_audio_t class instance.
 *
 * @param instance_name             Name of global instance.
 * @param interfaces_configs        Interfaces configurations.
 * @param user_ev_handler           User event handler.
 * @param format_descriptor         Audio class Format descriptor.
 * @param input_descriptor          Audio class Input Terminal descriptor.
 * @param output_descriptor         Audio class Output Terminal descriptor.
 * @param feature_descriptor        Audio class Feature Unit descriptor.
 * @param delay                     Streaming delay.
 * @param format                    FormatTag (@ref app_usbd_audio_as_iface_format_tag_t).
 * @param ep_size                   Endpoint size.
 * @param type_str                  Streaming type MIDISTREAMING/AUDIOSTREAMING.
 *
 * @note This macro is just simplified version of @ref APP_USBD_AUDIO_GLOBAL_DEF_INTERNAL
 *
 */
#define APP_USBD_MIDI_GLOBAL_DEF(instance_name,                              \
                                    user_ev_handler,                            \
                                    comm_ifc,                                   \
                                    data_ifc,                                   \
                                    comm_ein,                                   \
                                    data_ein,                                   \
                                    data_eout)                               \
    APP_USBD_MIDI_GLOBAL_DEF_INTERNAL(instance_name,                         \
                                         user_ev_handler,                       \
                                         comm_ifc,                              \
                                         data_ifc,                              \
                                         comm_ein,                              \
                                         data_ein,                              \
                                         data_eout)                          \

// Standard AC Interface Descriptor
#define APP_USBD_MIDI_DESCRIPTOR(name, ...)             \
    static uint8_t const CONCAT_2(name,  _data)[] =             \
    {                                                           \
        __VA_ARGS__                                             \
    };                                                          \
    static const app_usbd_audio_subclass_desc_t name =          \
    {                                                           \
        sizeof(CONCAT_2(name, _data)),                          \
        APP_USBD_AUDIO_AS_IFACE_SUBTYPE_FORMAT_TYPE,            \
        CONCAT_2(name,_data)                                    \
    }

/**
 * @brief Initializer of Audio Format descriptor.
 *
 * @param name  Format descriptor name.
 * @param ...   Format descriptor data.
*/

#define APP_USBD_AUDIO_FORMAT_DESCRIPTOR(name, ...)             \
    static uint8_t const CONCAT_2(name,  _data)[] =             \
    {                                                           \
        __VA_ARGS__                                             \
    };                                                          \
    static const app_usbd_audio_subclass_desc_t name =          \
    {                                                           \
        sizeof(CONCAT_2(name, _data)),                          \
        APP_USBD_AUDIO_AS_IFACE_SUBTYPE_FORMAT_TYPE,            \
        CONCAT_2(name,_data)                                    \
    }

/**
 * @brief Initializer of Audio Input descriptor.
 *
 * @param name  Input descriptor name.
 * @param ...   Input descriptor data.
*/

#define APP_USBD_AUDIO_INPUT_DESCRIPTOR(name, ...)              \
    static uint8_t const CONCAT_2(name,  _data)[] =             \
    {                                                           \
        __VA_ARGS__                                             \
    };                                                          \
    static const app_usbd_audio_subclass_desc_t name =          \
    {                                                           \
        sizeof(CONCAT_2(name, _data)),                          \
        APP_USBD_AUDIO_AC_IFACE_SUBTYPE_INPUT_TERMINAL,         \
        CONCAT_2(name,_data)                                    \
    }

/**
 * @brief Initializer of Audio Output descriptor.
 *
 * @param name  Output descriptor name.
 * @param ...   Output descriptor data.
*/

#define APP_USBD_AUDIO_OUTPUT_DESCRIPTOR(name, ...)             \
    static uint8_t const CONCAT_2(name,  _data)[] =             \
    {                                                           \
        __VA_ARGS__                                             \
    };                                                          \
    static const app_usbd_audio_subclass_desc_t name =          \
    {                                                           \
        sizeof(CONCAT_2(name, _data)),                          \
        APP_USBD_AUDIO_AC_IFACE_SUBTYPE_OUTPUT_TERNINAL,        \
        CONCAT_2(name,_data)                                    \
    }

/**
 * @brief Initializer of Feture Output descriptor.
 *
 * @param name  Feture descriptor name.
 * @param ...   Feture descriptor data.
*/

#define APP_USBD_AUDIO_FEATURE_DESCRIPTOR(name, ...)            \
    static uint8_t const CONCAT_2(name,  _data)[] =             \
    {                                                           \
        __VA_ARGS__                                             \
    };                                                          \
    static const app_usbd_audio_subclass_desc_t name =          \
    {                                                           \
        sizeof(CONCAT_2(name, _data)),                          \
        APP_USBD_AUDIO_AC_IFACE_SUBTYPE_FEATURE_UNIT,           \
        CONCAT_2(name,_data)                                    \
    }

/**
 * @@brief Helper function to get class instance from Audio class.
 *
 * @param[in] p_audio Audio class instance (declared by @ref APP_USBD_AUDIO_GLOBAL_DEF).
 * @return Base class instance.
 */
static inline app_usbd_class_inst_t const *
app_usbd_audio_class_inst_get(app_usbd_audio_t const * p_audio)
{
    return &p_audio->base;
}

/**
 * @brief Helper function to get audio specific request from audio class.
 *
 * @param[in] p_audio Audio class instance (declared by @ref APP_USBD_AUDIO_GLOBAL_DEF).
 * @return Audio class specific request.
 */
static inline app_usbd_audio_req_t *
app_usbd_audio_class_request_get(app_usbd_audio_t const * p_audio)
{
    return &p_audio->specific.p_data->ctx.request;
}

/**
 * @brief Helper function to get audio from base class instance.
 *
 * @param[in] p_inst Base class instance.
 * @return Audio class handle.
 */
static inline app_usbd_audio_t const *
app_usbd_audio_class_get(app_usbd_class_inst_t const * p_inst)
{
    return (app_usbd_audio_t const *)p_inst;
}

/**
 * @brief Get the size of last received transfer.
 *
 * @note Call this function in reaction to a SOF event to check if there is any data to process.
 *
 * @param p_inst Base class instance.
 *
 * @return Number of bytes received in the last transmission.
 */
size_t app_usbd_audio_class_rx_size_get(app_usbd_class_inst_t const * p_inst);

/**
 * @brief Start audio data copying from the endpoint buffer.
 *
 * Function to be used to copy data from an audio OUT endpoint to a given buffer.
 * When it finishes, an @ref APP_USBD_AUDIO_USER_EVT_RX_DONE event is generated.
 *
 * @param p_inst Base class instance.
 * @param p_buff Target buffer.
 * @param size   Size of the requested data.
 *
 * @return Result of the endpoint transmission start.
 *
 * @sa app_usbd_audio_class_rx_size_get
 *
 * @note This function should be called in reaction to a SOF event.
 *       Isochronous endpoints are double buffered and they are automatically switched at every SOF.
 */
ret_code_t app_usbd_audio_class_rx_start(
    app_usbd_class_inst_t const * p_inst,
    void * p_buff,
    size_t size);

/**
 * @brief Start copying audio data to the endpoint buffer.
 *
 * Function to be used to copy data to an audio IN endpoint from a given buffer.
 * When it finishes, an @ref APP_USBD_AUDIO_USER_EVT_TX_DONE event is generated.
 *
 * @param p_inst Base class instance.
 * @param p_buff Source buffer.
 * @param size   Size of the data to be sent.
 *
 * @return Result of the endpoint transsmision start.
 *
 * @note This function should be called in reaction to a SOF event.
 *       Isochronous endpoints are double buffered and they are automatically switched at every SOF.
 */
ret_code_t app_usbd_audio_class_tx_start(
    app_usbd_class_inst_t const * p_inst,
    const void * p_buff,
    size_t size);

/**
 * @brief Register audio instance as the one that requires SOF events in interrupt.
 *
 * This function should be called before appending the instance.
 *
 * @param p_inst  Audio instance that requires SOF event.
 * @param handler Handler to SOF event
 *
 * @retval NRF_SUCCESS Instance linked into SOF processing list.
 *
 * @sa app_usbd_class_sof_interrupt_register
 */
ret_code_t app_usbd_audio_sof_interrupt_register(app_usbd_class_inst_t const * p_inst,
                                                 app_usbd_sof_interrupt_handler_t handler);
/** @} */

#ifdef __cplusplus
}
#endif

#endif /* APP_USBD_MIDI_H__ */
