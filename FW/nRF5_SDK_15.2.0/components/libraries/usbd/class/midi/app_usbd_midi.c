#include "sdk_common.h"
#if NRF_MODULE_ENABLED(APP_USBD_AUDIO)

#include "app_usbd_midi.h"
#include "app_util_platform.h"

#define NRF_LOG_MODULE_NAME midi

#include "nrf_log.h"
NRF_LOG_MODULE_REGISTER();

#define APP_USBD_MIDI_IFACE_IDX    		0 // MIDI interface index
#define APP_USBD_MIDI_STREAMING_EP_IDX 	0 // MIDI streaming endpoint index

// Auxiliary function to access audio class instance data.
static inline app_usbd_audio_t const * audio_get(app_usbd_class_inst_t const * p_inst)
{
    ASSERT(p_inst != NULL);
    return (app_usbd_audio_t const *)p_inst;
}


// Auxiliary function to access audio class context data.
static inline app_usbd_audio_ctx_t * audio_ctx_get(app_usbd_audio_t const * p_audio)
{
    ASSERT(p_audio != NULL);
    ASSERT(p_audio->specific.p_data != NULL);
    return &p_audio->specific.p_data->ctx;
}


// User event handler.
static inline void user_event_handler(
    app_usbd_class_inst_t const * p_inst,
    app_usbd_audio_user_event_t   event)
{
    app_usbd_audio_t const * p_audio = audio_get(p_inst);

    if (p_audio->specific.inst.user_ev_handler != NULL)
    {
        p_audio->specific.inst.user_ev_handler(p_inst, event);
    }
}



/**
 * @brief Internal SETUP standard IN request handler.
 *
 * @param[in] p_inst        Generic class instance.
 * @param[in] p_setup_ev    Setup event.
 *
 * @return Standard error code.
 * @retval NRF_SUCCESS              Request handled correctly.
 * @retval NRF_ERROR_NOT_SUPPORTED  Request is not supported.
 */
static ret_code_t setup_req_std_in(app_usbd_class_inst_t const * p_inst,
                                   app_usbd_setup_evt_t const  * p_setup_ev)
{
	app_usbd_setup_reqrec_t requestType = app_usbd_setup_req_rec(p_setup_ev->setup.bmRequestType);
	NRF_LOG_INFO("std in req: %d, %d", requestType, p_setup_ev->setup.bmRequest);
	
    /* Only Get Descriptor standard IN request is supported by Audio class */
    if ((requestType == APP_USBD_SETUP_REQREC_INTERFACE)
        &&
        (p_setup_ev->setup.bmRequest == APP_USBD_SETUP_STDREQ_GET_DESCRIPTOR))
    {
        size_t dsc_len = 0;
        size_t max_size;

        uint8_t * p_trans_buff = app_usbd_core_setup_transfer_buff_get(&max_size);

        /* Try to find descriptor in class internals*/
        ret_code_t ret = app_usbd_class_descriptor_find(
            p_inst,
            p_setup_ev->setup.wValue.hb,
            p_setup_ev->setup.wValue.lb,
            p_trans_buff,
            &dsc_len);
		
		NRF_LOG_INFO("std in : %d, %d dsc_len=%d ret = %d", p_setup_ev->setup.wValue.hb, p_setup_ev->setup.wValue.lb, dsc_len, ret);

        if (ret != NRF_ERROR_NOT_FOUND)
        {
            ASSERT(dsc_len < NRF_DRV_USBD_EPSIZE);
            return app_usbd_core_setup_rsp(&(p_setup_ev->setup), p_trans_buff, dsc_len);
        }
    }

    return NRF_ERROR_NOT_SUPPORTED;
}

// Internal SETUP class IN request handler.
static ret_code_t setup_req_class_in(
    app_usbd_class_inst_t const * p_inst,
    app_usbd_setup_evt_t const  * p_setup_ev)
{
    switch (p_setup_ev->setup.bmRequest)
    {
        default:
            break;
    }

    return NRF_ERROR_NOT_SUPPORTED;
}


/**
 * @brief Internal SETUP class OUT request handler.
 *
 * @param[in] p_inst        Generic class instance.
 * @param[in] p_setup_ev    Setup event.
 *
 * @return Standard error code
 * @retval NRF_SUCCESS              Request handled correctly.
 * @retval NRF_ERROR_NOT_SUPPORTED  Request is not supported.
 */
static ret_code_t setup_req_class_out(
    app_usbd_class_inst_t const * p_inst,
    app_usbd_setup_evt_t const  * p_setup_ev)
{
    switch (p_setup_ev->setup.bmRequest)
    {
        default:
            break;
    }

    return NRF_ERROR_NOT_SUPPORTED;
}


/**
 * @brief Control endpoint handle.
 *
 * @param[in] p_inst        Generic class instance.
 * @param[in] p_setup_ev    Setup event.
 *
 * @return Standard error code.
 * @retval NRF_SUCCESS              Request handled correctly.
 * @retval NRF_ERROR_NOT_SUPPORTED  Request is not supported.
 */
static ret_code_t setup_event_handler(
    app_usbd_class_inst_t const * p_inst,
    app_usbd_setup_evt_t const  * p_setup_ev)
{
    ASSERT(p_inst != NULL);
    ASSERT(p_setup_ev != NULL);

	uint32_t type;
	type = app_usbd_setup_req_typ(p_setup_ev->setup.bmRequestType);
	
    if (app_usbd_setup_req_dir(p_setup_ev->setup.bmRequestType) == APP_USBD_SETUP_REQDIR_IN)
    {
		NRF_LOG_INFO(" *** setup IN : %d", type);
        switch (type)
        {
            case APP_USBD_SETUP_REQTYPE_STD:
                return setup_req_std_in(p_inst, p_setup_ev);

            case APP_USBD_SETUP_REQTYPE_CLASS:
                return setup_req_class_in(p_inst, p_setup_ev);

            default:
                break;
        }
    }
    else /*APP_USBD_SETUP_REQDIR_OUT*/
    {
		NRF_LOG_INFO(" *** setup OUT : %d", type);
        switch (type)
        {
			//case APP_USBD_SETUP_REQTYPE_STD:
            //    return setup_req_std_out(p_inst, p_setup_ev);
            case APP_USBD_SETUP_REQTYPE_CLASS:
                return setup_req_class_out(p_inst, p_setup_ev);

            default:
                break;
        }
    }

    return NRF_ERROR_NOT_SUPPORTED;
}


/**
 * @brief Endpoint IN event handler.
 *
 * @param[in] p_inst        Generic class instance.
 *
 * @return Standard error code.
 * @retval NRF_SUCCESS              Request handled correctly.
 * @retval NRF_ERROR_NOT_SUPPORTED  Request is not supported.
 */
static ret_code_t endpoint_in_event_handler(app_usbd_class_inst_t const * p_inst)
{
    //NRF_LOG_INFO("Tx done");
    user_event_handler(p_inst, APP_USBD_AUDIO_USER_EVT_TX_DONE);
    return NRF_SUCCESS;
}


/**
 * @brief Endpoint OUT event handler.
 *
 * @param[in] p_inst        Generic class instance.
 *
 * @return Standard error code.
 * @retval NRF_SUCCESS              Request handled correctly.
 * @retval NRF_ERROR_NOT_SUPPORTED  Request is not supported.
 */
static ret_code_t endpoint_out_event_handler(app_usbd_class_inst_t const * p_inst)
{
    NRF_LOG_INFO("Rx done");
    user_event_handler(p_inst, APP_USBD_AUDIO_USER_EVT_RX_DONE);
    return NRF_SUCCESS;
}


/**
 * @brief Auxiliary function to access endpoint address.
 *
 * @param[in] p_inst Class instance data.
 *
 * @return ISO endpoint address.
 */
static inline nrf_drv_usbd_ep_t ep_midi_addr_get(app_usbd_class_inst_t const * p_inst)
{
    app_usbd_class_iface_conf_t const * class_iface;

    class_iface = app_usbd_class_iface_get(p_inst, APP_USBD_MIDI_IFACE_IDX);

    app_usbd_class_ep_conf_t const * ep_cfg;
    ep_cfg = app_usbd_class_iface_ep_get(class_iface, APP_USBD_MIDI_STREAMING_EP_IDX);

    return app_usbd_class_ep_address_get(ep_cfg);
}


/**
 * @brief @ref app_usbd_class_methods_t::event_handler
 */
static ret_code_t audio_event_handler(
    app_usbd_class_inst_t const  * p_inst,
    app_usbd_complex_evt_t const * p_event)
{
    ASSERT(p_inst != NULL);
    ASSERT(p_event != NULL);

    ret_code_t ret = NRF_SUCCESS;

    //NRF_LOG_INFO("audio_event_handler : %d", p_event->app_evt.type);
	
    switch (p_event->app_evt.type)
    {
        case APP_USBD_EVT_DRV_RESET:
			NRF_LOG_INFO("***reset");
            break;

        case APP_USBD_EVT_DRV_SETUP:
			NRF_LOG_INFO("***setup");
            ret = setup_event_handler(p_inst, (app_usbd_setup_evt_t const *)p_event);
            break;

        case APP_USBD_EVT_DRV_EPTRANSFER:
            if (NRF_USBD_EPIN_CHECK(p_event->drv_evt.data.eptransfer.ep))
            {
				//NRF_LOG_INFO("***ep-transfer -> in");
                ret = endpoint_in_event_handler(p_inst);
            }
            else
            {
				//NRF_LOG_INFO("***ep-transfer -> out");
                ret = endpoint_out_event_handler(p_inst);
            }
            break;

        case APP_USBD_EVT_DRV_SUSPEND:
			NRF_LOG_INFO("*** suspend");
            break;

        case APP_USBD_EVT_DRV_RESUME:
			NRF_LOG_INFO("*** resume");
            break;

        case APP_USBD_EVT_INST_APPEND:
        {
			NRF_LOG_INFO("*** inst append");
            app_usbd_audio_t const * p_audio     = audio_get(p_inst);
            app_usbd_audio_ctx_t   * p_audio_ctx = audio_ctx_get(p_audio);
            if(p_audio_ctx->sof_handler != NULL)
            {
                ret = app_usbd_class_sof_interrupt_register(p_inst, p_audio_ctx->sof_handler);
                APP_ERROR_CHECK(ret);
            }
            break;
        }

        case APP_USBD_EVT_INST_REMOVE:
        {
			NRF_LOG_INFO("*** inst remove");
            app_usbd_audio_t const * p_audio     = audio_get(p_inst);
            app_usbd_audio_ctx_t   * p_audio_ctx = audio_ctx_get(p_audio);
            if(p_audio_ctx->sof_handler != NULL)
            {
                ret = app_usbd_class_sof_interrupt_unregister(p_inst);
                APP_ERROR_CHECK(ret);
            }
            break;
        }

        case APP_USBD_EVT_STARTED:
			NRF_LOG_INFO("*** started");
            break;

        case APP_USBD_EVT_STOPPED:
			NRF_LOG_INFO("*** stopped");
            break;

        default:
			NRF_LOG_INFO("*** Not supported");
            ret = NRF_ERROR_NOT_SUPPORTED;
            break;
    }

    return ret;
}


/**
 * @brief @ref app_usbd_class_methods_t::feed_descriptors
 */

static bool audio_feed_descriptors(app_usbd_class_descriptor_ctx_t * p_ctx,
                                   app_usbd_class_inst_t const     * p_inst,
                                   uint8_t                         * p_buff,
                                   size_t                            max_size)
{
    static uint8_t ifaces   = 0;
		
    ifaces = app_usbd_class_iface_count_get(p_inst);
	ASSERT(ifaces == 2);
	
    app_usbd_audio_t const * p_audio = audio_get(p_inst);

    APP_USBD_CLASS_DESCRIPTOR_BEGIN(p_ctx, p_buff, max_size);

	// Audio interface colletion
	{
		// MIDIStream interface descriptor
		// 09, 04, 01, 00, 02, 01, 03, 00, 00
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x09); 									// bLength
		APP_USBD_CLASS_DESCRIPTOR_WRITE(APP_USBD_DESCRIPTOR_INTERFACE); 		// (04) bDescriptorType = Interface
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x01); 									// bInterfaceNumber
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x00); 									// bAlternateSetting
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x02); 									// bNumEndpoints (2개)
		APP_USBD_CLASS_DESCRIPTOR_WRITE(APP_USBD_AUDIO_CLASS); 					// (01) bInterfaceClass = Audio
		APP_USBD_CLASS_DESCRIPTOR_WRITE(APP_USBD_AUDIO_SUBCLASS_MIDISTREAMING); // (3) bInterfaceSubclass (Midi streaming)
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x00); 									// (00) bInterfaceProtocol
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x00); 									// iInterface

		// class specific MIDIStream interface header descriptor
		// 0x07, 0x24, 0x01, 0x00, 0x01, 0x41, 0x00
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x07); 									// bLength
		APP_USBD_CLASS_DESCRIPTOR_WRITE(APP_USBD_AUDIO_DESCRIPTOR_INTERFACE); 	// (24) bDescriptorType
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x01); 									// MS_HEADER subtype
		APP_USBD_CLASS_DESCRIPTOR_WRITE(LSB_16(0x0100)); 						// revision of this class specification
		APP_USBD_CLASS_DESCRIPTOR_WRITE(MSB_16(0x0100)); 
		APP_USBD_CLASS_DESCRIPTOR_WRITE(LSB_16(0x0041)); 						// total size of class specification descriptors
		APP_USBD_CLASS_DESCRIPTOR_WRITE(MSB_16(0x0041)); 						// 65
	}	
	
	// MIDI IN JACKS
	{
		//0x06, 0x24, 0x02, 0x01, 0x01, 0x00,
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x06); // bLength
		APP_USBD_CLASS_DESCRIPTOR_WRITE(APP_USBD_AUDIO_DESCRIPTOR_INTERFACE); // (24) CS_INTERFACE descriptor.
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x02); // subtype midi_in_jack
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x01); // bJackType (embedded)
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x01); // bJackID
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x00); // iJack (Unused)
		
		
		// 0x06, 0x24, 0x02, 0x02, 0x02, 0x00,
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x06); // bLength
		APP_USBD_CLASS_DESCRIPTOR_WRITE(APP_USBD_AUDIO_DESCRIPTOR_INTERFACE); // CS_INTERFACE descriptor.
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x02); // subtype midi_in_jack
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x02); // bJackType (external)
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x02); // bJackID
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x00); // iJack (Unused)
	}
	
	// MIDI OUT JACKS
	{
		//0x09, 0x24, 0x03, 0x01, 0x03, 0x01, 0x02, 0x01, 0x00,
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x09); 									// bLength
		APP_USBD_CLASS_DESCRIPTOR_WRITE(APP_USBD_AUDIO_DESCRIPTOR_INTERFACE); 	// CS_INTERFACE descriptor.
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x03); 	// subtype midi_out_jack
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x01); // bJackType (embedded)
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x03); // bJackID
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x01); // number of input pins of this jack
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x02); // ID if tge entity to which this pin is connected
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x01); // Output pin number of the entity to which this input pin is connected
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x00); // iJack (Unused)
		
		
		//0x09, 0x24, 0x03, 0x02, 0x06, 0x01, 0x01, 0x01, 0x00, (조금 다르네)
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x09); 									// bLength
		APP_USBD_CLASS_DESCRIPTOR_WRITE(APP_USBD_AUDIO_DESCRIPTOR_INTERFACE); 	// CS_INTERFACE descriptor.
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x03); // subtype midi_out_jack
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x02); // bJackType (external)
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x06); // bJackID
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x01); // number of input pins of this jack
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x01); // ID if tge entity to which this pin is connected
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x01); // Output pin number of the entity to which this input pin is connected
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x00); // iJack (Unused)
	}
	
	// OUT endpoint descriptor
	{
		// standard out end point descriptor
		// 0x09, 0x05, MIDI_OUT_EP, 0x02, 0x40, 0x00, 0x00, 0x00, 0x00,
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x09);
		APP_USBD_CLASS_DESCRIPTOR_WRITE(APP_USBD_DESCRIPTOR_ENDPOINT);
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x01);		// *** OUT Endpoint 1     (요거확인)
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x02);		// bmAttributes - Bulk
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x40);		// 64 bytes per packet
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x00);		// 64 bytes per packet
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x00);		// interval, ignore for bulk, set to zero
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x00);		// bRefresh - unused
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x00);		// bSynchAddress - unused
		
		// MS Bulk
		// 0x05, 0x25, 0x01, 0x01, 0x01,
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x05);  // descriptor length (4+num_port)
		APP_USBD_CLASS_DESCRIPTOR_WRITE(APP_USBD_AUDIO_DESCRIPTOR_ENDPOINT);
		APP_USBD_CLASS_DESCRIPTOR_WRITE(APP_USBD_AUDIO_AS_IFACE_SUBTYPE_GENERAL); // (01)
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x01); 		// number of embedded midi in jacks
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x01); 		// id of the embedded midi in jack
	}

	// IN endpoint descriptor
	{
		// standard bulk in endpoint descriptor
		//0x09, 0x05, MIDI_IN_EP, 0x02, 0x40, 0x00, 0x00, 0x00, 0x00,
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x09);
		APP_USBD_CLASS_DESCRIPTOR_WRITE(APP_USBD_DESCRIPTOR_ENDPOINT);
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x81);		// *** IN Endpoint 1     (요거확인)
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x02);
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x40);		// 64 bytes per packet
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x00);
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x00);	// interval, ignore for bulk, set to zero
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x00);	// bRefresh - unused
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x00);	// bSynchAddress - unused
			
		// MS Bulk
		//0x05, 0x25, 0x01, 0x01, 0x03,
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x05); // descriptor length (4+num_port)
		APP_USBD_CLASS_DESCRIPTOR_WRITE(APP_USBD_AUDIO_DESCRIPTOR_ENDPOINT);
		APP_USBD_CLASS_DESCRIPTOR_WRITE(APP_USBD_AUDIO_AS_IFACE_SUBTYPE_GENERAL); // (01)
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x01); // number of embedded midi in jacks
		APP_USBD_CLASS_DESCRIPTOR_WRITE(0x03); // id of the embedded midi in jack
	}
		
    APP_USBD_CLASS_DESCRIPTOR_END();
}

/** @} */

const app_usbd_class_methods_t app_usbd_audio_class_methods = {
    .event_handler       = audio_event_handler,
    .feed_descriptors    = audio_feed_descriptors,
};

ret_code_t app_usbd_audio_class_tx_start(
    app_usbd_class_inst_t const * p_inst,
    const void                  * p_buff,
    size_t                        size)
{
    nrf_drv_usbd_ep_t ep_addr;

    ep_addr = ep_midi_addr_get(p_inst);
    ASSERT(NRF_USBD_EPISO_CHECK(ep_addr));

    NRF_DRV_USBD_TRANSFER_IN(transfer, p_buff, size);
    return app_usbd_ep_transfer(ep_addr, &transfer);
}


#endif //NRF_MODULE_ENABLED(APP_USBD_AUDIO)
