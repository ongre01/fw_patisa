#ifndef APP_USBD_MIDI_H__
#define APP_USBD_MIDI_H__

#include <stdint.h>
#include <stdbool.h>

#include "nrf_drv_usbd.h"
#include "app_usbd_class_base.h"
#include "app_usbd.h"
#include "app_usbd_core.h"
#include "app_usbd_descriptor.h"

#include "app_usbd_midi_types.h"
#include "app_usbd_midi_desc.h"
#include "app_usbd_midi_internal.h"

#ifdef __cplusplus
extern "C" {
#endif

/*lint -save -e10 -e26 -e123 -e505 */
APP_USBD_CLASS_TYPEDEF(app_usbd_audio,    \
    APP_USBD_MIDI_CONFIG(0, 0, 0),          \
    APP_USBD_AUDIO_INSTANCE_SPECIFIC_DEC, \
    APP_USBD_AUDIO_DATA_SPECIFIC_DEC      \
);
/*lint -restore*/


/*lint -save -e407 */
// Events passed to user event handler
typedef enum app_usbd_audio_user_event_e {
    APP_USBD_AUDIO_USER_EVT_CLASS_REQ,   // User event CLASS_REQ 
    APP_USBD_AUDIO_USER_EVT_RX_DONE,     // User event RX_DONE   
    APP_USBD_AUDIO_USER_EVT_TX_DONE,     // User event TX_DONE   
} app_usbd_audio_user_event_t;
/*lint -restore*/

#define APP_USBD_MIDI_GLOBAL_DEF(instance_name,        \
                                    user_ev_handler,   \
                                    data_ifc,          \
                                    data_ein,          \
                                    data_eout)         \
    APP_USBD_MIDI_GLOBAL_DEF_INTERNAL(instance_name,   \
                                      user_ev_handler, \
                                      data_ifc,        \
                                      data_ein,        \
                                      data_eout)       \


// Helper function to get class instance from Audio class.
static inline app_usbd_class_inst_t const *
app_usbd_audio_class_inst_get(app_usbd_audio_t const * p_audio)
{
    return &p_audio->base;
}

// Helper function to get audio specific request from audio class.
static inline app_usbd_audio_req_t *
app_usbd_audio_class_request_get(app_usbd_audio_t const * p_audio)
{
    return &p_audio->specific.p_data->ctx.request;
}

// Helper function to get audio from base class instance.
static inline app_usbd_audio_t const *
app_usbd_audio_class_get(app_usbd_class_inst_t const * p_inst)
{
    return (app_usbd_audio_t const *)p_inst;
}

// Get the size of last received transfer.
size_t app_usbd_audio_class_rx_size_get(app_usbd_class_inst_t const * p_inst);

// Start audio data copying from the endpoint buffer.
ret_code_t app_usbd_audio_class_rx_start(
    app_usbd_class_inst_t const * p_inst,
    void * p_buff,
    size_t size);

// Start copying audio data to the endpoint buffer.
ret_code_t app_usbd_audio_class_tx_start(
    app_usbd_class_inst_t const * p_inst,
    const void * p_buff,
    size_t size);

// Register audio instance as the one that requires SOF events in interrupt.
ret_code_t app_usbd_audio_sof_interrupt_register(app_usbd_class_inst_t const * p_inst,
                                                 app_usbd_sof_interrupt_handler_t handler);

#ifdef __cplusplus
}
#endif

#endif /* APP_USBD_MIDI_H__ */
