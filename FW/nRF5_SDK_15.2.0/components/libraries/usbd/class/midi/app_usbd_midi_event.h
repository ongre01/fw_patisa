#ifndef _app_usbd_midi_event_h
#define _app_usbd_midi_event_h

#include "sdk_common.h"

typedef enum
{
	MIDI_CODE_INDEX_MISCELLANEOUS=0,
	MIDI_CODE_INDEX_CABLE_EVENT,
	MIDI_CODE_INDEX_2SYSTEM_COMMON_MESSAGE,
	MIDI_CODE_INDEX_3SYSTEM_COMMON_MESSAGE,
	MIDI_CODE_INDEX_SYSEX_START=4,
	MIDI_CODE_INDEX_SYSEX_CONT=4,
	MIDI_CODE_INDEX_1SYSTEM_COMMON_MESSAGE=5,
	MIDI_CODE_INDEX_SYSEX_END=5,
	MIDI_CODE_INDEX_SYSEX_END_2FOLLOW=6,
	MIDI_CODE_INDEX_SYSEX_END_3FOLLOW=7,
	MIDI_CODE_INDEX_NOTE_OFF=8,
	MIDI_CODE_INDEX_NOTE_ON=9,
	MIDI_CODE_INDEX_POLY_KEY_PRESS=0x0a,
	MIDI_CODE_INDEX_CONTROL_CHANGE=0x0b,
	MIDI_CODE_INDEX_PROGRAM_CHANGE=0x0c,
	MIDI_CODE_INDEX_CHANNEL_PRESSURE=0x0d,
	MIDI_CODE_INDEX_PITBAND_CHANGE=0x0e,
	MIDI_CODE_INDEX_SINGLE_BYTE=0x0f,
} app_usbd_midi_code_index_no_t;

#pragma pack(1)

#define APP_MIDI_OUT_EVENT_LEN		3

typedef struct
{
	uint8_t	status;					// event(4), channel(4)
	uint8_t	note;
	uint8_t	velocity;
} app_midi_out_event_t;

#pragma pack()


#pragma pack(1)

#define APP_USBD_MIDI_EVENT_LEN		4

typedef struct
{
	uint8_t	code_index_no:4;		// code index number
	uint8_t	cable_no:4;				// cable number
	uint8_t	status;					// event(4), channel(4)
	uint8_t	note;
	uint8_t	velocity;
} app_usbd_midi_event_t;

#pragma pack()

#define APP_USBD_MIDI_STATUS(event, channel) 	((1<<7) | (((event)&0x07)<<4) | ((channel)&0x0F))
#define APP_USBD_MIDI_STATUS_NOTE_ON(channel)  	APP_USBD_MIDI_STATUS(0x01, channel)
#define APP_USBD_MIDI_STATUS_NOTE_OFF(channel) 	APP_USBD_MIDI_STATUS(0x00, channel)

#define APP_USBD_MIDI_EVENT_NOTE_ON(_cable, _channel, _note, _velocity) {	\
	.code_index_no	= MIDI_CODE_INDEX_NOTE_ON,							\
	.cable_no		= ((_cable) & 0x0F),								\
	.status			= APP_USBD_MIDI_STATUS_NOTE_ON(_channel),			\
	.note			= ((_note) & 0x7F),									\
	.velocity		= ((_velocity) & 0x7F),								\
}

#define APP_USBD_MIDI_EVENT_NOTE_OFF(_cable, _channel, _note) {			\
	.code_index_no	= MIDI_CODE_INDEX_NOTE_OFF,							\
	.cable_no		= ((_cable) & 0x0F),								\
	.status			= APP_USBD_MIDI_STATUS_NOTE_OFF(_channel),			\
	.note			= ((_note) & 0x7F),									\
	.velocity		= 0,												\
}

#endif // _app_usbd_midi_event_h
