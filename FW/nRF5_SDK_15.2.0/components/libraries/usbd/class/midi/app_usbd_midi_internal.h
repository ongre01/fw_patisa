#ifndef APP_USBD_AUDIO_INTERNAL_H__
#define APP_USBD_AUDIO_INTERNAL_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup app_usbd_audio_internal USB Audio internals
 * @brief @tagAPI52840 USB Audio class internals.
 * @ingroup app_usbd_audio
 * @{
 */

/**
 * @brief Forward declaration of type defined by @ref APP_USBD_CLASS_TYPEDEF in audio class.
 *
 */
APP_USBD_CLASS_FORWARD(app_usbd_audio);

/*lint -save -e165*/
/**
 * @brief Forward declaration of @ref app_usbd_audio_user_event_e
 *
 */
enum app_usbd_audio_user_event_e;

/*lint -restore*/

/**
 * @brief User event handler.
 *
 * @param[in] p_inst    Class instance.
 * @param[in] event     User event.
 *
 */
typedef void (*app_usbd_audio_user_ev_handler_t)(app_usbd_class_inst_t const *  p_inst,
                                                 enum app_usbd_audio_user_event_e event);


/**
 * @brief Audio subclass descriptor.
 */

typedef struct {
   size_t                size;
   uint8_t               type;
   uint8_t const * const p_data;
} app_usbd_audio_subclass_desc_t;

/**
 * @brief Audio class part of class instance data.
 */
typedef struct {

    app_usbd_audio_subclass_desc_t const * const p_format_dsc;  //!< Audio class Format descriptor
    app_usbd_audio_subclass_desc_t const * const p_input_dsc;   //!< Audio class Input Terminal descriptor
    app_usbd_audio_subclass_desc_t const * const p_output_dsc;  //!< Audio class Output Terminal descriptor
    app_usbd_audio_subclass_desc_t const * const p_feature_dsc; //!< Audio class Feature Unit descriptor

    uint8_t  delay;     //!< Streaming delay
    uint16_t format;    //!< FormatTag (@ref app_usbd_audio_as_iface_format_tag_t)
    uint16_t ep_size;   //!< Endpoint size

    app_usbd_audio_subclass_t type_streaming;   //!< Streaming type MIDISTREAMING/AUDIOSTREAMING (@ref app_usbd_audio_subclass_t)

    app_usbd_audio_user_ev_handler_t user_ev_handler; //!< User event handler
} app_usbd_audio_inst_t;

/**
 * @brief Audio class request target.
 */
typedef enum {
    APP_USBD_AUDIO_CLASS_REQ_IN,  /**< Audio class request IN              */
    APP_USBD_AUDIO_CLASS_REQ_OUT, /**< Audio class request OUT             */
    APP_USBD_AUDIO_EP_REQ_IN,     /**< Audio class endpoint request IN     */
    APP_USBD_AUDIO_EP_REQ_OUT,    /**< Audio class endpoint request OUT    */
} app_usbd_audio_class_req_target_t;

/**
 * @brief Audio class specific request handled via control endpoint.
 */
typedef struct {
    app_usbd_audio_class_req_target_t req_target;   //!< Request target
    app_usbd_audio_req_type_t         req_type;     //!< Request type

    uint8_t  control;       //!< Request control field
    uint8_t  channel;       //!< Channel ID
    uint8_t  interface;     //!< Interface ID
    uint8_t  entity;        //!< Entity ID
    uint16_t length;        //!< Request payload length

    uint8_t payload[64];    //!< Request payload
} app_usbd_audio_req_t;


/**
 * @brief Audio class context.
 *
 */
typedef struct {
    app_usbd_audio_req_t                request;        //!< Audio class request
    bool                                streaming;      //!< Streaming flag
    app_usbd_sof_interrupt_handler_t    sof_handler;    //!< SOF event handler
} app_usbd_audio_ctx_t;


// Audio class configuration macro.
#define APP_USBD_MIDI_CONFIG(iface_data, epin_data, epout_data)   \
        (                                                           \
         (iface_data, epin_data, epout_data))

/**
 * @brief Specific class constant data for audio class.
 *
 * @ref app_usbd_audio_inst_t
 */
#define APP_USBD_AUDIO_INSTANCE_SPECIFIC_DEC app_usbd_audio_inst_t inst;

/**
 * @brief Configures audio class instance.
 *
 * @param user_event_handler        User event handler.
 * @param format_descriptor         Audio class Format descriptor.
 * @param input_descriptor          Audio class Input Terminal descriptor.
 * @param output_descriptor         Audio class Output Terminal descriptor.
 * @param feature_descriptor        Audio class Feature Unit descriptor.
 * @param dlay                      Streaming delay.
 * @param frmat                     FormatTag (@ref app_usbd_audio_as_iface_format_tag_t).
 * @param ep_siz                    Endpoint size.
 * @param type_str                  Streaming type MIDISTREAMING/AUDIOSTREAMING.
 */
 #define APP_USBD_AUDIO_INST_CONFIG(user_event_handler)             \
    .inst = {                                                       \
         .user_ev_handler = user_event_handler }

/**
 * @brief Specific class data for audio class.
 *
 * @ref app_usbd_audio_ctx_t
 */
#define APP_USBD_AUDIO_DATA_SPECIFIC_DEC app_usbd_audio_ctx_t ctx;


/**
 * @brief Audio class descriptors config macro.
 *
 * @param interface_number Interface number.
 * @param ...              Extracted endpoint list.
 */
#define APP_USBD_AUDIO_DSC_CONFIG(interface_number, ...) {              \
    APP_USBD_AUDIO_INTERFACE_DSC(interface_number,                      \
                                 0,                                     \
                                 0,                                     \
                                 APP_USBD_AUDIO_SUBCLASS_AUDIOCONTROL)  \
    }

#define APP_USBD_CDC_ACM_CONFIG(iface_comm, epin_comm, iface_data, epin_data, epout_data)   \
	((iface_comm, epin_comm),                                                           \
	 (iface_data, epin_data, epout_data))

/**
 * @brief Public audio class interface.
 *
 */
extern const app_usbd_class_methods_t app_usbd_audio_class_methods;


/**
 * @brief Global definition of @ref app_usbd_audio_t class
 *
 */
/*lint -save -emacro(26 64 123 505 572 651, APP_USBD_CDC_ACM_GLOBAL_DEF_INTERNAL)*/
	
 #define APP_USBD_MIDI_GLOBAL_DEF_INTERNAL(instance_name,          \
                                             user_ev_handler,       \
                                             data_ifc,              \
                                             data_ein,              \
                                             data_eout)             \
    APP_USBD_CLASS_INST_GLOBAL_DEF(                                 \
        instance_name,                                              \
        app_usbd_audio,                                             \
        &app_usbd_audio_class_methods,                              \
        APP_USBD_MIDI_CONFIG(data_ifc, data_ein, data_eout),  \
        (APP_USBD_AUDIO_INST_CONFIG(user_ev_handler))               \
    )
	
/*lint -restore*/


/** @} */


#ifdef __cplusplus
}
#endif

#endif /* APP_USBD_AUDIO_INTERNAL_H__ */
