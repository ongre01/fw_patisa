echo off
set arg1=%1
nrfutil pkg generate --hw-version 52 --sd-req 0xAE --application-version 0x00 --application bedsore_app_nrf52840_xxaa.hex --key-file .\signing_key\priv_bluefish.pem %arg1%.zip
